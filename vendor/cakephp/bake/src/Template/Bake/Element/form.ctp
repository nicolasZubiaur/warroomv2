<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->columnType($field) !== 'binary';
    });

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}
%>
<section class="content">
<div class="row">
  <div class="col-md-12">
      <div class="box">
	        <div class="box-header">
	          <h3 class="box-title"><i class="fa fa-add"></i> <?= __('<%= Inflector::humanize($action) %> <%= $singularHumanName %>') ?></h3>
	
	        </div>
        	<!-- /.box-header -->
<div class="box-body no-padding">
    <?= $this->Form->create($<%= $singularVar %>,['type'=>'file']) ?>
	<table class="table" style="width: 90%">
	<tbody>

        
<%
        foreach ($fields as $field) {
            if (in_array($field, $primaryKey)) {
                continue;
            }
            if (isset($keyFields[$field])) {
                $fieldData = $schema->column($field);
                if (!empty($fieldData['null'])) {
				echo '<tr><th>FIELDNAME</th><td><?php';
%>
            echo $this->Form->input('<%= $field %>',['options' => $<%= $keyFields[$field] %>, 'empty' => true,'class'=>'form-control','label'=>false]);
<%				echo '?></td></tr>';
                } else {
				echo '<tr><th>FIELDNAME</th><td><?php';
%>			
            echo $this->Form->input('<%= $field %>', ['options' => $<%= $keyFields[$field] %>, 'empty' => true,'class'=>'form-control','label'=>false]);
<%			echo '?></td></tr>';
                }
                continue;
            }
            if (!in_array($field, ['created', 'modified', 'updated'])) {
                $fieldData = $schema->column($field);
                if (($fieldData['type'] === 'date') && (!empty($fieldData['null']))) {
				echo '<tr><th>FIELDNAME</th><td><?php';
%>
            echo $this->Form->input('<%= $field %>', ['empty' => true,'class'=>'form-control','label'=>false]);
<%			echo '?></td></tr>';
                } else {
				echo '<tr><th>FIELDNAME</th><td><?php';
%>
            echo $this->Form->input('<%= $field %>', ['empty' => true,'class'=>'form-control','label'=>false]);
<%			echo '?></td></tr>';
                }
            }
        }
        if (!empty($associations['BelongsToMany'])) {
            foreach ($associations['BelongsToMany'] as $assocName => $assocData) {
			echo '<tr><th>FIELDNAME</th><td><?php';
%>
            echo $this->Form->input('<%= $assocData['property'] %>._ids', ['options' => $<%= $assocData['variable'] %>, 'empty' => true,'class'=>'form-control','label'=>false]);
<%			echo '?></td></tr>';
            }
        }
%>
       
          </tbody></table>
          <div style="padding-top: 10px;padding-bottom: 50px;padding-left: 5%;padding-right: 5%">
          	<?= 
              $this->Form->button('<i class="fa fa-floppy-o "></i> Guardar', [
														    'type' => 'submit',
														    'escape' => false,
														    'class' =>'btn btn-primary btn-block btn-lg'
															]); ?>
          </div>
              
    		<?= $this->Form->end() ?>

        </div>
        <!-- /.box-body -->
      </div>

	</div>
    <!-- /.col -->
</div>