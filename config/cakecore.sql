/*
Navicat MySQL Data Transfer

Source Server         : asukalap
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : cakecore

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-03-04 18:13:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('1', 'The title', 'This is the article body.', '2016-03-03 22:38:26', null, null);
INSERT INTO `articles` VALUES ('2', 'A title once again', 'And the article body follows.', '2016-03-03 22:38:26', null, null);
INSERT INTO `articles` VALUES ('3', 'Title strikes back', 'This is really exciting! Not.', '2016-03-03 22:38:26', null, null);
INSERT INTO `articles` VALUES ('4', 'vfdvf', '<p>irvj rf oi j 0ro o rfopofg&nbsp;</p>\r\n', '2016-03-04 06:49:20', '2016-03-04 06:51:49', '1');

-- ----------------------------
-- Table structure for `articles_categories`
-- ----------------------------
DROP TABLE IF EXISTS `articles_categories`;
CREATE TABLE `articles_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of articles_categories
-- ----------------------------

-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categories
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'pvargas@enacment.com', '$2y$10$CHgish4fsd5phIUMy3L31uvdIe3nF.9K9ODi2GiK5jbPehfOJBfyK', 'admin', 'upload/avatar/1/2299a8283861f9507e647fe443642d47.jpg', '2016-03-04 04:54:50', '2016-03-05 00:11:46');
INSERT INTO `users` VALUES ('2', 'nzubiaur@enacment.com', '$2y$10$EDvGDJubxdpsbfnxK/57HOmfp9zXaBFmMAgA1J.BQPp18X3JT.AKS', 'admin', null, '2016-03-05 00:13:10', '2016-03-05 00:13:10');
