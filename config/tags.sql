/*
Navicat MySQL Data Transfer

Source Server         : asukalap
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : warroomv2

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-06-24 13:18:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tags`
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tags
-- ----------------------------
INSERT INTO `tags` VALUES ('1', null, 'Uncategorized', 'uncategorized', '', '2009-07-22 03:38:43', null, '2009-07-22 03:34:56');
INSERT INTO `tags` VALUES ('2', null, 'Announcements', 'announcements', '', '2010-05-16 23:57:06', null, '2009-07-22 03:45:37');
INSERT INTO `tags` VALUES ('4', '2', 'Comunicados', 'comunicados', '', '2015-10-20 21:57:53', '2', '2015-10-20 21:57:53');
INSERT INTO `tags` VALUES ('5', '2', 'Casos prácticos', 'casos-practicos', '', '2015-10-20 22:05:22', '2', '2015-10-20 22:05:22');
INSERT INTO `tags` VALUES ('6', '2', 'Incidentes generales', 'incidentes-generales', '', '2015-10-20 22:06:14', '2', '2015-10-20 22:06:14');
INSERT INTO `tags` VALUES ('7', '2', 'Accidente en la red', 'accidente-en-la-red', '', '2015-10-20 22:06:38', '2', '2015-10-20 22:06:38');
INSERT INTO `tags` VALUES ('8', '2', 'Accidente en vivienda', 'accidente-en-vivienda', '', '2015-10-20 22:06:57', '2', '2015-10-20 22:06:57');
INSERT INTO `tags` VALUES ('9', '2', 'Fallo suministro', 'fallo-suministro', '', '2015-10-20 22:07:20', '2', '2015-10-20 22:07:20');
INSERT INTO `tags` VALUES ('10', '2', 'Boicots', 'boicots', '', '2015-10-20 22:07:35', '2', '2015-10-20 22:07:35');
INSERT INTO `tags` VALUES ('11', '2', 'Factura', 'factura', '', '2015-10-20 22:07:51', '2', '2015-10-20 22:07:51');
INSERT INTO `tags` VALUES ('12', '2', 'Twitter', 'twitter', '', '2015-10-20 22:08:01', '2', '2015-10-20 22:08:01');
INSERT INTO `tags` VALUES ('13', '2', 'Incendio', 'incendio', '', '2015-10-20 22:08:27', '2', '2015-10-20 22:08:27');
INSERT INTO `tags` VALUES ('14', '2', 'Explosión', 'explosion', '', '2015-10-20 22:09:07', '2', '2015-10-20 22:09:07');
INSERT INTO `tags` VALUES ('15', '4', 'DGC', 'dgc', 'Dirección General de Comunicación', '2015-10-22 23:02:14', '4', '2015-10-22 23:02:14');
INSERT INTO `tags` VALUES ('16', '4', 'Relación con medios de comunicación', 'relacion-con-medios-de-comunicacion', '', '2015-10-22 23:07:01', '4', '2015-10-22 23:07:01');
