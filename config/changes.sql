/*14 Marzo 2016*/

ALTER TABLE `users`
ADD COLUMN `name`  varchar(255) NULL AFTER `username`,
ADD COLUMN `lastname`  varchar(255) NULL AFTER `name`,
ADD COLUMN `birthday`  date NULL AFTER `lastname`,
ADD COLUMN `about`  text NULL AFTER `role`,
ADD COLUMN `telephone`  varchar(255) NULL AFTER `avatar`;


/*17 Marzo 2016*/
ALTER TABLE `articles` ADD `image` VARCHAR(255) NOT NULL AFTER `body`; 

/*27 Junio 2016*/
ALTER TABLE `crises`
MODIFY COLUMN `Analysis`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `description`,
MODIFY COLUMN `Action`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `Analysis`,
MODIFY COLUMN `Result`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `Action`;

ALTER TABLE `feeds`
ADD COLUMN `file`  varchar(255) NOT NULL AFTER `file_description`;

/*28 Junio 2016*/
ALTER TABLE `connections`
ADD COLUMN `file`  varchar(255) NULL AFTER `email`;

ALTER TABLE `articles`
DROP COLUMN `modified`,
MODIFY COLUMN `user_id`  int(11) NULL DEFAULT NULL AFTER `id`,
ADD COLUMN `parent_id`  int(20) NULL DEFAULT NULL AFTER `image`,
ADD COLUMN `status`  int(1) NULL DEFAULT NULL AFTER `parent_id`,
ADD COLUMN `type`  varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'node' AFTER `status`,
ADD COLUMN `updated_by`  int(11) NULL DEFAULT NULL AFTER `type`,
ADD COLUMN `modified`  datetime NOT NULL AFTER `created`;

RENAME TABLE articles_categories TO articles_tags;

ALTER TABLE `articles_tags`
CHANGE COLUMN `category_id` `tag_id`  int(11) NOT NULL AFTER `article_id`;

ALTER TABLE `articles`
MODIFY COLUMN `type`  varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' AFTER `status`;

/*30 Junio 2016*/
ALTER TABLE `articles`
ADD COLUMN `slug`  varchar(255) NULL AFTER `title`;

INSERT INTO articles (user_id, title, slug, body, parent_id, `status`, type, updated_by, created, modified)
SELECT user_id, title, slug, body, parent_id, `status`, 1, updated_by, created, updated FROM nodes WHERE type = 'page';

INSERT INTO articles (user_id, title, slug, body, parent_id, `status`, type, updated_by, created, modified)
SELECT user_id, title, slug, body, parent_id, `status`, 0, updated_by, created, updated FROM nodes WHERE type = 'blog';

/*01 Julio 2016*/
ALTER TABLE `articles`
ADD COLUMN `next_id`  int NULL AFTER `parent_id`,
ADD COLUMN `before_id`  int NULL AFTER `next_id`;

ALTER TABLE `articles`
MODIFY COLUMN `title`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `user_id`;

/*05 Julio 2016*/
ALTER TABLE `categories`
CHANGE COLUMN `created_on` `created`  date NOT NULL AFTER `description`,
ADD COLUMN `modified`  date NOT NULL AFTER `created`;

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) DEFAULT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Node',
  `foreign_key` int(20) NOT NULL,
  `user_id` int(20) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `comment_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'comment',
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_fk` (`model`,`foreign_key`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('1', null, 'Node', '29', '2', 'Administrator', 'info@enacment.com', '', '201.153.227.64', null, 'comentario', null, '1', '0', 'blog', 'comment', '1', '2', '2015-10-12 18:45:45', '2', '2015-10-12 18:45:45', '2');
INSERT INTO `comments` VALUES ('2', null, 'Node', '38', '4', 'Victor', 'vvalverde@llorenteycuenca.com', '', '201.141.43.80', null, 'Lorem ipsum', null, '1', '0', 'blog', 'comment', '3', '4', '2015-10-23 03:39:42', '4', '2015-10-23 03:39:42', '4');
INSERT INTO `comments` VALUES ('3', null, 'Node', '38', '2', 'Administrator', 'info@enacment.com', '', '189.179.172.133', null, 'probando', null, '1', '0', 'blog', 'comment', '5', '6', '2015-10-26 18:27:50', '2', '2015-10-26 18:27:50', '2');
INSERT INTO `comments` VALUES ('4', null, 'Node', '7', '4', 'Victor', 'vvalverde@llorenteycuenca.com', '', '201.149.18.90', null, 'Prueba de comentario', null, '1', '0', 'page', 'comment', '7', '8', '2015-10-27 20:26:47', '4', '2015-10-27 20:26:47', '4');
INSERT INTO `comments` VALUES ('5', null, 'Node', '37', '2', 'Administrator', 'info@enacment.com', '', '201.102.128.129', null, '&lt;p&gt;probando&lt;/p&gt;\r\n', null, '1', '0', 'blog', 'comment', '9', '10', '2015-11-23 19:16:03', '2', '2015-11-23 19:16:03', '2');


ALTER TABLE `comments`
DROP COLUMN `model`,
DROP COLUMN `foreign_key`,
DROP COLUMN `name`,
DROP COLUMN `email`,
DROP COLUMN `website`,
DROP COLUMN `ip`,
DROP COLUMN `rating`,
DROP COLUMN `comment_type`,
DROP COLUMN `lft`,
DROP COLUMN `rght`,
DROP COLUMN `updated_by`,
DROP COLUMN `created_by`,
CHANGE COLUMN `updated` `modified`  datetime NOT NULL AFTER `type`;

ALTER TABLE `comments`
CHANGE COLUMN `type` `article_id`  int(11) NOT NULL AFTER `user_id`;

ALTER TABLE `comments`
DROP COLUMN `title`,
DROP COLUMN `notify`;

/*11 Julio 2016*/
ALTER TABLE `connections`
ADD COLUMN `country`  varchar(120) NULL AFTER `job_description`;

/*13 Julio 2016*/
CREATE TABLE `countries` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  varchar(250) NOT NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `articles`
ADD COLUMN `country_id`  int(11) NULL AFTER `type`;

ALTER TABLE `crises`
ADD COLUMN `country_id`  int(11) NULL AFTER `user_id`;

ALTER TABLE `users`
ADD COLUMN `country_id`  int(11) NOT NULL AFTER `bio`;

ALTER TABLE `crises`
DROP COLUMN `country`;

ALTER TABLE `connections`
CHANGE COLUMN `country` `country_id`  int(11) NULL DEFAULT NULL AFTER `job_description`;

CREATE TABLE `groups` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) NOT NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `groups`
ADD COLUMN `role_id`  int(11) NULL AFTER `name`;

ALTER TABLE `groups`
MODIFY COLUMN `name`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `id`,
ADD COLUMN `country_id`  int(11) NOT NULL AFTER `role_id`;







