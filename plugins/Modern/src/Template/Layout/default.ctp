<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GNF War Rooms</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
   	<?= $this->Html->css('bootstrap.min.css');?>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme style -->
    <?= $this->Html->css('AdminLTE.min.css'); ?>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <?= $this->Html->css('skins/_all-skins.min.css'); ?>
    <!-- iCheck -->
    <?= $this->Html->css('iCheck/flat/blue.css'); ?>
    <!-- Morris chart -->
    <?= $this->Html->css('morris/morris.css'); ?>
    <!-- jvectormap -->
    <?= $this->Html->css('jvectormap/jquery-jvectormap-1.2.2.css'); ?>
    <!-- Date Picker -->
    <?= $this->Html->css('datepicker/datepicker3.css'); ?>
    <!-- Daterange picker -->
    <?= $this->Html->css('daterangepicker/daterangepicker-bs3.css'); ?>
    
    <!-- jQuery 2.1.4 -->
    <?= $this->Html->script('jQuery-2.1.4.min.js');?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><?= $this->Html->image('logo-mini.png', ['class'=>'img-responsive']); ?></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg text-center"><?= $this->Html->image('logo-header.png', ['class'=>'img-responsive']); ?></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

			<!-- Tasks: style can be found in dropdown.less -->

              <!-- Tasks: style can be found in dropdown.less -->

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
               	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
               		<?php
               			if ($authUser['image']){
                  		
                  		 echo $this->Html->image('/'.$authUser['image'], [ 'class'=>'user-image']);
                  	}else{
                  		
                  		echo $this->Html->image('default-avatar.png', [ 'class'=>'user-image']);
                  	}

                  	?>
                  
                  <span class="hidden-xs"><?= $authUser['name']; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                  	<?php

                  		if($authUser['image']){
							echo $this->Html->image('/'.$authUser['image'], [ 'class'=>'img-circle', 'alt'=>'User Image']);
						}else{
							echo $this->Html->image('/img/default-avatar.png', [ 'class'=>'img-circle', 'alt'=>'User Image']);
						}

                  	?>
                    
                    <p>
                      <?= $authUser['name']; ?> 
                  </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="col-xs-6 text-center">
                      <?= $this->Html->link('<i class="fa fa-user"></i> Mi Perfil', 
    						array('controller' => 'users', 'action' => 'view', $authUser['id']), 
    						array('escape' => false));
                      ?>
                    </div>
                    <div class="col-xs-6 text-center">
                      <?= $this->Html->link('<i class="fa fa-sign-out"></i> Salir', 
    						array('controller' => 'users', 'action' => 'logout'), 
    						array('escape' => false));
                      ?>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  
              
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
            <?php

            	if($authUser['image']){
					echo $this->Html->image('/'.$authUser['image'], [ 'class'=>'img-circle', 'alt'=>'User Image']);
				}else{
					echo $this->Html->image('/img/default-avatar.png'.$authUser['image'], [ 'class'=>'img-circle', 'alt'=>'User Image']);
				}

            ?>
            </div>
            <div class="pull-left info">
              <h5>
              	  <?= $this->Html->link($authUser['name'], 
    						array('controller' => 'users', 'action' => 'view', $authUser['id']), 
    						array('escape' => false));
                      ?>
</h5>
            </div>
          </div>
          <!-- search form -->
          <?php echo $this -> Form -> create('Crises', array('url' => array('controller' => 'Crises', 'action' => 'find'), 'class' => 'sidebar-form')); ?>
            <div class="input-group">
              <input type="text" name="term" class="form-control" placeholder="Buscar..." required="required">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          <?php 
          	echo $this -> Form -> end();
			
			//MENU CONSTRUCTION
			$cont =  $this->request->params['controller'];
			$action =  $this->request->params['action'];
			
			if(($cont=='Pages')||($cont=='Articles')){
				$url = $action;
				if($action != 'manual'){
					$url = $cont;
				}
			}else{
				$url = $cont;
			}
			
			$stylec = $styled = $stylem = $styles = $styleb = $stylecat = $stylecom = $styletag = $styleuse = $stylecoun = $styleuseg = 'treeview';
			
			switch($url){
				case 'Crises':
					$stylec = 'active treeview';
					break;
				case 'Connections':
					$styles = 'active treeview';
					break;
				case 'manual':
					$stylem = 'active treeview';
					break;
				case 'Articles':
					$styleb = 'active treeview';
					break;
				case 'Categories':
					$stylecat = 'active treeview';
					break;
				case 'Comments':
					$stylecom = 'active treeview';
					break;
				case 'Tags':
					$styletag = 'active treeview';
					break;
				case 'Users':
					$styleuse = 'active treeview';
					break;
				case 'Groups':
					$styleuseg = 'active treeview';
					break;
				case 'Countries':
					$stylecoun = 'active treeview';
					break;
				default:
					$styled = 'active treeview';
			}
          
          ?>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
				<li class="<?php echo $styled;?>">
					<?= $this->Html->link('<i class="fa fa-home"></i> <span>Dashboard</span>', ['controller'=>'Pages', 'action'=>'dashboard'], ['escape'=>false]) ?>
				</li>
				<?php if($authUser['role_id']!= 3){ ?>
				<li class="<?php echo $stylem;?>">
				  <a href="#">
					 <i class="fa fa-book"></i><span>Manual de Comunicación en situaciones de Crisis</span>
				  </a>
				  <ul class="treeview-menu">
				  	<li id="procedimiento-general">
				  		<a href="#" class="nav-link">
							<i class="fa fa-life-saver"></i> Procedimiento General
						</a>
						<ul class="treeview-menu">
				  			<li id="consideraciones-previas">
				  				<?= $this->Html->link('<span>Consideraciones Previas</span>', ['controller'=>'Articles', 'action'=>'manual', 'consideraciones-previas'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="claves-de-uso">
				  				<?= $this->Html->link('<span>Claves de Uso</span>', ['controller'=>'Articles', 'action'=>'manual', 'claves-de-uso'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="protocolo">
				  				<?= $this->Html->link('<span>Protocolos de actuación</span>', ['controller'=>'Articles', 'action'=>'manual', 'protocolo'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="la-fase-inicial-de-la-gestion-de-la-crisis">
				  				<?= $this->Html->link('<span>La fase inicial de la gestión de la crisis</span>', ['controller'=>'Articles', 'action'=>'manual', 'la-fase-inicial-de-la-gestion-de-la-crisis'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="actuacion-ante-los-principales-stakeholders">
				  				<?= $this->Html->link('<span>Actuación ante los principales stakeholders</span>', ['controller'=>'Articles', 'action'=>'manual', 'actuacion-ante-los-principales-stakeholders'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  		</ul>
				  	</li>
				  	<li id="manual-gestion-comunicacion-crisis">
				  		<a href="#" class="nav-link">
							<i class="fa fa-book"></i> Gestión de Crisis
						</a>
						<ul class="treeview-menu">
				  			<li id="objeto-del-manual">
				  				<?= $this->Html->link('<span>Objeto del Manual</span>', ['controller'=>'Articles', 'action'=>'manual', 'objeto-del-manual'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="la-comunicacion-en-el-comite-de-gestion-de-crisis-cgc">
				  				<a href="#" class="nav-link">
									<i class="fa fa-book"></i> La comunicación en el Comité de Gestión de Crisis (CGC)
								</a>
								<ul class="treeview-menu">
						  			<li id="gestion-de-comunicacion-nivel-1-verde">
						  				<?= $this->Html->link('<span>Gestión de Comunicación. Nivel 1 - Verde</span>', ['controller'=>'Articles', 'action'=>'manual', 'gestion-de-comunicacion-nivel-1-verde'], ['escape'=>false, 'class'=>'nav-link']) ?>
						  			</li>
						  			<li id="gestion-comunicacion-nivel-2-amarillo">
						  				<?= $this->Html->link('<span>Gestión de Comunicación. Nivel 2 - Amarillo</span>', ['controller'=>'Articles', 'action'=>'manual', 'gestion-comunicacion-nivel-2-amarillo'], ['escape'=>false, 'class'=>'nav-link']) ?>
						  			</li>
						  			<li id="gestion-comunicacion-nivel-3-naranja">
						  				<?= $this->Html->link('<span>Gestión de Comunicación. Nivel 3 - Naranja</span>', ['controller'=>'Articles', 'action'=>'manual', 'gestion-comunicacion-nivel-3-naranja'], ['escape'=>false, 'class'=>'nav-link']) ?>
						  			</li>
						  			<li id="gestion-comunicacion-nivel-4-rojo">
						  				<?= $this->Html->link('<span>Gestión de Comunicación. Nivel 4 - Rojo</span>', ['controller'=>'Articles', 'action'=>'manual', 'gestion-comunicacion-nivel-4-rojo'], ['escape'=>false, 'class'=>'nav-link']) ?>
						  			</li>
						  		</ul>
				  			</li>
				  			<li id="normas-para-portavoces">
				  				<?= $this->Html->link('<span>Normas para portavoces</span>', ['controller'=>'Articles', 'action'=>'manual', 'normas-para-portavoces'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="como-preparar-una-comparecencia-publica">
				  				<?= $this->Html->link('<span>Cómo preparar una comparecencia pública</span>', ['controller'=>'Articles', 'action'=>'manual', 'como-preparar-una-comparecencia-publica'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="como-actuar-ante-publicos-hostiles">
				  				<?= $this->Html->link('<span>Cómo actuar ante públicos hostiles</span>', ['controller'=>'Articles', 'action'=>'manual', 'como-actuar-ante-publicos-hostiles'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="como-actuar-en-el-lugar-de-los-hechos">
				  				<?= $this->Html->link('<span>Cómo actuar en el lugar de los hechos</span>', ['controller'=>'Articles', 'action'=>'manual', 'como-actuar-en-el-lugar-de-los-hechos'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="relaciones-con-medios-de-comunicacion">
				  				<?= $this->Html->link('<span>Relaciones con medios de comunicación</span>', ['controller'=>'Articles', 'action'=>'manual', 'relaciones-con-medios-de-comunicacion'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  			<li id="procedimiento-operativo-para-dircom-en-el-lugar-de-los-hechos">
				  				<?= $this->Html->link('<span>Procedimiento operativo para DIRCOM en el lugar de los hechos</span>', ['controller'=>'Articles', 'action'=>'manual', 'procedimiento-operativo-para-dircom-en-el-lugar-de-los-hechos'], ['escape'=>false, 'class'=>'nav-link']) ?>
				  			</li>
				  		</ul>
					</li>
					<li id="manual-gestion-comunicacion-crisis">
						<?= $this->Html->link('<span>Fase de control del incidente</span>', ['controller'=>'Articles', 'action'=>'manual', 'fase-de-control-del-incidente'], ['escape'=>false, 'class'=>'nav-link']) ?>
					</li>
					<li>
				  		<?= $this->Html->link('<i class="fa fa-download"></i> Descarga Manual', '/uploads/Manual-de-Comunicacion-de-Crisis-GNF.pdf', ['escape'=>false, 'class'=>'nav-link']) ?>
				  	</li>
				  </ul>
				</li>
				<?php } ?>
				<li class="<?php echo $stylec;?>">
					<?= $this->Html->link('<i class="fa fa-exclamation-triangle"></i> <span>War Rooms</span>', ['controller'=>'Crises', 'action'=>'index'], ['escape'=>false]) ?>
				</li>
				<li class="<?php echo $styles;?>">
					<?= $this->Html->link('<i class="fa fa-users"></i> <span>Directorio Stakeholders</span>', ['controller'=>'Connections', 'action'=>'index'], ['escape'=>false]) ?>
				</li>
				<li class="<?php echo $styleb;?>">
					<?= $this->Html->link('<i class="fa fa-file-o"></i> <span>Trainings y Materiales</span>', ['controller'=>'Articles', 'action'=>'index'], ['escape'=>false]) ?>
				</li>
				<?php
					if($authUser['role_id']==1){
				?>
					<li class="<?php echo $styleuse;?>">
						<?= $this->Html->link('<i class="fa fa-user"></i> <span>Usuarios</span>', ['controller'=>'Users', 'action'=>'index'], ['escape'=>false]) ?>
					</li>
					<li class="<?php echo $styleuseg;?>">
						<?= $this->Html->link('<i class="fa fa-users"></i> <span>Grupos</span>', ['controller'=>'Groups', 'action'=>'index'], ['escape'=>false]) ?>
					</li>
					<li class="<?php echo $stylecat;?>">
						<?= $this->Html->link('<i class="fa fa-tag"></i> <span>Categorías</span>', ['controller'=>'Categories', 'action'=>'index'], ['escape'=>false]) ?>
					</li>
					<li class="<?php echo $stylecom;?>">
						<?= $this->Html->link('<i class="fa fa-comments"></i> <span>Comentarios</span>', ['controller'=>'Comments', 'action'=>'index'], ['escape'=>false]) ?>
					</li>
					<li class="<?php echo $styletag;?>">
						<?= $this->Html->link('<i class="fa fa-tags"></i> <span>Tags</span>', ['controller'=>'Tags', 'action'=>'index'], ['escape'=>false]) ?>
					</li>
					<li class="<?php echo $stylecoun;?>">
						<?= $this->Html->link('<i class="fa fa-globe"></i> <span>Países</span>', ['controller'=>'Countries', 'action'=>'index'], ['escape'=>false]) ?>
					</li>
				<?php
					}
				?>
		</ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
		<div class="row">
		<?= $this->Flash->render() ?>
		</div>
		<div class="row">
		<?= $this->fetch('content') ?>
		</div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          War Room v 0.3</b>
        </div>
          &copy;  Gas Natural Fenosa. <strong>Información Confidencial</strong>
      </footer>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <?= $this->Html->script('bootstrap.min');?>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <?= $this->Html->script('morris/morris.min.js');?>
    <!-- Sparkline -->
    <?= $this->Html->script('sparkline/jquery.sparkline.min.js');?>
    <!-- jvectormap -->
    <?= $this->Html->script('jvectormap/jquery-jvectormap-1.2.2.min.js');?>
    <?= $this->Html->script('jvectormap/jquery-jvectormap-world-mill-en.js');?>
    <!-- jQuery Knob Chart -->
    <?= $this->Html->script('knob/jquery.knob.js');?>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
     <?= $this->Html->script('daterangepicker/daterangepicker.js');?>
    <!-- datepicker -->
     <?= $this->Html->script('datepicker/bootstrap-datepicker.js');?>
   
    <!-- Slimscroll -->
    <?= $this->Html->script('slimScroll/jquery.slimscroll.min.js');?>
    <!-- FastClick -->
    <?= $this->Html->script('fastclick/fastclick.min.js');?>
    <!-- AdminLTE App -->
    <?= $this->Html->script('app.min');?>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

    <!-- AdminLTE for demo purposes -->
    <?= $this->Html->script('demo.js');?>
    
    <!-- Adding Checkeditor -->
    <?= $this->Html->script('ckeditor/ckeditor.js') ?>
    <!-- Select2 -->
	<?= $this->Html->css('/plugins/select2/select2.min.css'); ?>
    <?= $this->Html->script('/plugins/select2/select2.min.js') ?>
<script>
$(document).ready(function() {
  $(".select2").select2();
});
</script>
  </body>
</html>
