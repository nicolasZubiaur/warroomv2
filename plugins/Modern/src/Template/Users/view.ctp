<?php 
use Cake\I18n\Time;

if(isset($authUser->timezone)){
  $time->timezone = $authUser->timezone;
}
?>
<div class="row"><div class="col-md-12">
        <?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar',
    ['action' => 'edit', $user->id],
    ['escape' => false,
	'class'=>'btn btn-app']
); ?> 
<?= $this->Form->postLink(
						$this->Html->tag('i', '', array('class' => 'fa fa-trash')). " Borrar",
						array('action' => 'delete', $user->id),
						array('escape'=>false , 'class' => 'btn btn-app','confirm' => __('Are you sure you want to delete # {0}?', $user->id))) ;?>	
	<?= $this->Html->link(
    '<i class="fa fa-search"></i> Ver Todos',
    ['action' => 'index'],
    ['escape' => false,
	'class'=>'btn btn-app']
); ?> 
	<?= $this->Html->link(
    '<i class="fa fa-plus"></i> Nuevo',
    ['action' => 'add'],
    ['escape' => false,
	'class'=>'btn btn-app']
); ?> 
	
</div></div>

<section class="content-header">
          <h1>
            Perfil de Usuario
          </h1>
          <ol class="breadcrumb">
            <li><?php echo $this->Html->link('<i class="fa fa-home"></i> <span>Dashboard</span>', ['controller'=>'Pages', 'action'=>'dashboard'], ['escape'=>false]); ?></li>
            <li><a href="#">Perfil de Usuario</a></li>
            <li class="active"><?= h($user->username); ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <?php
                  	if ($user->image){
                  		
                  		 echo $this->Html->image('/'.$user->image, [ 'class'=>'profile-user-img img-responsive ']);
                  	}else{
                  		
                  		echo $this->Html->image('default-avatar.png', [ 'class'=>'profile-user-img img-responsive ']);
                  	}
						
                  
                   ?>
                  <h3 class="profile-username text-center"><?= h($user->name) ?> <?= h($user->lastname) ?></h3>
                  <p class="text-muted text-center"><i class="fa fa-user pull-left" style="display:inline"></i> <?= h($user->username) ?></p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item text-muted text-center">
                      <i class="fa fa-briefcase pull-left" style="display:inline"></i> <?= h($user->job_description) ?>
                    </li>
                    <li class="list-group-item text-muted text-center">
                      <i class="fa fa-phone pull-left" style="display:inline"></i> <?= h($user->telephone) ?>
                    </li>
                    <li class="list-group-item text-muted text-center">
                      <i class="fa fa-envelope pull-left" style="display:inline"></i> <?= $this->Text->autoLinkEmails(h($user->email)) ?>
                    </li> 
                  </ul>
<?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar',
    ['action' => 'edit', $user->id],
    ['escape' => false,
  'class'=>'btn btn-primary btn-block']
); ?> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Acerca De <?= h($user->name) ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <?php echo $user->bio ?>

                  <hr>

                  <strong><i class="fa fa-clock-o margin-r-5"></i> Actualizado</strong>
                  
                  <?php

                   $now = new Time($user->modified); 
                    echo $now->timeAgoInWords(
                        ['format' => 'MMM d, YYY', 'end' => '+1 week']
                    );
                   ?>
                   <span class="pull-right">
<strong><i class="fa fa-clock-o margin-r-5"></i> Creado</strong>
                                     <?php

                   $now = new Time($user->created); 
                    echo $now->timeAgoInWords(
                        ['format' => 'MMM d, YYY', 'end' => '+1 week']
                    );
                   ?> </span>
                  <p>
                  
					
                  </p>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

          </div><!-- /.row -->

        </section><!-- /.content -->