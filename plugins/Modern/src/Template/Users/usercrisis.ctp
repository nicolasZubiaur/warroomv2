<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user,['type'=>'file']) ?>
    <fieldset>
        <legend><?= __('Invitar Usuario') ?></legend>
        <?php
        
        $status = ['0'=>'Inactivo', '1'=>'Activo'];
        
            echo $this->Form->input('username');
            echo $this->Form->input('name');
			echo $this->Form->input('email');
            echo $this->Form->input('role_id' , ['value'=>'2', 'type'=>'hidden']);
			echo $this->Form->input('status', ['value'=>'1', 'type'=>'hidden']);
        ?>
        <select name="crises[_ids][]" class="hidden" id="crises-ids">
        	<option value="<?php echo $crisis->id  ?>"><?php echo $crisis->id  ?></option>
        </select>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
