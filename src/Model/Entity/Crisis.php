<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Crisis Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $Analysis
 * @property string $Action
 * @property string $Result
 * @property string $category
 * @property string $severity
 * @property int $user_id
 * @property string $status
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $created
 * @property string $terms
 * @property string $city
 * @property string $address
 * @property string $country
 * @property string $google_link
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\CrisisType[] $crisis_types
 * @property \App\Model\Entity\Feed[] $feeds
 * @property \App\Model\Entity\Task[] $tasks
 * @property \App\Model\Entity\Connection[] $connections
 */
class Crisis extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
