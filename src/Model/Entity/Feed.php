<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Feed Entity.
 *
 * @property int $id
 * @property string $post
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $crisis_id
 * @property \App\Model\Entity\Crisis $crisis
 * @property int $parent_id
 * @property \App\Model\Entity\ParentFeed $parent_feed
 * @property int $stackholder_connection
 * @property int $assign_user_id
 * @property \App\Model\Entity\AssignUser $assign_user
 * @property int $connection_id
 * @property \App\Model\Entity\Connection $connection
 * @property string $matter
 * @property string $twitter_code
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $created
 * @property int $type
 * @property \App\Model\Entity\ChildFeed[] $child_feeds
 */
class Feed extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'file_field' => true,
    ];
}
