<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CrisisType Entity.
 *
 * @property int $id
 * @property int $crisis_id
 * @property \App\Model\Entity\Crisis $crisis
 * @property string $name
 * @property string $value
 * @property \App\Model\Entity\Type $type
 */
class CrisisType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
