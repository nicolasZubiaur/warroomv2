<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Connection Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $office_phone
 * @property string $mobile_phone
 * @property string $job_description
 * @property string $email
 * @property string $notes
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $created
 * @property \App\Model\Entity\Feed[] $feeds
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\Crisis[] $crises
 */
class Connection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'file_field' => true,
    ];
}
