<?php
namespace App\Model\Table;

use App\Model\Entity\Crisis;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Crises Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $CrisisTypes
 * @property \Cake\ORM\Association\HasMany $Feeds
 * @property \Cake\ORM\Association\HasMany $Tasks
 * @property \Cake\ORM\Association\BelongsToMany $Connections
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class CrisesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('crises');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
		
		$this->belongsTo('Countries');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CrisisTypes', [
            'foreignKey' => 'crisis_id',
			'dependent' => false,
        ]);
        $this->hasMany('Feeds', [
            'foreignKey' => 'crisis_id',
			'dependent' => false,
			'sort' => array('Feeds.created'=>'DESC')
        ]);
        $this->hasMany('Tasks', [
            'foreignKey' => 'crisis_id',
            'dependent' => false,
            'sort' => array('Tasks.created'=>'DESC')
        ]);
        $this->belongsToMany('Connections', [
            'foreignKey' => 'crisis_id',
            'targetForeignKey' => 'connection_id',
            'joinTable' => 'connections_crises',
            'unique' => 'keepExisting',
        ]);
		
		$this->belongsToMany('Tags', [
            'foreignKey' => 'crisis_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'crises_tags',
            'unique' => 'keepExisting',
        ]);
		
       $this->belongsToMany('Users', [
            'foreignKey' => 'crisis_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_crises',
            'unique' => 'keepExisting'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator            
            ->allowEmpty('Analysis');

        $validator            
            ->allowEmpty('Action');

        $validator
            ->allowEmpty('Result');

        $validator
            ->allowEmpty('category');

        $validator
            ->allowEmpty('severity');

        $validator
            ->allowEmpty('status');

        $validator
            ->allowEmpty('terms');

        $validator
            ->allowEmpty('city');

        $validator
            ->allowEmpty('address');

        $validator
            ->requirePresence('country_id', 'create')
            ->notEmpty('country_id');

        $validator
            ->allowEmpty('google_link');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
		$rules->add($rules->existsIn(['country_id'], 'Countries'));
        return $rules;
    }
}
