<?php
namespace App\Model\Table;

use App\Model\Entity\Feed;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Feeds Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Crises
 * @property \Cake\ORM\Association\BelongsTo $ParentFeeds
 * @property \Cake\ORM\Association\BelongsTo $AssignUsers
 * @property \Cake\ORM\Association\BelongsTo $Connections
 * @property \Cake\ORM\Association\HasMany $ChildFeeds
 */
class FeedsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('feeds');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Crises', [
            'foreignKey' => 'crisis_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ParentFeeds', [
            'className' => 'Feeds',
            'foreignKey' => 'parent_id'
        ]);
        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'INNER'
        ]);
		
		$this->addBehavior('Xety/Cake3Upload.Upload', [
		        'fields' => [
		            'file' => [
		                'path' => 'upload/feed/:md5',
		                'defaultFile' => 'img/photo1.png'
		            ]
		        ]
		    ]
		);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('post');

        $validator
            ->integer('stackholder_connection')            
            ->allowEmpty('stackholder_connection');

        $validator
            ->allowEmpty('matter');

        $validator
            ->allowEmpty('twitter_code');

        $validator
            ->integer('type')
            ->allowEmpty('type');
			
		$validator
			->allowEmpty('file_file')
		  	->add('file_file', [
                'mimeType' => [
                    'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                    'message' => __("Tipo de archivo no permitido."),
                    'on' => function ($context) {
                            return !empty($context['data']['file_file']['name']);
                    }
                ],
                'fileExtension' => [
                    'rule' => ['extension', ['jpg', 'jpeg', 'png']],
                    'message' => __("Las extensiones permitidas son {0}.", '.jpg, .jpeg and .png'),
                    'on' => function ($context) {
                            return !empty($context['data']['file_file']['name']);
                    }
                ]]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['crisis_id'], 'Crises'));
        $rules->add($rules->existsIn(['parent_id'], 'ParentFeeds'));
        $rules->add($rules->existsIn(['connection_id'], 'Connections'));
        return $rules;
    }
}
