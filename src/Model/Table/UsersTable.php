<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
		
		$this->belongsTo('Countries');
		
		$this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
		
		 $this->belongsToMany('Crises', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'crisis_id',
            'joinTable' => 'users_crises',
            'unique' => 'keepExisting'
        ]);
		
		 $this->belongsToMany('Groups', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'group_id',
            'joinTable' => 'users_groups'
        ]);
		
		$this->addBehavior('Xety/Cake3Upload.Upload', [
	        'fields' => [
	            'image' => [
	                'path' => 'upload/avatar/:id/:md5',
	                'defaultFile' => 'img/user.jpg'
	            ]
	        ]
	    ]
	);
		
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        	$validator
            ->notEmpty('username', 'A username is required')
			->notEmpty('email', 'A email is required')
            ->notEmpty('password', 'A password is required')
			->notEmpty('name', 'A name is required')
            ->notEmpty('role_id', 'A role is required');
			
			$validator
			->allowEmpty('image_file')
		  	->add('image_file', [
                'mimeType' => [
                    'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                    'message' => __("Tipo de archivo no permitido."),
                    'on' => function ($context) {
                            return !empty($context['data']['image_file']['name']);
                    }
                ],
                'fileExtension' => [
                    'rule' => ['extension', ['jpg', 'jpeg', 'png']],
                    'message' => __("Las extensiones permitidas son {0}.", '.jpg, .jpeg and .png'),
                    'on' => function ($context) {
                            return !empty($context['data']['image_file']['name']);
                    }
                ]]);
				
			return $validator;
    }

    /**
     * Invite validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationInvite(Validator $validator)
    {
        	$validator
            ->notEmpty('username', 'A username is required')
			->notEmpty('email', 'A email is required')
			->notEmpty('name', 'A name is required')
            ->notEmpty('role_id', 'A role is required');
				
			return $validator;
    }
	
	public function validationPassword(Validator $validator )
    {
 
        $validator
            ->add('email', 'valid', ['rule' => 'email'])
            ->requirePresence('email', 'create')
            ->notEmpty('email');
			
		 $validator
            ->requirePresence('country_id', 'create')
            ->notEmpty('country_id');
 
 
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
		$rules->add($rules->isUnique(['email']));
		$rules->add($rules->existsIn(['country_id'], 'Countries'));
		
        return $rules;
    }
}
