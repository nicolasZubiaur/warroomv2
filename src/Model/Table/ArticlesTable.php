<?php
namespace App\Model\Table;

use App\Model\Entity\Article;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $ParentArticles
 * @property \Cake\ORM\Association\HasMany $ChildArticles
 * @property \Cake\ORM\Association\BelongsToMany $Tags
 */
class ArticlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('articles');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
		
		$this->hasMany('Comments', [
            'foreignKey' => 'article_id',
            'dependent' => true,
            'sort'=>['Comments.created' => 'DESC']
        ]);
		
		$this->belongsTo('Countries');
		
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('ParentArticles', [
            'className' => 'Articles',
            'foreignKey' => 'parent_id'
        ]);
        $this->belongsTo('ChildArticles', [
            'className' => 'Articles',
            'foreignKey' => 'next_id',
        ]);
		$this->belongsTo('BeforeArticles', [
            'className' => 'Articles',
            'foreignKey' => 'before_id'
        ]);
        $this->belongsToMany('Tags', [
            'foreignKey' => 'article_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'articles_tags'
        ]);
		$this->addBehavior('Xety/Cake3Upload.Upload', [
	        'fields' => [
	            'image' => [
	                'path' => 'upload/articles/:id/:md5',
	                'defaultFile' => 'img/photo1.png'
	            ]
	        ]
	    ]
		);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('body');

        $validator
            ->allowEmpty('image');

        $validator
            ->integer('status')
            ->allowEmpty('status');
		
		 $validator
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');
			
		$validator
            ->requirePresence('country_id', 'create')
            ->notEmpty('country_id');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->integer('updated_by')
            ->allowEmpty('updated_by');
			
		$validator
			->allowEmpty('image_file')
		  	->add('image_file', [
                'mimeType' => [
                    'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                    'message' => __("Tipo de archivo no permitido."),
                    'on' => function ($context) {
                            return !empty($context['data']['image_file']['name']);
                    }
                ],
                'fileExtension' => [
                    'rule' => ['extension', ['jpg', 'jpeg', 'png']],
                    'message' => __("Las extensiones permitidas son {0}.", '.jpg, .jpeg and .png'),
                    'on' => function ($context) {
                            return !empty($context['data']['image_file']['name']);
                    }
                ]]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['parent_id'], 'ParentArticles'));
		$rules->add($rules->existsIn(['next_id'], 'ChildArticles'));
		$rules->add($rules->existsIn(['before_id'], 'BeforeArticles'));
		$rules->add($rules->existsIn(['country_id'], 'Countries'));
		$rules->add($rules->isUnique(['slug']));
        return $rules;
    }
}
