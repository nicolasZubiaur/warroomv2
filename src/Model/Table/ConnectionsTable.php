<?php
namespace App\Model\Table;

use App\Model\Entity\Connection;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Connections Model
 *
 * @property \Cake\ORM\Association\HasMany $Feeds
 * @property \Cake\ORM\Association\BelongsToMany $Categories
 * @property \Cake\ORM\Association\BelongsToMany $Crises
 */
class ConnectionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('connections');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Feeds', [
            'foreignKey' => 'connection_id'
        ]);
		$this->belongsTo('Countries');
        $this->belongsToMany('Categories', [
            'foreignKey' => 'connection_id',
            'targetForeignKey' => 'category_id',
            'joinTable' => 'connections_categories'
        ]);
        $this->belongsToMany('Crises', [
            'foreignKey' => 'connection_id',
            'targetForeignKey' => 'crisis_id',
            'joinTable' => 'connections_crises'
        ]);
		
		$this->addBehavior('Xety/Cake3Upload.Upload', [
		        'fields' => [
		            'file' => [
		                'path' => 'upload/avatar/:md5',
		                'defaultFile' => 'img/default-avatar.png'
		            ]
		        ]
		    ]
		);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('office_phone');

        $validator
            ->allowEmpty('mobile_phone');

        $validator
            ->allowEmpty('job_description');

        $validator
            ->email('email')
			->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->allowEmpty('notes');
			
		$validator
			->allowEmpty('file_file')
		  	->add('file_file', [
                'mimeType' => [
                    'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                    'message' => __("Tipo de archivo no permitido."),
                    'on' => function ($context) {
                            return !empty($context['data']['file_file']['name']);
                    }
                ],
                'fileExtension' => [
                    'rule' => ['extension', ['jpg', 'jpeg', 'png']],
                    'message' => __("Las extensiones permitidas son {0}.", '.jpg, .jpeg and .png'),
                    'on' => function ($context) {
                            return !empty($context['data']['file_file']['name']);
                    }
                ]]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
}
