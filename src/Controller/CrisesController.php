<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Email\Email;
use Cake\Event\Event;

/**
 * Crises Controller
 *
 * @property \App\Model\Table\CrisesTable $Crises
 */
class CrisesController extends AppController
{
	public $components = array(
	    'UserPermissions.UserPermissions'
	);

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    	if ($this->request->is(['patch', 'post', 'put'])){
			if(($this->request->data['severity'])&&($this->request->data['country'])){
				$sev = $this->request->data['severity'];
				$coun = $this->request->data['country'];
				if(($this->Auth->user('role_id')==2)||($this->Auth->user('role_id')== 4)){
					$crises = $this->Crises->find('all', [
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
			            'conditions'=>['Crises.status'=>'Abierta', 'Crises.severity'=>$sev, 'Crises.country_id'=>$coun, 'Crises.user_id'=>$this->Auth->user('id')]
			        ]);
					$crises2 = $this->Crises->find('all',[
			            'contain'=>['Countries'],
			            'order' => ['Crises.created'=>'DESC'],
						'conditions'=>['Crises.status'=>'Abierta', 'Crises.severity'=>$sev, 'Crises.country_id'=>$coun]])->join([
					        't' => [
					            'table' => 'users',
					            'type' => 'INNER',
					            'conditions' => ['t.id'=>$this->Auth->user('id')],
					        ],
					        'tl' => [
					            'table' => 'users_crises',
					            'type' => 'INNER',
					            'conditions' => ['tl.crisis_id = Crises.id','tl.user_id = t.id'],
					        ]
					        
					    ]);
						
					$crises->union($crises2);
				}else{
					$crises = $this->Crises->find('all', [
			            'conditions'=>['Crises.status'=>'Abierta', 'Crises.severity'=>$sev, 'Crises.country_id'=>$coun],
			            'contain'=>['Countries'],
			            'order' => ['Crises.created'=>'DESC'],
			            'sort' => ['Crises.created'=>'DESC']
			        ]);
				}	
			}else if(($this->request->data['severity'])&&(!$this->request->data['country'])){
				$sev = $this->request->data['severity'];
				if(($this->Auth->user('role_id')==2)||($this->Auth->user('role_id')== 4)){
					$crises = $this->Crises->find('all', [
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
			            'conditions'=>['Crises.status'=>'Abierta', 'Crises.severity'=>$sev, 'Crises.user_id'=>$this->Auth->user('id')]
			        ]);
					
					$crises2 = $this->Crises->find('all',[
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
						'conditions'=>['Crises.status'=>'Abierta', 'Crises.severity'=>$sev]])->join([
					        't' => [
					            'table' => 'users',
					            'type' => 'INNER',
					            'conditions' => ['t.id'=>$this->Auth->user('id')],
					        ],
					        'tl' => [
					            'table' => 'users_crises',
					            'type' => 'INNER',
					            'conditions' => ['tl.crisis_id = Crises.id','tl.user_id = t.id'],
					        ]
					        
					    ]);
						
					$crises->union($crises2);
				}else{
					$crises = $this->Crises->find('all', [
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
			            'conditions'=>['Crises.status'=>'Abierta', 'Crises.severity'=>$sev]
			        ]);
				}	
			}else if((!$this->request->data['severity'])&&($this->request->data['country'])){				
				$coun = $this->request->data['country'];
				if(($this->Auth->user('role_id')==2)||($this->Auth->user('role_id')== 4)){
					$crises = $this->Crises->find('all', [
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
			            'conditions'=>['Crises.status'=>'Abierta', 'Crises.country_id'=>$coun, 'Crises.user_id'=>$this->Auth->user('id')]
			        ]);
					
					$crises2 = $this->Crises->find('all',[
					'contain'=>['Countries'],
					'order' => ['Crises.created'=>'DESC'],
					'conditions'=>['Crises.status'=>'Abierta', 'Crises.country_id'=>$coun]])->join([
					        't' => [
					            'table' => 'users',
					            'type' => 'INNER',
					            'conditions' => ['t.id'=>$this->Auth->user('id')],
					        ],
					        'tl' => [
					            'table' => 'users_crises',
					            'type' => 'INNER',
					            'conditions' => ['tl.crisis_id = Crises.id','tl.user_id = t.id'],
					        ]
					        
					    ]);
					
					$crises->union($crises2);
				}else{
					$crises = $this->Crises->find('all', [
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
			            'conditions'=>['Crises.status'=>'Abierta', 'Crises.country_id'=>$coun]
			        ]);
				}
			}else{
				if(($this->Auth->user('role_id')==2)||($this->Auth->user('role_id')== 4)){
					
					$crises = $this->Crises->find('all', [
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
			            'conditions'=>['Crises.status'=>'Abierta','Crises.user_id'=>$this->Auth->user('id')]
			        ]);
					
					$crises2 = $this->Crises->find('all',[
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
						'conditions'=>['Crises.status'=>'Abierta']])->join([
					        't' => [
					            'table' => 'users',
					            'type' => 'INNER',
					            'conditions' => ['t.id'=>$this->Auth->user('id')],
					        ],
					        'tl' => [
					            'table' => 'users_crises',
					            'type' => 'INNER',
					            'conditions' => ['tl.crisis_id = Crises.id','tl.user_id = t.id'],
					        ]
					        
					    ]);
						
					$crises->union($crises2);
					
				}else{
					$crises = $this->Crises->find('all', [
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
			            'conditions'=>['Crises.status'=>'Abierta']
			        ]);
				}	
			}
		}else{
			if(($this->Auth->user('role_id')==2)||($this->Auth->user('role_id')==4)){
				
				$crises = $this->Crises->find('all', [
					'contain'=>['Countries'],
			        'order' => ['Crises.created'=>'DESC'],
		            'conditions'=>['Crises.status'=>'Abierta', 'Crises.country_id'=>$this->Auth->user('country_id'), 'Crises.user_id'=>$this->Auth->user('id')]
		        ]);
				
				$crises2 = $this->Crises->find('all',[
					'contain'=>['Countries'],
			        'order' => ['Crises.created'=>'DESC'],
					'conditions'=>['Crises.status'=>'Abierta', 'Crises.country_id'=>$this->Auth->user('country_id')]])->join([
				        't' => [
				            'table' => 'users',
				            'type' => 'INNER',
				            'conditions' => ['t.id'=>$this->Auth->user('id')]
				        ],
				        'tl' => [
				            'table' => 'users_crises',
				            'type' => 'INNER',
				            'conditions' => ['tl.crisis_id = Crises.id','tl.user_id = t.id'],
				        ]
				        
				    ]);
					
			$crises->union($crises2);
				
			}else{
				$crises = $this->Crises->find('all', [
					'contain'=>['Countries'],
		            'conditions'=>['Crises.status'=>'Abierta'],
			        'order' => ['Crises.created'=>'DESC']
		        ]);
			}	
		}
    	
		$countries = $this->Crises->Countries->find('list');
        $this->paginate($crises);

        $this->set(compact('crises', 'coun', 'countries'));
        $this->set('_serialize', ['crises']);
    }

    /**
     * View method
     *
     * @param string|null $id Crisis id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $crisis = $this->Crises->get($id, [
            'contain' => ['Countries', 'Users', 'Connections', 'CrisisTypes', 'Feeds'=>['Users'], 'Tasks'=>['Users']]]);
		
		$idsus = [];
		foreach($crisis->users as $usuario){
			array_push($idsus, $usuario['id']);
		}
		
		if($idsus){
			if(($this->Auth->user('role_id')==2)||($this->Auth->user('role_id')==4)){
				if($crisis->user_id != $this->Auth->user('id')){
					if(!in_array($this->Auth->user('id'), $idsus)){
						$this->Flash->error(__('No tienes acceso a esa sección'));
						return $this->redirect(['controller'=>'Pages', 'action' => 'dashboard']);		
					}	
				}
			}
			
			$usuarios = $this->Crises->Users->find('list', ['conditions'=>['Users.id NOT IN'=>$idsus]]);
		}else{
			if(($this->Auth->user('role_id')==2)||($this->Auth->user('role_id')==4)){
				$this->Flash->error(__('No tienes acceso a esa sección'));
				return $this->redirect(['controller'=>'Pages', 'action' => 'dashboard']);		
			}
			
			$usuarios = $this->Crises->Users->find('list');
		}
		
		$status = $this->Crises->Tasks->Statuses->find('list');

        $this->set(compact('crisis', 'usuarios', 'status', 'connections'));
        $this->set('_serialize', ['crisis']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $crisis = $this->Crises->newEntity();
        if ($this->request->is('post')) {
            $crisis = $this->Crises->patchEntity($crisis, $this->request->data);
			
			$data = $this->request->data;
			$crisistypes = ['frequency', 'management', 'media', 'social', 'authorities', 'people', 'assets', 'incident', 'alarm', 'facilities', 'business'];
			
            if ($this->Crises->save($crisis)) {
			 	if(!isset($this->request->data['forma'])){
	            	foreach($crisistypes as $tipos){
						$data = $this->Crises->CrisisTypes->newEntity();
						$data->crisis_id = $crisis->id;
						$data->name = $tipos;
						$data->value = $this->request->data[$tipos];
						$this->Crises->CrisisTypes->save($data);
					}
				}
				foreach($crisis->users as $usario){
					$email = new Email('default');
						$email->template('newcrisis')
							->emailFormat('html')
							->from(['no-reply@war-roomdigital.com' => 'Info'])
						    ->to($usario['email'])
						    ->subject('War-Room Digital - Nueva Incidencia '.strtoupper($crisis->severity))
							->viewVars(['crisis' => $crisis->name, 'id'=>$crisis->id, 'user'=>$usario['name']])
						    ->send();
				} 
				
                $this->Flash->success(__('The crisis has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The crisis could not be saved. Please, try again.'));
            }
        }
        $users = $this->Crises->Users->find('list');
        $connections = $this->Crises->Connections->find('list');
		$tags = $this->Crises->Tags->find('list');
		$countries = $this->Crises->Countries->find('list');
		
        $this->set(compact('crisis', 'users', 'connections', 'tags', 'data', 'us', 'countries'));
        $this->set('_serialize', ['crisis']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Crisis id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $crisis = $this->Crises->get($id, [
            'contain' => ['Users', 'Connections', 'CrisisTypes', 'Tags']
        ]);
		
		$idsus = [];
		foreach($crisis->users as $usuario){
			array_push($idsus, $usuario['id']);
		}
		
		if($this->Auth->user('role_id')==2){
			if($crisis->user_id != $this->Auth->user('id')){
				if(!in_array($this->Auth->user('id'), $idsus)){
					$this->Flash->error(__('No tienes acceso a esa sección'));
					return $this->redirect(['controller'=>'Pages', 'action' => 'dashboard']);		
				}	
			}
		}
		
        if ($this->request->is(['patch', 'post', 'put'])) {
            $crisis = $this->Crises->patchEntity($crisis, $this->request->data);
			
			if ($this->Crises->save($crisis)){
				if(!$this->request->data['action']){
					foreach($crisis->crisis_types as $key=>$tipos){
						$data = $this->Crises->CrisisTypes->get($tipos['id']);
						$data->value = $this->request->data[$tipos['name']];
						$this->Crises->CrisisTypes->save($data);
					}
					
					foreach($crisis->users as $usario){
						$email = new Email('default');
							$email->template('editcrisis')
								->emailFormat('html')
								->from(['no-reply@war-roomdigital.com' => 'Info'])
							    ->to($usario['email'])
							    ->subject('War-Room Digital - Incidencia: '.$crisis->name)
								->viewVars(['crisis' => $crisis->name, 'id'=>$crisis->id, 'user'=>$usario['name']])
							    ->send();
					} 
				}
				
				$this->Flash->success(__('The crisis has been saved.'));
                return $this->redirect(['action' => 'view', $id]);	
				
            } else {
                $this->Flash->error(__('The crisis could not be saved. Please, try again.'));
            }
        }
        $users = $this->Crises->Users->find('list');
        $connections = $this->Crises->Connections->find('list');
		$tags = $this->Crises->Tags->find('list');
		$countries = $this->Crises->Countries->find('list');
		
        $this->set(compact('crisis', 'users', 'connections', 'tags', 'countries'));
        $this->set('_serialize', ['crisis']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Crisis id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $crisis = $this->Crises->get($id);
        if ($this->Crises->delete($crisis)) {
            $this->Flash->success(__('The crisis has been deleted.'));
        } else {
            $this->Flash->error(__('The crisis could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

	public function closed() {
		$this->paginate = [
	            'conditions'=>['Crises.status'=>'Cerrada'],
	            'contain' => ['Users', 'Countries']
	        ];
			
		$crises = $this->paginate($this->Crises);
        $this->set(compact('crises'));
        $this->set('_serialize', ['crises']);

	}
	
	public function close($id = null) {
		$this->request->allowMethod('post', 'put');
		
		$crisis = $this->Crises->get($id);
		$crisis->set('status', 'Cerrada');
		
		if ($this->Crises->save($crisis)) {
			foreach($crisis->users as $usario){
						$email = new Email('default');
							$email->template('closedcrisis')
								->emailFormat('html')
								->from(['no-reply@war-roomdigital.com' => 'Info'])
							    ->to($usario['email'])
							    ->subject('War-Room Digital - Incidencia Cerrada: '.$crisis->name)
								->viewVars(['crisis' => $crisis->name, 'id'=>$crisis->id, 'user'=>$usario['name']])
							    ->send();
			} 
			 $this->Flash->success(__('The crisis has been saved.'));
			return $this->redirect(array('action' => 'view', $id));
		}else{
			$this->Flash->error(__('The crisis could not be deleted. Please, try again.'));
			return $this->redirect(array('action' => 'view', $id));
		}

	}

	public function addusers() {
		$connection = ConnectionManager::get('default');
		$crisisId = $this->request->data['crisis_id'];
		$userId = $this->request->data['user_id'];
		
		$connection->execute('INSERT INTO users_crises (crisis_id, user_id) VALUES ('.$crisisId.','.$userId.')');
		
		$usuario = $this->Crises->Users->get($userId);
		$crisis = $this->Crises->get($crisisId);
		
		$email = new Email('default');
						$email->template('newcrisis')
							->emailFormat('html')
							->from(['no-reply@war-roomdigital.com' => 'Info'])
						    ->to($usuario['email'])
						    ->subject('War-Room Digital - Invitación a Incidencia '.strtoupper($crisis->severity))
							->viewVars(['crisis' => $crisis->name, 'id'=>$crisis->id, 'user'=>$usuario['name']])
						    ->send();
		
		$this->Flash->success(__('Usuario añadido.'));
		return $this->redirect(array('action' => 'view', $crisisId));
	}
	
	public function find() {
		$term = $this->request->data('term');
		
		if (($this->request->is(['patch', 'post', 'put']))&&($term)) {
			
			$terms = explode(' ', trim($term));
			$terms = array_diff($terms, array(''));
			$counids = [];
			foreach($terms as $term){
						$country = $this->Crises->Countries->find('all', ['conditions'=>['Countries.name LIKE' => '%'.$term.'%']]);
						
						foreach($country as $pais){
							array_push($counids, $pais->id);
						}
						
						if($counids){
							$conditions[] = [ 'OR'=>[
											'Crises.name LIKE'=>'%'.$term.'%', 'Crises.description LIKE'=>'%'.$term.'%',
											'Crises.Analysis LIKE'=>'%'.$term.'%', 'Crises.Action LIKE'=>'%'.$term.'%',
											'Crises.Result LIKE'=>'%'.$term.'%', 'Crises.severity LIKE'=>'%'.$term.'%',
											'Crises.city LIKE'=>'%'.$term.'%', 'Crises.address LIKE'=>'%'.$term.'%',
											'Crises.country_id IN ' => $counids
											]
										];
										
							$conditionsb[] = [ 'OR'=>[
												'Articles.title LIKE'=>'%'.$term.'%', 'Articles.body LIKE'=>'%'.$term.'%',
												'Articles.country_id IN ' => $counids
												]
											];	
						}else{
							$conditions[] = [ 'OR'=>[
											'Crises.name LIKE'=>'%'.$term.'%', 'Crises.description LIKE'=>'%'.$term.'%',
											'Crises.Analysis LIKE'=>'%'.$term.'%', 'Crises.Action LIKE'=>'%'.$term.'%',
											'Crises.Result LIKE'=>'%'.$term.'%', 'Crises.severity LIKE'=>'%'.$term.'%',
											'Crises.city LIKE'=>'%'.$term.'%', 'Crises.address LIKE'=>'%'.$term.'%',
											]
										];
										
							$conditionsb[] = [ 'OR'=>[
												'Articles.title LIKE'=>'%'.$term.'%', 'Articles.body LIKE'=>'%'.$term.'%',
												]
											];	
						}
					}
			
			if(($this->Auth->user('role_id')==2)||($this->Auth->user('role_id')== 4)){
					$crises = $this->Crises->find('all', [
						'contain'=>['Countries'],
						'order' => ['Crises.created'=>'DESC'],
			            'conditions'=>['Crises.user_id'=>$this->Auth->user('id'), $conditions]
			        ]);
					$crises2 = $this->Crises->find('all',[
			            'contain'=>['Countries'],
			            'order' => ['Crises.created'=>'DESC'],
						'conditions'=>[$conditions]])->join([
					        't' => [
					            'table' => 'users',
					            'type' => 'INNER',
					            'conditions' => ['t.id'=>$this->Auth->user('id')],
					        ],
					        'tl' => [
					            'table' => 'users_crises',
					            'type' => 'INNER',
					            'conditions' => ['tl.crisis_id = Crises.id','tl.user_id = t.id'],
					        ]
					        
					    ]);
						
					$crises->union($crises2);
				}else{
					$crises = $this->Crises->find('all', ['conditions'=>$conditions]);	
				}
			
			$this->loadModel('Articles');
			$nodes = $this->Articles->find('all', ['conditions'=>$conditionsb]);
			
	        $crises = $this->paginate($crises);		
		}else{
			$crises = $nodes = [];	
		}
		
		
		$this->set(compact('crises', 'nodes', 'conditions'));
        $this->set('_serialize', ['crises']);
	}

	public function beforeFilter (Event $event) {
	    parent::beforeFilter($event); 
	    
 		$auth_user = $this->Auth->user();
	    $user_type = $auth_user['role_id'];
		if($user_type == 1){
			$user_type = 'admin';
		}elseif($user_type == 2){
			$user_type = 'admin-team';
			
		}elseif($user_type == 3){
			$user_type = 'user';
		}elseif($user_type == 4){
			$user_type = 'guest';
		}
	
	    //pass user type to the plugin
	    $rules = array(
	        'user_type' => $user_type,
	        'redirect' => $this->referer(),
	        'message' => 'No tienes acceso a esa sección',
	        'action' =>  $this->request->params['action'],
	        'controller' =>  $this->request->params['controller'],
	        'groups' => array(
	            'guest' => array('index', 'view', 'find'),
			    'admin' => array('*'), 
			    'admin-team' => array('view','index', 'find', 'edit', 'add'),
			    'user' => array('view','index', 'find')
	        )
	    );
	
	    $this->UserPermissions->allow($rules);
	}	
	
}
