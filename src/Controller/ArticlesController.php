<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Email\Email;
use Cake\Event\Event;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AppController
{
	public $components = array(
	    'UserPermissions.UserPermissions'
	);

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'], 'conditions'=>['Articles.type'=>0],
            'order' => ['Articles.created'=>'DESC']
        ];
        $articles = $this->paginate($this->Articles);

        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);
    }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Users', 'Tags', 'ParentArticles', 'ChildArticles', 'BeforeArticles', 'Countries' , 
            				'Comments'=>['ChildComments'=>['Users'], 'Users', 'conditions'=>['Comments.parent_id IS NULL', 'Comments.status = 1']]]
        ]);
		
		if($article->type == 1){
			return $this->redirect(['action' => 'manual', $article->slug]);
		}
		
		if($article->updated_by){
			$updated = $this->Articles->Users->get($article->updated_by);	
		}
		
		$tagids = [];
		
		foreach($article->tags as $tags){
			array_push($tagids, $tags['id']);
		}
		
		if($tagids){
			$articulos = $this->Articles->find('all',['contain'=>['Users', 'Tags'], 'conditions'=>['Articles.status'=>1]])->join([
			        't' => [
			            'table' => 'tags',
			            'type' => 'INNER',
			            'conditions' => ['t.id IN'=>$tagids],
			        ],
			        'tl' => [
			            'table' => 'articles_tags',
			            'type' => 'INNER',
			            'conditions' => ['tl.article_id = Articles.id','tl.tag_id = t.id', 'tl.article_id !='.$id],
			        ]
			        
			    ]);	
		}

        $this->set(compact('article', 'updated', 'articulos'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
            	if(($article->type==0)&&($article->status == 1)){
            		$usuarios = $this->Articles->Users->find('all', ['coditions'=>['Users.id !='=>$article->user_id]]);
					foreach($usuarios as $users){
						$email = new Email('default');
						$email->template('newarticle')
							->emailFormat('html')
							->from(['no-reply@war-roomdigital.com' => 'Info'])
						    ->to('pvargas@enacment.com')
						    ->subject('War-Room Digital - Nueva Entrada en Trainigs Y Materiales')
							->viewVars(['articulo' => $article->title, 'id'=>$article->id, 'user'=>$users['name']])
						    ->send();
					} 
            	}
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        
        $categories = $this->Articles->Tags->find('list');
		$articles = $this->Articles->find('list');
		$countries = $this->Articles->Countries->find('list');
		
        $this->set(compact('article', 'categories', 'articles', 'countries'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Tags']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
            	if(($article->type==0)&&($article->status == 1)){
            		$usuarios = $this->Articles->Users->find('all', ['coditions'=>['Users.id !='=>$article->user_id]]);
					foreach($usuarios as $users){
						$email = new Email('default');
						$email->template('editarticle')
							->emailFormat('html')
							->from(['no-reply@war-roomdigital.com' => 'Info'])
						    ->to('pvargas@enacment.com')
						    ->subject('War-Room Digital - Trainigs Y Materiales: '.$article->title)
							->viewVars(['articulo' => $article->title, 'id'=>$article->id, 'user'=>$users['name']])
						    ->send();
					} 
            	}
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        
        $categories = $this->Articles->Tags->find('list');
		$articles = $this->Articles->find('list');
		$countries = $this->Articles->Countries->find('list');
		
        $this->set(compact('article', 'categories', 'articles', 'countries'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The article has been deleted.'));
        } else {
            $this->Flash->error(__('The article could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

	/**
     * Manual method
     *
     * @return \Cake\Network\Response|null
     */
    public function manual($id = null)
    {
    	if($id){
    		$articles = $this->Articles->find('all', ['contain' => [
    																'Users', 'Tags', 'ParentArticles', 'ChildArticles', 'BeforeArticles','Countries', 'Comments'=>['Users']], 
    												  'conditions'=>[
    												  				'Articles.slug'=>$id], 'limit' => 1]
											)->toArray();
			if($articles[0]->updated_by){
				$updated = $this->Articles->Users->get($articles[0]->updated_by);	
			}
			
			$tagids = [];
		
			foreach($articles[0]->tags as $tags){
				array_push($tagids, $tags['id']);
			}
			
			if($tagids){
				$articulos = $this->Articles->find('all',['contain'=>['Users', 'Tags'], 'conditions'=>['Articles.status'=>1], 'group'=>['Articles.id']])->join([
			        't' => [
			            'table' => 'tags',
			            'type' => 'INNER',
			            'conditions' => ['t.id IN'=>$tagids],
			        ],
			        'tl' => [
			            'table' => 'articles_tags',
			            'type' => 'INNER',
			            'conditions' => ['tl.article_id = Articles.id','tl.tag_id = t.id', 'tl.id !='.$articles[0]->id],
			        ]
			        
			    ]);
			}
			
			
			$this->set(compact('articles', 'updated', 'articulos'));
			$this->render('manual_view');	
    	}else{
    		$this->paginate = [
	            'contain' => ['Users'], 'conditions'=>['Articles.type'=>1]
	        ];
	        $articles = $this->paginate($this->Articles);
			 $this->set(compact('articles'));
        	$this->set('_serialize', ['articles']);	
    	}
    }

	public function related($tig = null)
    {
        $articles = $this->Articles->find('all', [
            'contain' => ['Users'], 
            'order' => ['Articles.created'=>'DESC']
        ])->join([
					        't' => [
					            'table' => 'tags',
					            'type' => 'INNER',
					            'conditions' => ['t.id'=>$tig],
					        ],
					        'tl' => [
					            'table' => 'articles_tags',
					            'type' => 'INNER',
					            'conditions' => ['tl.article_id = Articles.id','tl.tag_id = t.id'],
					        ]
					        
					    ]
		);
		
        $articles = $this->paginate($articles);
		$tag = $this->Articles->Tags->get($tig);

        $this->set(compact('articles', 'tag'));
        $this->set('_serialize', ['articles']);
    }


	public function beforeFilter (Event $event) {
	    parent::beforeFilter($event); 
	    
 		$auth_user = $this->Auth->user();
	    $user_type = $auth_user['role_id'];
		if($user_type == 1){
			$user_type = 'admin';
		}elseif($user_type == 2){
			$user_type = 'admin-team';
			
		}elseif($user_type == 3){
			$user_type = 'user';
		}elseif($user_type == 4){
			$user_type = 'guest';
		}
	
	    //pass user type to the plugin
	    $rules = array(
	        'user_type' => $user_type,
	        'redirect' => $this->referer(),
	        'message' => 'No tienes acceso a esa sección',
	        'action' =>  $this->request->params['action'],
	        'controller' =>  $this->request->params['controller'],
	        'groups' => array(
	            'guest' => array('index', 'view', 'related'),
			    'admin' => array('*'), 
			    'admin-team' => array('view','index', 'manual', 'edit', 'related'),
			    'user' => array('view','index', 'related')
	        )
	    );
	
	    $this->UserPermissions->allow($rules);
	}	
	
}
