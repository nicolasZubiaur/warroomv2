<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Feeds Controller
 *
 * @property \App\Model\Table\FeedsTable $Feeds
 */
class FeedsController extends AppController
{
	public $components = array(
	    'UserPermissions.UserPermissions'
	);

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Crises', 'ParentFeeds', 'AssignUsers', 'Connections']
        ];
        $feeds = $this->paginate($this->Feeds);

        $this->set(compact('feeds'));
        $this->set('_serialize', ['feeds']);
    }

    /**
     * View method
     *
     * @param string|null $id Feed id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $feed = $this->Feeds->get($id, [
            'contain' => ['Users', 'Crises', 'ParentFeeds', 'AssignUsers', 'Connections', 'ChildFeeds']
        ]);

        $this->set('feed', $feed);
        $this->set('_serialize', ['feed']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $feed = $this->Feeds->newEntity();
        if ($this->request->is('post')) {
            $feed = $this->Feeds->patchEntity($feed, $this->request->data);
            if ($this->Feeds->save($feed)) {
                $this->Flash->success(__('The feed has been saved.'));
                return $this->redirect(['controller'=>'Crises', 'action' => 'view', $feed->crisis_id]);
            } else {
                $this->Flash->error(__('The feed could not be saved. Please, try again.'));
				debug($feed->errors());
            }
        }
        $users = $this->Feeds->Users->find('list', ['limit' => 200]);
        $crises = $this->Feeds->Crises->find('list', ['limit' => 200]);
        $connections = $this->Feeds->Connections->find('list', ['limit' => 200]);
        $this->set(compact('feed', 'users', 'crises', 'connections'));
        $this->set('_serialize', ['feed']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Feed id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $feed = $this->Feeds->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $feed = $this->Feeds->patchEntity($feed, $this->request->data);
            if ($this->Feeds->save($feed)) {
                $this->Flash->success(__('The feed has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The feed could not be saved. Please, try again.'));
            }
        }
        $users = $this->Feeds->Users->find('list', ['limit' => 200]);
        $crises = $this->Feeds->Crises->find('list', ['limit' => 200]);
        $parentFeeds = $this->Feeds->ParentFeeds->find('list', ['limit' => 200]);
        $assignUsers = $this->Feeds->AssignUsers->find('list', ['limit' => 200]);
        $connections = $this->Feeds->Connections->find('list', ['limit' => 200]);
        $this->set(compact('feed', 'users', 'crises', 'parentFeeds', 'assignUsers', 'connections'));
        $this->set('_serialize', ['feed']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Feed id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $feed = $this->Feeds->get($id);
		
		if(($this->Auth->user('role_id')!=1)&&($feed->user_id!=$this->Auth->user('id'))){
			$this->Flash->error(__('No tienes acceso a esa sección'));
			return $this->redirect($this->referer());
		}
		
        if ($this->Feeds->delete($feed)) {
            $this->Flash->success(__('The feed has been deleted.'));
        } else {
            $this->Flash->error(__('The feed could not be deleted. Please, try again.'));
        }
        return $this->redirect($this->referer());
    }
	
	public function beforeFilter (Event $event) {
	    parent::beforeFilter($event); 
	    
 		$auth_user = $this->Auth->user();
	    $user_type = $auth_user['role_id'];
		if($user_type == 1){
			$user_type = 'admin';
		}elseif($user_type == 2){
			$user_type = 'admin-team';
			
		}elseif($user_type == 3){
			$user_type = 'user';
		}elseif($user_type == 4){
			$user_type = 'guest';
		}
	
	    //pass user type to the plugin
	    $rules = array(
	        'user_type' => $user_type,
	        'redirect' => $this->referer(),
	        'message' => 'No tienes acceso a esa sección',
	        'action' =>  $this->request->params['action'],
	        'controller' =>  $this->request->params['controller'],
	        'groups' => array(
	            'guest' => array('*'),
			    'admin' => array('*'), 
			    'admin-team' => array('*'),
			    'user' => array()
	        )
	    );
	
	    $this->UserPermissions->allow($rules);
	}
}
