<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Email\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
	public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['forgot', 'change']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    	if($this->Auth->user('role_id')!=1){
    		$this->Flash->error(__('No tienes acceso a esa sección'));
			return $this->redirect(['controller'=>'Pages', 'action' => 'dashboard']);
    	}
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
    	if($this->Auth->user('role_id')!=1){
    		$this->Flash->error(__('No tienes acceso a esa sección'));
			return $this->redirect(['controller'=>'Pages', 'action' => 'dashboard']);
    	}
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
		
		$countries = $this->Users->Countries->find('list');
		$roles = $this->Users->Roles->find('list');
		
        $this->set(compact('user', 'countries', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
    	if(($this->Auth->user('role_id')!=1)&&($this->Auth->user('id')!=$id)){
    		$this->Flash->error(__('No tienes acceso a esa sección'));
			return $this->redirect(['controller'=>'Pages', 'action' => 'dashboard']);
    	}
		
        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
		
		$roles = $this->Users->Roles->find('list');
		$countries = $this->Users->Countries->find('list');

        $this->set(compact('user', 'roles', 'countries'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
    	if($this->Auth->user('role_id')!=1){
    		$this->Flash->error(__('No tienes acceso a esa sección'));
			return $this->redirect(['controller'=>'Pages', 'action' => 'dashboard']);
    	}
		
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
	
	public function login()
	{
		$this->viewBuilder()->layout('login');
	    if ($this->request->is('post')) {
	        $user = $this->Auth->identify();
	        if ($user) {
	            $this->Auth->setUser($user);
	            return $this->redirect($this->Auth->redirectUrl());
	        }
	        $this->Flash->error(__('Datos incorrectos, intenta nuevamente'));
	    }
	}
	
	public function logout()
	{
	    return $this->redirect($this->Auth->logout());
	}
	
	/**
     * Invite method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function usercrisis($cid = null)
    {
        $user = $this->Users->newEntity();
		$crisis = $this->Users->Crises->get($cid);
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
			$pass = md5(uniqid());
			$user->password = $pass;
            if ($this->Users->save($user)) {
            	
            	$email = new Email('default');
						$email->template('newusercrisis')
							->emailFormat('html')
							->from(['no-reply@war-roomdigital.com' => 'Info'])
						    ->to($user['email'])
						    ->subject('War-Room Digital - Invitación a Incidencia '.strtoupper($crisis->severity))
							->viewVars(['crisis' => $crisis->name, 'id'=>$crisis->id, 'user'=>$user['name'], 'username'=>$user['username'], 
										'pass'=>$pass, 'uid'=>$user['id']])
						    ->send();
							
                $this->Flash->success(__('Usuario invitado.'));
                return $this->redirect(['controller'=>'Crises', 'action' => 'view', $cid]);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user', 'crisis'));
        $this->set('_serialize', ['user']);
    }

	public function forgot(){
		$this->viewBuilder()->layout('change_password');
		
		if ($this -> request -> is(['patch', 'post', 'put'])) {
        	$user =$this->Users->find('all',['conditions'=>['Users.email'=>$this->request->data['email']]])->toArray();
			
			$token = md5(uniqid(rand(), true));
			$user[0]->activation_key = $token;
			
            if ($this->Users->save($user[0])) {
            	
				$email = new Email('default');
				$email->template('passforgot')
					->emailFormat('html')
					->from(['no-reply@war-roomdigital.com' => 'Info'])
				    ->to($this->request->data['email'])
				    ->subject('War-Room Digital - Recupera tu Contraseña')
					->viewVars(['name' => $user[0]['name'], 'id'=>$user[0]['id'], 'token'=>$token])
				    ->send();
				
                $this->Flash->success('Se te ha enviado un correo con las instrucciones para recuperar tu contraseña.');
                $this->redirect($this->referer());
            } else {
                $this->Flash->error('Ocurrió un error, prueba nuevamente.');
            }
        } 
	}

	public function change() {
		$this->viewBuilder()->layout('change_password'); 
	    
        if ($this -> request -> is(['patch', 'post', 'put'])) {
        	$user =$this->Users->find('all',['conditions'=>['Users.email'=>$this->request->data['email']]])->toArray();
			
			if(($user[0]['id']==$this->request->data['user'])&&($user[0]['activation_key']==$this->request->data['token'])){
				$user[0] = $this->Users->patchEntity($user[0], [
	                    'password'     => $this->request->data['password1'],
	                    'activation_key'     => ''
	                ],
	                ['validate' => 'password']
	            );
				if ($this->Users->save($user[0])) {
	                $this->Flash->success('Tu contraseña ha sido actualizada!');
	                $this->redirect(['controller'=>'Users', 'action'=>'login']);
	            } else {
	                $this->Flash->error('Ha ocurrido un error, comprueba tus datos e intenta nuevamente.');
	            }
			}else{
				$this->Flash->error('Verifica tu información!');
			}
        }
        
	}

		
	public function searchusers(){
		
		if($this->request->query['country']){
			$country = $this->request->query['country'];
			$role = $this->request->query['role'];
			
			$usuarios = $this->Users->find('all', array('fields'=>array('Users.id','Users.name'), 
														'conditions'=>['Users.country_id'=>$country, 'Users.role_id'=>$role])
														);
		}
		
		echo json_encode($usuarios);
		$this->autoRender = false;
		
	}
}
