<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Groups Controller
 *
 * @property \App\Model\Table\GroupsTable $Groups
 */
class GroupsController extends AppController
{
		
	public $components = array(
	    'UserPermissions.UserPermissions'
	);

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Roles', 'Countries']
        ];
        $groups = $this->paginate($this->Groups);

        $this->set(compact('groups'));
        $this->set('_serialize', ['groups']);
    }

    /**
     * View method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => ['Roles', 'Countries', 'Users']
        ]);

        $this->set('group', $group);
        $this->set('_serialize', ['group']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $group = $this->Groups->newEntity();
        if ($this->request->is('post')) {
            $group = $this->Groups->patchEntity($group, $this->request->data);
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The group could not be saved. Please, try again.'));
            }
        }
        $roles = $this->Groups->Roles->find('list', ['limit' => 200]);
        $countries = $this->Groups->Countries->find('list', ['limit' => 200]);
        $users = $this->Groups->Users->find('list', ['limit' => 200]);
        $this->set(compact('group', 'roles', 'countries', 'users'));
        $this->set('_serialize', ['group']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $group = $this->Groups->patchEntity($group, $this->request->data);
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The group could not be saved. Please, try again.'));
            }
        }
        $roles = $this->Groups->Roles->find('list', ['limit' => 200]);
        $countries = $this->Groups->Countries->find('list', ['limit' => 200]);
        $users = $this->Groups->Users->find('list', ['conditions'=>['Users.role_id'=>$group->role_id]]);
        $this->set(compact('group', 'roles', 'countries', 'users'));
        $this->set('_serialize', ['group']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->Groups->get($id);
        if ($this->Groups->delete($group)) {
            $this->Flash->success(__('The group has been deleted.'));
        } else {
            $this->Flash->error(__('The group could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

	public function searchgroups(){
		if($this->request->query['country']){
			$country = $this->request->query['country'];
			
			$groups = $this->Groups->find('all', [
				'contain' => ['Roles'],
	            'conditions' => ['Groups.country_id' => $country]
	        ]);
		}
		
		echo json_encode($groups);
		$this->autoRender = false;
	} 
	
	public function getusers(){
		if($this->request->query['group']){
			$group = $this->request->query['group'];
			$usuarios = [];
			
			foreach($group as $grupo){
				$conditions[] = ['Groups.id'=> $grupo];
			}
			
			$groups = $this->Groups->find('all', [
				'conditions'=>['OR'=>$conditions],
				'contain' => ['Users']
	        ]);
			
			foreach($groups as $grp){
				foreach($grp->users as $usrs){
					array_push($usuarios, $usrs['id']);
				}
			}

			$this->loadModel('Users');
			$users = $this->Users->find('all', ['conditions'=>['Users.id IN '=>$usuarios]]);
		}
		
		echo json_encode($users);
		$this->autoRender = false;
	}
	
	public function beforeFilter (Event $event) {
	    parent::beforeFilter($event); 
	    
 		$auth_user = $this->Auth->user();
	    $user_type = $auth_user['role_id'];
		if($user_type == 1){
			$user_type = 'admin';
		}elseif($user_type == 2){
			$user_type = 'admin-team';
			
		}elseif($user_type == 3){
			$user_type = 'user';
		}elseif($user_type == 4){
			$user_type = 'guest';
		}
	
	    //pass user type to the plugin
	    $rules = array(
	        'user_type' => $user_type,
	        'redirect' => $this->referer(),
	        'message' => 'No tienes acceso a esa sección',
	        'action' =>  $this->request->params['action'],
	        'controller' =>  $this->request->params['controller'],
	        'groups' => array(
	            'guest' => array(),
			    'admin' => array('*'), 
			    'admin-team' => array('searchgroups', 'getusers'),
			    'user' => array()
	        )
	    );
	
	    $this->UserPermissions->allow($rules);
	}	
}
