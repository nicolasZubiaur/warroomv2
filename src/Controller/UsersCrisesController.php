<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersCrises Controller
 *
 * @property \App\Model\Table\UsersCrisesTable $UsersCrises
 */
class UsersCrisesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Crises']
        ];
        $usersCrises = $this->paginate($this->UsersCrises);

        $this->set(compact('usersCrises'));
        $this->set('_serialize', ['usersCrises']);
    }

    /**
     * View method
     *
     * @param string|null $id Users Crisis id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersCrisis = $this->UsersCrises->get($id, [
            'contain' => ['Users', 'Crises']
        ]);

        $this->set('usersCrisis', $usersCrisis);
        $this->set('_serialize', ['usersCrisis']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersCrisis = $this->UsersCrises->newEntity();
        if ($this->request->is('post')) {
            $usersCrisis = $this->UsersCrises->patchEntity($usersCrisis, $this->request->data);
            if ($this->UsersCrises->save($usersCrisis)) {
                $this->Flash->success(__('The users crisis has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users crisis could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersCrises->Users->find('list', ['limit' => 200]);
        $crises = $this->UsersCrises->Crises->find('list', ['limit' => 200]);
        $this->set(compact('usersCrisis', 'users', 'crises'));
        $this->set('_serialize', ['usersCrisis']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Crisis id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersCrisis = $this->UsersCrises->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersCrisis = $this->UsersCrises->patchEntity($usersCrisis, $this->request->data);
            if ($this->UsersCrises->save($usersCrisis)) {
                $this->Flash->success(__('The users crisis has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users crisis could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersCrises->Users->find('list', ['limit' => 200]);
        $crises = $this->UsersCrises->Crises->find('list', ['limit' => 200]);
        $this->set(compact('usersCrisis', 'users', 'crises'));
        $this->set('_serialize', ['usersCrisis']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Crisis id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersCrisis = $this->UsersCrises->get($id);
        if ($this->UsersCrises->delete($usersCrisis)) {
            $this->Flash->success(__('The users crisis has been deleted.'));
        } else {
            $this->Flash->error(__('The users crisis could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
