<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Tags Controller
 *
 * @property \App\Model\Table\TagsTable $Tags
 */
class TagsController extends AppController
{
	public $components = array(
	    'UserPermissions.UserPermissions'
	);

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $tags = $this->paginate($this->Tags);

        $this->set(compact('tags'));
        $this->set('_serialize', ['tags']);
    }

    /**
     * View method
     *
     * @param string|null $id Tag id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tag = $this->Tags->get($id, [
            'contain' => ['Users', 'Crises']
        ]);

        $this->set('tag', $tag);
        $this->set('_serialize', ['tag']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tag = $this->Tags->newEntity();
        if ($this->request->is('post')) {
            $tag = $this->Tags->patchEntity($tag, $this->request->data);
            if ($this->Tags->save($tag)) {
                $this->Flash->success(__('The tag has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tag could not be saved. Please, try again.'));
            }
        }
        $users = $this->Tags->Users->find('list', ['limit' => 200]);
        $crises = $this->Tags->Crises->find('list', ['limit' => 200]);
        $this->set(compact('tag', 'users', 'crises'));
        $this->set('_serialize', ['tag']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tag id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tag = $this->Tags->get($id, [
            'contain' => ['Crises']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tag = $this->Tags->patchEntity($tag, $this->request->data);
            if ($this->Tags->save($tag)) {
                $this->Flash->success(__('The tag has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tag could not be saved. Please, try again.'));
            }
        }
        $users = $this->Tags->Users->find('list', ['limit' => 200]);
        $crises = $this->Tags->Crises->find('list', ['limit' => 200]);
        $this->set(compact('tag', 'users', 'crises'));
        $this->set('_serialize', ['tag']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tag id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tag = $this->Tags->get($id);
        if ($this->Tags->delete($tag)) {
            $this->Flash->success(__('The tag has been deleted.'));
        } else {
            $this->Flash->error(__('The tag could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter (Event $event) {
	    parent::beforeFilter($event); 
	    
 		$auth_user = $this->Auth->user();
	    $user_type = $auth_user['role_id'];
		if($user_type == 1){
			$user_type = 'admin';
		}elseif($user_type == 2){
			$user_type = 'admin-team';
			
		}elseif($user_type == 3){
			$user_type = 'user';
		}elseif($user_type == 4){
			$user_type = 'guest';
		}
	
	    //pass user type to the plugin
	    $rules = array(
	        'user_type' => $user_type,
	        'redirect' => $this->referer(),
	        'message' => 'No tienes acceso a esa sección',
	        'action' =>  $this->request->params['action'],
	        'controller' =>  $this->request->params['controller'],
	        'groups' => array(
	            'guest' => array(),
			    'admin' => array('*'), 
			    'admin-team' => array(),
			    'user' => array()
	        )
	    );
	
	    $this->UserPermissions->allow($rules);
	}
}
