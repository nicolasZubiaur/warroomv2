<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 */
class CommentsController extends AppController
{
	public $components = array(
	    'UserPermissions.UserPermissions'
	);

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentComments', 'Users', 'Articles']
        ];
        $comments = $this->paginate($this->Comments);

        $this->set(compact('comments'));
        $this->set('_serialize', ['comments']);
    }

    /**
     * View method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => ['ParentComments', 'Users', 'Articles', 'ChildComments']
        ]);

        $this->set('comment', $comment);
        $this->set('_serialize', ['comment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->data);
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));
				if(isset($this->request->data['slug'])){
					return $this->redirect(['controller'=>'Articles', 'action' => 'manual', $this->request->data['slug']]);
				}else{
					return $this->redirect(['controller'=>'Articles', 'action' => 'view', $comment->article_id]);	
				}
            } else {
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }
        
        $this->set(compact('comment'));
        $this->set('_serialize', ['comment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $comment = $this->Comments->patchEntity($comment, $this->request->data);
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }
        $parentComments = $this->Comments->ParentComments->find('list', ['limit' => 200]);
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $articles = $this->Comments->Articles->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'parentComments', 'users', 'articles'));
        $this->set('_serialize', ['comment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $comment = $this->Comments->get($id);
        if ($this->Comments->delete($comment)) {
            $this->Flash->success(__('The comment has been deleted.'));
        } else {
            $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter (Event $event) {
	    parent::beforeFilter($event); 
	    
 		$auth_user = $this->Auth->user();
	    $user_type = $auth_user['role_id'];
		if($user_type == 1){
			$user_type = 'admin';
		}elseif($user_type == 2){
			$user_type = 'admin-team';
			
		}elseif($user_type == 3){
			$user_type = 'user';
		}elseif($user_type == 4){
			$user_type = 'guest';
		}
	
	    //pass user type to the plugin
	    $rules = array(
	        'user_type' => $user_type,
	        'redirect' => $this->referer(),
	        'message' => 'No tienes acceso a esa sección',
	        'action' =>  $this->request->params['action'],
	        'controller' =>  $this->request->params['controller'],
	        'groups' => array(
	            'guest' => array('index', 'add', 'view', 'delete'),
			    'admin' => array('*'), 
			    'admin-team' => array('index', 'add', 'view', 'delete'),
			    'user' => array('add')
	        )
	    );
	
	    $this->UserPermissions->allow($rules);
	}
}
