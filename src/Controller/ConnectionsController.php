<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Connections Controller
 *
 * @property \App\Model\Table\ConnectionsTable $Connections
 */
class ConnectionsController extends AppController
{
	public $components = array(
	    'UserPermissions.UserPermissions'
	);
	
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
		if ($this->request->is(['patch', 'post', 'put'])) {
			if(($this->request->data['category_id'])&&($this->request->data['country_id'])){
				$cat = $this->request->data['category_id'];
				$coun = $this->request->data['country_id'];
				
				$connections = $this->Connections->find('all', ['conditions'=>['Connections.country_id'=>$coun]])->join([
			        't' => [
			            'table' => 'categories',
			            'type' => 'INNER',
			            'conditions' => ['t.id'=>$cat],
			        ],
			        'tl' => [
			            'table' => 'connections_categories',
			            'type' => 'INNER',
			            'conditions' => ['tl.connection_id = Connections.id','tl.category_id = t.id'],
			        ],
			        
			    ]);	
			}else if(($this->request->data['category_id'])&&(!$this->request->data['country_id'])){
				$cat = $this->request->data['category_id'];
				
				$connections = $this->Connections->find('all')->join([
			        't' => [
			            'table' => 'categories',
			            'type' => 'INNER',
			            'conditions' => ['t.id'=>$cat],
			        ],
			        'tl' => [
			            'table' => 'connections_categories',
			            'type' => 'INNER',
			            'conditions' => ['tl.connection_id = Connections.id','tl.category_id = t.id'],
			        ],
			        
			    ]);	
			}else if((!$this->request->data['category_id'])&&($this->request->data['country_id'])){
				$coun = $this->request->data['country_id'];
				
				$connections = $this->Connections->find('all', ['conditions'=>['Connections.country_id'=>$coun]]);	
			}else{
				$connections = $this->Connections->find('all');
			}
			
		}else{
			if($this->Auth->user('role_id')==1){
				$connections = $this->Connections->find('all');	
				$countries = $this->Connections->Countries->find('all');
			}else{
				$connections = $this->Connections->find('all', ['conditions'=>['Connections.country_id'=>$this->Auth->user('country_id')]]);
			}
		}
		
		$categories = $this->Connections->Categories->find('list');

        $this->set(compact('connections', 'categories', 'coun', 'countries'));
        $this->set('_serialize', ['connections']);
    }

    /**
     * View method
     *
     * @param string|null $id Connection id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
    	$connection = $this->Connections->get($id, [
	            'contain' => ['Categories', 'Crises', 'Feeds', 'Countries']
	        ]);
			
    	if(($this->Auth->user('role_id')!=1)&&($connection->country_id!=$this->Auth->user('country_id'))){
	        $this->Flash->error(__('No tienes acceso a esa sección.'));
			return $this->redirect(['action' => 'index']);
		}
		
        $this->set('connection', $connection);
        $this->set('_serialize', ['connection']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $connection = $this->Connections->newEntity();
        if ($this->request->is('post')) {
            $connection = $this->Connections->patchEntity($connection, $this->request->data);
            if ($this->Connections->save($connection)) {
                $this->Flash->success(__('The connection has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
            	//debug($connection->errors());
                $this->Flash->error(__('The connection could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Connections->Categories->find('list');
		$countries = $this->Connections->Countries->find('list');
        $crises = $this->Connections->Crises->find('list', ['conditions' => ['Crises.status = "Abierta"']]);
        $this->set(compact('connection', 'categories', 'crises', 'countries'));
        $this->set('_serialize', ['connection']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Connection id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $connection = $this->Connections->get($id, [
            'contain' => ['Categories', 'Crises']
        ]);
		
		if(($this->Auth->user('role_id')!=1)&&($connection->country_id!=$this->Auth->user('country_id'))){
	        $this->Flash->error(__('No tienes acceso a esa sección.'));
			return $this->redirect(['action' => 'index']);
		}
		
        if ($this->request->is(['patch', 'post', 'put'])) {
            $connection = $this->Connections->patchEntity($connection, $this->request->data);
            if ($this->Connections->save($connection)) {
                $this->Flash->success(__('The connection has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The connection could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Connections->Categories->find('list');
        $crises = $this->Connections->Crises->find('list');
		$countries = $this->Connections->Countries->find('list');
        $this->set(compact('connection', 'categories', 'crises', 'countries'));
        $this->set('_serialize', ['connection']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Connection id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $connection = $this->Connections->get($id);
		
		if(($this->Auth->user('role_id')!=1)&&($connection->country_id!=$this->Auth->user('country_id'))){
	        $this->Flash->error(__('No tienes acceso a esa sección.'));
			return $this->redirect(['action' => 'index']);
		}

        if ($this->Connections->delete($connection)) {
            $this->Flash->success(__('The connection has been deleted.'));
        } else {
            $this->Flash->error(__('The connection could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter (Event $event) {
	    parent::beforeFilter($event); 
	    
 		$auth_user = $this->Auth->user();
	    $user_type = $auth_user['role_id'];
		if($user_type == 1){
			$user_type = 'admin';
		}elseif($user_type == 2){
			$user_type = 'admin-team';
			
		}elseif($user_type == 3){
			$user_type = 'user';
		}elseif($user_type == 4){
			$user_type = 'guest';
		}
	
	    //pass user type to the plugin
	    $rules = array(
	        'user_type' => $user_type,
	        'redirect' => $this->referer(),
	        'message' => 'No tienes acceso a esa sección',
	        'action' =>  $this->request->params['action'],
	        'controller' =>  $this->request->params['controller'],
	        'groups' => array(
	            'guest' => array('index', 'view', 'add', 'edit'),
			    'admin' => array('*'), 
			    'admin-team' => array('index', 'view', 'add', 'edit'),
			    'user' => array()
	        )
	    );
	
	    $this->UserPermissions->allow($rules);
	}
}
