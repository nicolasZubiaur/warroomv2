<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CrisisTypes Controller
 *
 * @property \App\Model\Table\CrisisTypesTable $CrisisTypes
 */
class CrisisTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Crises']
        ];
        $crisisTypes = $this->paginate($this->CrisisTypes);

        $this->set(compact('crisisTypes'));
        $this->set('_serialize', ['crisisTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Crisis Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $crisisType = $this->CrisisTypes->get($id, [
            'contain' => ['Crises']
        ]);

        $this->set('crisisType', $crisisType);
        $this->set('_serialize', ['crisisType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $crisisType = $this->CrisisTypes->newEntity();
        if ($this->request->is('post')) {
            $crisisType = $this->CrisisTypes->patchEntity($crisisType, $this->request->data);
            if ($this->CrisisTypes->save($crisisType)) {
                $this->Flash->success(__('The crisis type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The crisis type could not be saved. Please, try again.'));
            }
        }
        $crises = $this->CrisisTypes->Crises->find('list', ['limit' => 200]);
        $this->set(compact('crisisType', 'crises'));
        $this->set('_serialize', ['crisisType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Crisis Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $crisisType = $this->CrisisTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $crisisType = $this->CrisisTypes->patchEntity($crisisType, $this->request->data);
            if ($this->CrisisTypes->save($crisisType)) {
                $this->Flash->success(__('The crisis type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The crisis type could not be saved. Please, try again.'));
            }
        }
        $crises = $this->CrisisTypes->Crises->find('list', ['limit' => 200]);
        $this->set(compact('crisisType', 'crises'));
        $this->set('_serialize', ['crisisType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Crisis Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $crisisType = $this->CrisisTypes->get($id);
        if ($this->CrisisTypes->delete($crisisType)) {
            $this->Flash->success(__('The crisis type has been deleted.'));
        } else {
            $this->Flash->error(__('The crisis type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
