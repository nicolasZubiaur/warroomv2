<?php 
use Cake\I18n\Time;

if(isset($authUser->timezone)){
  $time->timezone = $authUser->timezone;
}
?>
<section class="content-header">


      <h1>
        <?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> Entradas relacionadas con "<?php echo $tag->title ?>"
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"><a href="#"><i class="fa fa-page"></i> Trainings y Materiales</a></li>
        
      </ol>
      <div class="btn-group pull-right">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
       <i class="fa fa-gears"></i> <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
      <li><a href="#"><?= $this->Html->link(__('Nuevo Articulo'), ['action' => 'add']) ?></a></li>
      <li><a href="#"><?= $this->Html->link(__('Nuevo Tag'), ['controller' => 'Tags', 'action' => 'add']) ?></a></li>
    </ul>
  </div>
    </section>
    <br>
<div class="col-md-12">
    <table cellpadding="0" cellspacing="0" class="table table-striped">
        <thead>
            <tr>

                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>

                <th><?= $this->Paginator->sort('modified') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($articles as $article): ?>
            <tr>

                <td><?= $article->has('user') ? $this->Html->link($article->user->name, ['controller' => 'Users', 'action' => 'view', $article->user->id]) : '' ?></td>
                <td>                  <?= $this->Html->link(h($article->title), ['action' => 'view', $article->id]) ?></td>

                <td><?= h($article->modified) ?></td>

            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Ant.')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Sig.') . ' >') ?>
        </ul>

    </div>
</div>
