<?php 
use Cake\I18n\Time;

if(isset($authUser->timezone)){
  $time->timezone = $authUser->timezone;
}
?>
<section class="content-header">
          <h1>
           <?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> <?= h($article->title) ?>
          </h1>
          <ol class="breadcrumb">
            <li><?php echo $this->Html->link('<i class="fa fa-home"></i> <span>Dashboard</span>', ['controller'=>'Pages', 'action'=>'dashboard'], ['escape'=>false]); ?></li>
            <?php
                if($article->parent_article){
                    echo '<li>';
                    echo $this->Html->link($article->parent_article->title, ['controller' => 'Articles', 'action' => 'manual', $article->parent_article->slug]);
                    echo '</li>';
                }
            ?>
            <li class="active"><?= h($article->title) ?></li>
          </ol>
</section>

<div class="articles view large-9 medium-8 columns content">
  <section class="content">

<div class="row">
   <div class="col-md-12">
          <div class="box box-success">
            <div class="box-body">
            <div class="col-md-5">
            <p class="text-dark-gray"><span class="text-muted"><i class="fa fa-flag"></i> <?= $article->country->name ?> <br>
               
                   <span class="text-muted"><i class="fa fa-clock-o"></i> creado              <?php

                   $now = new Time($article->created); 
                    echo $now->timeAgoInWords(
                        ['format' => 'MMM d, YYY', 'end' => '+1 week']
                    );
                   ?>
                   por              <?php 
                if($article->user){
                    echo $this->Html->link($article->user->name, ['controller' => 'Users', 'action' => 'view', $article->user->id]);
                }
                ?>
                <br>
                  <span class="text-muted"><i class="fa fa-clock-o"></i> modificado             <?php

                   $now = new Time($article->modified); 
                    echo $now->timeAgoInWords(
                        ['format' => 'MMM d, YYY', 'end' => '+1 week']
                    );
                   ?>
                            <?php
        if($article->updated_by){
        ?>

            <?= __(' por') ?>
            <?= $this->Html->Link($updated->name, ['controller'=>'Users', 'action'=>'view', $updated->id]) ?>

        <?php
        }
        ?>
                </p>
                </div>
                <div class="col-md-7">
                <?php if(!empty($article->tags)){?>
                <h4 style="text-align: right;">Etiquetas</h4>
                <div class="row">
                    <?php foreach ($article->tags as $categories): ?>

                <?php echo $this->Html->link('#'.$categories->title, ['controller' => 'Articles', 'action' => 'related', $categories->id], ['target'=>'_blank', 'escape'=>false, 'class'=>"btn btn-danger pull-right", 'style'=>'margin: 0px 3px 6px 3px']); ?>

            <?php endforeach; ?>
            </div>
            <?php  } else{ echo '<br>';}?>
<div class="row">                        <?php 
                if($article->before_article){
                    echo $this->Html->link('<i class="fa fa-arrow-left"></i> '.$article->before_article->title, ['controller' => 'Articles', 'action' => 'manual', $article->before_article->slug], ['class'=>'btn btn-info pull-left', 'escape'=>false]);
                }
                ?>

                <?php 
                if($article->child_article){
                    echo $this->Html->link($article->child_article->title.' <i class="fa fa-arrow-right"></i> ', ['controller' => 'Articles', 'action' => 'manual', $article->child_article->slug], ['class'=>'btn btn-info pull-right', 'escape'=>false]);
                }
                ?></div>
                </div>
</div>
</div>
    <div class="box box-success">
    <div class="box-body">
                    <div class="btn-group pull-right">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span> <i class="fa fa-gears"></i>
        </button>
        <ul class="dropdown-menu">
          <li><?= $this->Html->link('<i class="fa fa-edit"></i> Editar Entrada', ['action' => 'edit', $article->id], ['escape' => false]); ?></li>
          <li><?= $this->Form->postLink(
                            '<i class="fa fa-trash"></i> Borrar Entrada',
                            array('action' => 'delete', $article->id),
                            array('escape'=>false ,'confirm' => __('Are you sure you want to delete # {0}?', $article->id))) ;?>
                            </li>
            <li><?= $this->Html->link('<i class="fa fa-plus"></i> Nuevo Articulo', ['action' => 'add'], ['escape'=>false]) ?></li>
        </ul>
    </div>
        <?= $this->Text->autoparagraph($article->body); ?>
        </div>
    </div>
<div class="box box-success">
  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-comments-o"></i>
    <h3 class="box-title">Mensajes</h3>
  </div>
            <div class="box-body chat" id="chat-box" style="overflow: hidden; width: auto;">
              <?php
                foreach($article->comments as $comment){
              ?>
                <div class="item">
                  <?php 
                    if($comment->user->image){
                      echo $this->Html->image('/'.$comment->user->image); 
                    }else{
                      echo $this->Html->image('default-avatar.png');
                    }
                  ?>
                  <p class="message">
                    <a href="#" class="name">
                          <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php echo $comment->created ?></small>
                          <?= h($comment->user->name) ?>
                      </a>
                      <?php echo $comment->body; ?>
                  </p>
                </div>
              <?php
                }
              ?>
            </div>
         
            <div class="box-footer">
            <?php echo $this -> Form -> create('Task', array('url' => array('controller' => 'comments', 'action' => 'add'), 'class' => 'form-horizontal')); 
              echo $this->Form->input('user_id', ['value'=>$authUser['id'], 'type'=>'hidden']);
              echo $this->Form->input('article_id', ['value'=>$article['id'], 'type'=>'hidden']);
              echo $this->Form->input('slug', ['value'=>$article['slug'], 'type'=>'hidden']);
              echo $this->Form->input('status', ['value'=>1, 'type'=>'hidden']);
              ?>
            <div class="col-md-11">
            <?php echo $this->Form->textarea('body', ['rows'=>'3', 'class'=>'form-control', 'placeholder'=>'Escribe tu Mensaje...', 'label'=>false]); ?>
                
            </div>
            <div class="col-md-1">
              <button class="btn btn-block btn-success" type="submit"><i class="fa fa-paper-plane"></i></button>
            </div>
            
            <?php echo $this -> Form -> end(); ?>
            </div>
        </div>
   <?php if (!empty($articulos)): ?> 
    <div class="box box-success">
      <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-random"></i>
    <h3 class="box-title"> <?= __('Relacionado') ?></h3>
  </div>
    <div class="box-body">
       
        <table class="table table-striped">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
            </tr>
            <?php foreach ($articulos as $articulo): ?>
            <tr>
                <td><?= h($articulo->id) ?></td>
                <td><?= h($articulo->user->name) ?></td>
                <td><?= $this->Html->link(h($articulo->title), ['controller' => 'Articles', 'action' => 'view', $articulo->id]) ?></td>
                <td><?= h($articulo->created) ?></td>
                <td><?= h($articulo->modified) ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        </div>
    </div><?php endif; ?>
            </div>  
</div>
</section>
</div>
<script>
    $(document).ready(function(){
        <?php
            if($article->slug){
                if($article->parent_article){
                ?>
                    $('#<?php echo $article->parent_article->slug ?>').addClass('active');
                <?php
                if(($article->slug=='gestion-de-comunicacion-nivel-1-verde')||($article->slug=='gestion-comunicacion-nivel-2-amarillo')||($article->slug=='gestion-comunicacion-nivel-3-naranja')||($article->slug=='gestion-comunicacion-nivel-4-rojo')){
                ?>
                    $('#la-comunicacion-en-el-comite-de-gestion-de-crisis-cgc').addClass('active');
                <?php
                }
                }
                ?>
                $('#<?php echo $article->slug; ?>').addClass('active');
        <?php
            }
        ?>      
    });
</script>