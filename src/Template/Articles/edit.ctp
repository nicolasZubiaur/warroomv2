<?= $this->Html->script('/plugins/ckeditor/ckeditor') ?>
<section class="content">
<div class="row">
  <div class="col-md-12">
      <div class="box">
	        <div class="box-header">
	          <h3 class="box-title"><i class="fa fa-add"></i> <?= __('Editar Articulo') ?></h3>
	
	        </div>
        	<!-- /.box-header -->
<div class="box-body no-padding">
    <?= $this->Form->create($article,['type'=>'file']) ?>
	<table class="table" style="width: 90%">
	<tbody>

        
<?php            
	$status = ['0'=>'Unpublished', '1'=>'Published'];
	$type = ['0'=>'Training y Materiales', '1'=>'Manual'];
	echo $this->Form->input('updated_by',['value' => $authUser['id'], 'type' => 'hidden']);
?>
<tr><th>Título</th><td><?php            echo $this->Form->input('title', ['empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Slug</th><td><?php            echo $this->Form->input('slug', ['empty' => true,'class'=>'form-control ckeditor','label'=>false]);
?></td></tr><tr><th>Contenido</th><td><?php            echo $this->Form->input('body', ['empty' => true,'class'=>'form-control ckeditor','label'=>false]);
?></td></tr><tr><th>País</th><td><?php            echo $this->Form->input('country_id', ['empty' => true,'class'=>'form-control','label'=>false, 'options'=>$countries]);
?></td></tr><tr><th>Artículo Padre</th><td><?php            echo $this->Form->input('parent_id',['options' => $articles, 'empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Artículo Anterior</th><td><?php            echo $this->Form->input('before_id',['options' => $articles, 'empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Artículo Siguiente</th><td><?php            echo $this->Form->input('next_id',['options' => $articles, 'empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Estatus</th><td><?php            echo $this->Form->input('status', ['empty' => true,'class'=>'form-control','label'=>false, 'options'=>$status]);
?></td></tr><tr><th>Tipo</th><td><?php            echo $this->Form->input('type', ['empty' => true,'class'=>'form-control','label'=>false, 'options'=>$type]);
?></td></tr><tr><th>Tags</th><td><?php            echo $this->Form->input('tags._ids', ['options' => $categories, 'empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Imagen</th><td><?php            echo $this->Form->input('image_file', ['empty' => true,'class'=>'form-control','label'=>false, 'type'=>'file']);
?></td></tr>       
          </tbody></table>
          <div style="padding-top: 10px;padding-bottom: 50px;padding-left: 5%;padding-right: 5%">
          	<?= 
              $this->Form->button('<i class="fa fa-floppy-o "></i> Guardar', [
														    'type' => 'submit',
														    'escape' => false,
														    'class' =>'btn btn-primary btn-block btn-lg'
															]); ?>
          </div>
              
    		<?= $this->Form->end() ?>

        </div>
        <!-- /.box-body -->
      </div>

	</div>
    <!-- /.col -->
</div>
<script>
	$(document).ready(function(){
		$('#title').change(function(){
			var text = $('#title').val();
			text = text.toLowerCase();
			text = text.replace(/ /g,'-');
			text = text.replace(/á/g,'a');
			text = text.replace(/é/g,'e');
			text = text.replace(/í/g,'i');
			text = text.replace(/ó/g,'o');
			text = text.replace(/ú/g,'u');
			text = text.replace(/ü/g,'u');
			text = text.replace(/ñ/g,'n');
			$('#slug').val(text);
		});
	});
</script>