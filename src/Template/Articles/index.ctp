<?php 
use Cake\I18n\Time;

if(isset($authUser->timezone)){
  $time->timezone = $authUser->timezone;
}
?>
<section class="content-header">


      <h1>
        <?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> Trainings y Materiales
        <small>Índice</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"><a href="#"><i class="fa fa-page"></i> Trainings y Materiales</a></li>
        
      </ol>
      <div class="btn-group pull-right">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
       <i class="fa fa-gears"></i> <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
      <li><a href="#"><?= $this->Html->link(__('Nuevo Articulo'), ['action' => 'add']) ?></a></li>
      <li><a href="#"><?= $this->Html->link(__('Nueva Categoria'), ['controller' => 'Categories', 'action' => 'add']) ?></a></li>
    </ul>
  </div>
    </section>
    <br>
<div class="col-md-12">

            <?php foreach ($articles as $article): ?>
  <div class="col-md-6 col-sm-6">
    <div class="box" style="padding: 0px; min-height: 295px; border-top: none">
      <div class="bg-blue-active color-palette" style="padding:5px 15px">
                    <h3 class="font-white"><?php echo $this->Html->tag('i', '', array('class' => 'fa fa-book fa-fw '));?>
          <?php
          echo h($article['title']); 
        ?></h3> </div>
    <div style="padding:10px 15px 0px 15px;">
    <p>
        
        <?php echo '<i class="fa fa-file fa-fw pull-left"></i> '.strip_tags ($this->Text->truncate($article['body'],150, 
            array(
              'ellipsis' => '...',
              'exact' => true,
              'html' => true
          ))); ?>
       </em>
    </p>
    <p style="font-weight: 300"><i class="fa fa-clock-o fa-fw"></i> Ultima Modificacion: </em><?php echo h($article['modified']); ?> <br /><i class="fa fa-clock-o fa-fw"></i> Creado: </em>  <?php echo h($article['created']); ?><br><i class="fa fa-flag fa-fw"></i> </em></p>

    </div>
    <div style="padding:0px 10px 15px 10px;" class="text-right">
     <?php
     echo $this->Html->link('Leer mas <i class="fa fa-arrow-circle-right fa-fw"></i>', array('action' => 'view', $article['id']),array('class'=>'btn btn-info', 'escape'=>false));
     ?> 
     </div>
                
            </div>    
  </div>
            <?php endforeach; ?>

    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Ant.')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Sig.') . ' >') ?>
        </ul>

    </div>
</div>
