<br>
<section class="content-header">
      <h1>
        <?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> War Room Digital
        <small>Inicio</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        
      </ol>
    </section>
    <br>
<div style="padding-top:15px">
	<div class="col-lg-6 col-xs-6">
		<div class="small-box bg-aqua">

	        <div class="inner">
	            <h3>
	                <?php echo __('Manual de Comunicación<br /> de crisis');?>
	            </h3>
	        </div>
	        	        <div class="icon">
	            <i class="fa fa-book "></i>
	        </div>
	        <?php echo $this->Html->link('Ver Mas', ['controller'=>'Articles', 'action'=>'manual', 'objeto-del-manual'], ['class'=>'small-box-footer', 'escape'=>false]); ?>
	    </div>
	</div>
	
	<div class="col-lg-6 col-xs-6">
	    <div class="small-box bg-red">

	        <div class="inner">
	            <h3>
	                <?php echo __('Procedimientos en<br /> función de niveles de crisis');?>
	            </h3>
	        </div>
	        	        <div class="icon">
	            <i class="fa fa-life-saver"></i>
	        </div>
	        <?php echo $this->Html->link('Ver Mas', ['controller'=>'Articles', 'action'=>'manual', 'procedimiento-general'], ['class'=>'small-box-footer', 'escape'=>false]); ?>
	    </div>
	</div>
	
	<div class="col-lg-6 col-xs-6">
	    <div class="small-box bg-green">

	        <div class="inner">
	            <h3><br />
	                <?php echo __('War Room');?>
	            </h3>

	        </div>
	        	        <div class="icon">
	            <i class="fa fa-globe"></i>
	        </div>
	        <?php echo $this->Html->link('Ver Mas', ['controller'=>'Crises', 'action'=>'index'], ['class'=>'small-box-footer', 'escape'=>false]); ?>
	    </div>
	</div>
	
	<div class="col-lg-6 col-xs-6">
	    <div class="small-box bg-yellow">

	        <div class="inner">
	            <h3><br />
	                <?php echo __('Nueva Incidencia');?>
	            </h3>
	        </div>
	        	        <div class="icon">
	            <i class="fa fa-bullhorn"></i>
	        </div>
	        <?php echo $this->Html->link('Ver Mas', ['controller'=>'Crises', 'action'=>'add'], ['class'=>'small-box-footer', 'escape'=>false]); ?>
	    </div>
	</div>

	</div>
