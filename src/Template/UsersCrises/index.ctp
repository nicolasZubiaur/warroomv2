<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Users Crisis'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Crises'), ['controller' => 'Crises', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Crisis'), ['controller' => 'Crises', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersCrises index large-9 medium-8 columns content">
    <h3><?= __('Users Crises') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('crisis_id') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersCrises as $usersCrisis): ?>
            <tr>
                <td><?= $this->Number->format($usersCrisis->id) ?></td>
                <td><?= $usersCrisis->has('user') ? $this->Html->link($usersCrisis->user->name, ['controller' => 'Users', 'action' => 'view', $usersCrisis->user->id]) : '' ?></td>
                <td><?= $usersCrisis->has('crisis') ? $this->Html->link($usersCrisis->crisis->name, ['controller' => 'Crises', 'action' => 'view', $usersCrisis->crisis->id]) : '' ?></td>
                <td><?= h($usersCrisis->modified) ?></td>
                <td><?= h($usersCrisis->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usersCrisis->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usersCrisis->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usersCrisis->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersCrisis->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
