<div class="row">
<div class="col-md-12">

<?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar Users Crisis', ['action' => 'edit', $usersCrisis->id], ['escape' => false,
	'class'=>'btn btn-app']
); ?> 

<?= $this->Form->postLink(
						'<i class="fa fa-trash"></i> Borrar Users Crises',
						array('action' => 'delete', $usersCrisis->id),
						array('escape'=>false , 'class' => 'btn btn-app','confirm' => __('Are you sure you want to delete # {0}?', $usersCrisis->id))) ;?>
	
	<?= $this->Html->link(
    '<i class="fa fa-plus"></i> Nuevo Users Crises', ['action' => 'add'],
    ['escape' => false,
	'class'=>'btn btn-app']) ?>

</div>	
	
</div>

<section class="content-header">
          <h1>
            <?= h($usersCrisis->id) ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#"><?= h($usersCrisis->id) ?></a></li>
            <li class="active"></li>
          </ol>
        </section>

<div class="usersCrises view large-9 medium-8 columns content">
  <section class="content">

<div class="row">
   <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?= h($usersCrisis->id) ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <table class="table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $usersCrisis->has('user') ? $this->Html->link($usersCrisis->user->name, ['controller' => 'Users', 'action' => 'view', $usersCrisis->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Crisis') ?></th>
            <td><?= $usersCrisis->has('crisis') ? $this->Html->link($usersCrisis->crisis->name, ['controller' => 'Crises', 'action' => 'view', $usersCrisis->crisis->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersCrisis->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($usersCrisis->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($usersCrisis->created) ?></td>
        </tr>
    </table>
    

			
			</div>	
</div>
</section>
</div>
