<section class="content">
<div class="row">
  <div class="col-md-12">
      <div class="box">
	        <div class="box-header">
	          <h3 class="box-title"><i class="fa fa-add"></i> <?= __('Edit Comment') ?></h3>
	
	        </div>
        	<!-- /.box-header -->
<div class="box-body no-padding">
    <?= $this->Form->create($comment,['type'=>'file']) ?>
	<table class="table" style="width: 90%">
	<tbody>
	<?php
		$status = ['1'=>'Activo', '0'=>'Inactivo'];
	?>
        
<tr><th>Contenido</th><td><?php            echo $this->Form->input('body', ['empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Estatus</th><td><?php            echo $this->Form->input('status', ['empty' => true,'class'=>'form-control','label'=>false, 'options'=>$status]);
?></td></tr>       
          </tbody></table>
          <div style="padding-top: 10px;padding-bottom: 50px;padding-left: 5%;padding-right: 5%">
          	<?= 
              $this->Form->button('<i class="fa fa-floppy-o "></i> Guardar', [
														    'type' => 'submit',
														    'escape' => false,
														    'class' =>'btn btn-primary btn-block btn-lg'
															]); ?>
          </div>
              
    		<?= $this->Form->end() ?>

        </div>
        <!-- /.box-body -->
      </div>

	</div>
    <!-- /.col -->
</div>