<div class="row">
<div class="col-md-12">

<?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar Comment', ['action' => 'edit', $comment->id], ['escape' => false,
	'class'=>'btn btn-app']
); ?> 

<?= $this->Form->postLink(
						'<i class="fa fa-trash"></i> Borrar Comments',
						array('action' => 'delete', $comment->id),
						array('escape'=>false , 'class' => 'btn btn-app','confirm' => __('Are you sure you want to delete # {0}?', $comment->id))) ;?>
	
	<?= $this->Html->link(
    '<i class="fa fa-plus"></i> Nuevo Comments', ['action' => 'add'],
    ['escape' => false,
	'class'=>'btn btn-app']) ?>

</div>	
	
</div>

<section class="content-header">
          <h1>
            <?= h($comment->title) ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#"><?= h($comment->title) ?></a></li>
            <li class="active"></li>
          </ol>
        </section>

<div class="comments view large-9 medium-8 columns content">
  <section class="content">

<div class="row">
   <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?= h($comment->title) ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <table class="table">
        <tr>
            <th><?= __('Parent Comment') ?></th>
            <td><?= $comment->has('parent_comment') ? $this->Html->link($comment->parent_comment->title, ['controller' => 'Comments', 'action' => 'view', $comment->parent_comment->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $comment->has('user') ? $this->Html->link($comment->user->name, ['controller' => 'Users', 'action' => 'view', $comment->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Article') ?></th>
            <td><?= $comment->has('article') ? $this->Html->link($comment->article->title, ['controller' => 'Articles', 'action' => 'view', $comment->article->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($comment->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($comment->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($comment->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $comment->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    

    
        <h3><?= __('Body') ?></h3>
        <?= $this->Text->autoParagraph(h($comment->body)); ?>
    
    <div class="box-footer clearfix">
        <h3><?= __('Related Comments') ?></h3>
        <?php if (!empty($comment->child_comments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Parent Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Article Id') ?></th>
                <th><?= __('Body') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($comment->child_comments as $childComments): ?>
            <tr>
                <td><?= h($childComments->id) ?></td>
                <td><?= h($childComments->parent_id) ?></td>
                <td><?= h($childComments->user_id) ?></td>
                <td><?= h($childComments->article_id) ?></td>
                <td><?= h($childComments->body) ?></td>
                <td><?= h($childComments->status) ?></td>
                <td><?= h($childComments->modified) ?></td>
                <td><?= h($childComments->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Comments', 'action' => 'view', $childComments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Comments', 'action' => 'edit', $childComments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Comments', 'action' => 'delete', $childComments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childComments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
			
			</div>	
</div>
</section>
</div>
