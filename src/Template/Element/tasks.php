<?php echo $this -> Form -> create('Task', array('url' => array('controller' => 'tasks', 'action' => 'add'), 'class' => '')); ?>
<div class="form-body">
	<?php echo $this -> Form -> input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id'])); ?>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Name'); ?></label>
		<div class="col-md-8"><?php echo $this -> Form -> input('name', ['label' => false, 'class' => 'form-control', 'required' => 'required']); ?></div>
	</div>
	
		
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Responsable'); ?></label>
		<div class="col-md-8">
			<select name="user_id" required="required" class="form-control">
				<option value=""></option>
			<?php
				foreach($crisis->users as $usuario){
					echo '<option value="'.$usuario['id'].'">'.$usuario['name'].'</option>';
				}
			?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Inicio'); ?></label>
		<div class="col-md-8">
			<div class="input-group date date-picker input-small" data-date-format="yyyy-mm-dd" data-date-start-date="0y">
				<?php echo $this -> Form -> input('start_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date', 'required' => 'required')); ?>
				<span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-calendar"></i></button></span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Fin'); ?></label>
		<div class="col-md-8">
			<div class="input-group date date-picker input-small" data-date-format="yyyy-mm-dd" data-date-start-date="0y">
				<?php echo $this -> Form -> input('due_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date', 'required' => 'required')); ?>
				<span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-calendar"></i></button></span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Estado'); ?></label>
		<div class="col-md-8"><?php echo $this -> Form -> input('status_id', ['options'=>$status, 'empty'=>true, 'label' => false, 'class' => 'form-control', 'required' => 'required']); ?></div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Priority'); ?></label>
		<div class="col-md-8"><?php echo $this -> Form -> input('priority', ['label' => false, 'class' => 'form-control', 'required' => 'required']); ?></div>
	</div>
	
</div>
<div class="text-center">
	<button class="btn green" type="submit"><i class="fa fa-save"></i> <?php echo __('Guardar'); ?></button>
</div>
<?php echo $this -> Form -> end(); ?>