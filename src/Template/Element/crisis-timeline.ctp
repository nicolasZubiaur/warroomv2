
<div class="nav-tabs-custom" class="pre-scrollable">
<ul class="nav nav-tabs">
	<li class="active" data-toggle="tab"><a href="#tab_1" data-toggle="tab" aria-expanded="true"> <i class="fa fa-envelope-o"></i> Mensajes </a></li>
<li><a href="#tab_2" data-toggle="tab"><i class="fa fa-phone"></i> Contacto con Stakeholder </a></li>
<li><a href="#tab_3" data-toggle="tab"><i class="fa fa-share-alt"></i> Social Media </a></li>
<li><a href="#tab_4" data-toggle="tab"><i class="fa fa-paperclip"></i> Documentos </a></li>
</ul>
 <div class="tab-content">
 	<div class="tab-pane fade active in" id="tab_1">
 	
			<?php		
			foreach($crisis->feeds as $feed) {
				if($feed['parent_id']==null){
					echo '<div class="timeline-item">';
						if(isset($feed['user']['image'])){
							echo $this->Html->div('timeline-badge', $this->Html->image('/'.$feed['user']['image'], array('class' => 'timeline-badge-userpic')));	
						}else{
							echo $this->Html->div('timeline-badge', $this->Html->image('default-avatar.png', array('class' => 'timeline-badge-userpic')));
						}
						
						echo '<div class="timeline-body">';
							echo $this->Html->div('timeline-body-arrow', '');
							echo '<div class="timeline-body-head">';
								echo $this->Html->div('timeline-body-head-caption', 
								$this->Html->link($feed['user']['name'], '#', array('class' => 'timeline-body-title font-blue-madison')) . 
								$this->Html->tag('span', 'Escrito el ' . $feed['created'], array('class' => 'timeline-body-time')));
								$button = $this->Form->postLink('<i class="fa fa-trash-o fa-fw fa-2x"></i>', array('controller'=>'feeds','action' => 'delete', $feed['id']), array('escape' => false, 'class'=>'btn default btn-xs grey'), __('¿Seguro que desea borrar %s?', $feed['matter']));
								echo $this->Html->div('timeline-body-head-actions', $this->Html->div('btn-group', $button));
							echo '</div>';
							echo '<div class="timeline-body-content">';
							if($feed['type']==1){
								echo '<h4>'.$feed['matter'].'</h4>';
								echo '<span class="">'.$feed['post'].'</span>';	
							}else if($feed['type']==2){
								echo '<h4>'.$feed['matter'].'</h4>';
								foreach($crisis->users as $userds){
									if($userds['id']==$feed['assign_user_id']){
										echo '<h5>'.$userds['name'].' -> ';		
										foreach($crisis->connections as $coens){
											if($coens['id']==$feed['connection_id']){
												echo $coens['name'].'</h5>';
											}
										}
									}
								}
								echo '<span class="">'.$feed['post'].'</span>';
							}else if($feed['type']==3){
								echo '<h4>'.$feed['matter'].'</h4>';
								echo '<span class="">'.$feed['twitter_code'].'</span>';
							}else if($feed['type']==4){
								echo '<h4>'.$feed['matter'].'</h4>';
								echo '<p><span class="">'.$feed['post'].'</span></p>';
								echo "<p>Documento: ";
								echo $this -> Html -> link($feed['matter'], '/'.$feed['file'], array('escape' => false, 'target' => '_blank'));
								echo "</p>";
							}
							echo '</div>';
							echo '<hr />';
						//last replies
						foreach($crisis->feeds as $reply) {
							if($reply['parent_id']==$feed['id']){
								echo '<div class="timeline  white-bg ">';
									echo '<div class="timeline-item">';
										if(isset($feed['user']['image'])){
											echo $this->Html->div('timeline-badge', $this->Html->image('/'.$feed['user']['image'], array('class' => 'timeline-badge-userpic')));	
										}else{
											echo $this->Html->div('timeline-badge', $this->Html->image('default-avatar.png', array('class' => 'timeline-badge-userpic')));
										}
										echo '<div class="timeline-body">';
											echo '<div class="timeline-body-head">';
												echo $this->Html->div('timeline-body-head-caption', 
												$this->Html->link($reply['User']['name'], '#', array('class' => 'timeline-body-title font-blue-madison')) . 
												$this->Html->tag('span', 'Escrito el ' . $reply['created'], array('class' => 'timeline-body-time')));
												$button = $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-trash-o fa-fw fa-2x')), array('controller'=>'feeds','action' => 'delete', $reply['id']), array('class' => 'btn default btn-xs grey', 'escape' => false), __('Are you sure you want to delete # %s?', $reply['id']));
												echo $this->Html->div('timeline-body-head-actions', $this->Html->div('btn-group', $button));
											echo '</div>';
											echo $this->Html->div('timeline-body-content', $this->Html->tag('span', $reply['post'], array('class' => '')));
										echo "</div>";
									echo "</div>";
								echo "</div>";
							}
						}
						//end replies
						//reply form
						echo $this->Form->create('Feed', array(
																'url' => array('controller' => 'feeds', 'action' => 'add'),
																'class' => 'form-horizontal form-bordered form-row-stripped', 
																'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-control')));
						echo $this->Form->input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id']));
						echo $this->Form->input('parent_id', array('type' => 'hidden', 'value' => $feed['id']));
						echo $this -> Form -> input('user_id', array('type' => 'hidden', 'value' => $authUser['id']));
						?>
						<div class="form-group">
							<div class="col-md-8 col-sm-8">
								<?php echo $this -> Form -> input('post', array('type' => 'textarea', 'class' => 'form-control', 'rows' => '2', 'placeholder' => 'Añadir comentario', 'label'=>false)); ?>
							</div>
							<div class="col-md-2 col-sm-2">
								<button class="btn green btn-lg" type="submit"><i class="fa fa-comment"></i> <?php echo __('Submit'); ?></button>
							</div>
						</div>
				<?php
				echo $this -> Form -> end();
				//end form reply
				echo '</div>'; //body end
			echo '</div>'; //timelineitem end
				}
			}
			?>
			
	</div>
    <div class="tab-pane fade" id="tab_2">
			<?php
			$users = [];
			foreach($crisis->feeds as $feed) {
				if(($feed['parent_id']==null)&&($feed['type']==2)){
					echo '<div class="timeline-item">';
						if(isset($feed['user']['image'])){
							echo $this->Html->div('timeline-badge', $this->Html->image('/'.$feed['user']['image'], array('class' => 'timeline-badge-userpic')));	
						}else{
							echo $this->Html->div('timeline-badge', $this->Html->image('default-avatar.png', array('class' => 'timeline-badge-userpic')));
						}
						echo '<div class="timeline-body">';
							echo $this->Html->div('timeline-body-arrow', '');
							echo '<div class="timeline-body-head">';
								echo $this->Html->div('timeline-body-head-caption', 
								$this->Html->link($feed['user']['name'], '#', array('class' => 'timeline-body-title font-blue-madison')) . 
								$this->Html->tag('span', 'Escrito el ' . $feed['created'], array('class' => 'timeline-body-time')));
								$button = $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-trash-o fa-fw fa-2x')), array('controller'=>'feeds','action' => 'delete', $feed['id']), array('class' => 'btn default btn-xs grey', 'escape' => false), __('Are you sure you want to delete # %s?', $feed['id']));
								echo $this->Html->div('timeline-body-head-actions', $this->Html->div('btn-group', $button));
							echo '</div>';
							echo '<div class="timeline-body-content">';
								echo '<h4>'.$feed['matter'].'</h4>';
								foreach($crisis->users as $userds){
									if($userds['id']==$feed['assign_user_id']){
										echo '<h5>'.$userds['name'].' -> ';		
										foreach($crisis->connections as $coens){
											if($coens['id']==$feed['connection_id']){
												echo $coens['name'].'</h5>';
											}
										}		
									}
								}
								echo '<span class="">'.$feed['post'].'</span>';
							echo '</div>';
							echo '<hr />';
						//last replies
						foreach($crisis->feeds as $reply) {
							if($reply['parent_id']==$feed['id']){
								echo '<div class="timeline  white-bg ">';
									echo '<div class="timeline-item">';
										if(isset($feed['user']['image'])){
											echo $this->Html->div('timeline-badge', $this->Html->image('/'.$feed['user']['image'], array('class' => 'timeline-badge-userpic')));	
										}else{
											echo $this->Html->div('timeline-badge', $this->Html->image('default-avatar.png', array('class' => 'timeline-badge-userpic')));
										}
										echo '<div class="timeline-body">';
											echo '<div class="timeline-body-head">';
												echo $this->Html->div('timeline-body-head-caption', 
												$this->Html->link($reply['User']['name'], '#', array('class' => 'timeline-body-title font-blue-madison')) . 
												$this->Html->tag('span', 'Escrito el ' . $reply['created'], array('class' => 'timeline-body-time')));
												$button = $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-trash-o fa-fw fa-2x')), array('controller'=>'feeds','action' => 'delete', $reply['id']), array('class' => 'btn default btn-xs grey', 'escape' => false), __('Are you sure you want to delete # %s?', $reply['id']));
												echo $this->Html->div('timeline-body-head-actions', $this->Html->div('btn-group', $button));
											echo '</div>';
											echo $this->Html->div('timeline-body-content', $this->Html->tag('span', $reply['post'], array('class' => '')));
										echo "</div>";
									echo "</div>";
								echo "</div>";
							}
						}
						//end replies
						//reply form
						echo $this->Form->create('Feed', array(
																'url' => array('controller' => 'feeds', 'action' => 'add'),
																'class' => 'form-horizontal form-bordered form-row-stripped', 
																'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-control')));
						echo $this->Form->input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id']));
						echo $this->Form->input('parent_id', array('type' => 'hidden', 'value' => $feed['id']));
						echo $this -> Form -> input('user_id', array('type' => 'hidden', 'value' => $authUser['id']));
						?>

						<div class="form-group">
							<div class="col-md-8 col-sm-8">
								<?php echo $this -> Form -> input('post', array('type' => 'textarea', 'class' => 'form-control', 'rows' => '2', 'placeholder' => 'Añadir comentario', 'label'=>false)); ?>
							</div>
							<div class="col-md-2 col-sm-2">
								<button class="btn green btn-lg" type="submit"><i class="fa fa-comment"></i> <?php echo __('Submit'); ?></button>
							</div>
						</div>
				<?php
				echo $this -> Form -> end();
				//end form reply
				echo '</div>'; //body end
			echo '</div>'; //timelineitem end
				}
			}
			?>
	</div>
	<div class="tab-pane fade" id="tab_3">

			<?php
						
			foreach($crisis->feeds as $feed) {
				if(($feed['parent_id']==null)&&($feed['type']==3)){
					echo '<div class="timeline-item">';
						if(isset($feed['user']['image'])){
							echo $this->Html->div('timeline-badge', $this->Html->image('/'.$feed['user']['image'], array('class' => 'timeline-badge-userpic')));	
						}else{
							echo $this->Html->div('timeline-badge', $this->Html->image('default-avatar.png', array('class' => 'timeline-badge-userpic')));
						}
						echo '<div class="timeline-body">';
							echo $this->Html->div('timeline-body-arrow', '');
							echo '<div class="timeline-body-head">';
								echo $this->Html->div('timeline-body-head-caption', 
								$this->Html->link($feed['user']['name'], '#', array('class' => 'timeline-body-title font-blue-madison')) . 
								$this->Html->tag('span', 'Escrito el ' . $feed['created'], array('class' => 'timeline-body-time')));
								$button = $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-trash-o fa-fw fa-2x')), array('controller'=>'feeds','action' => 'delete', $feed['id']), array('class' => 'btn default btn-xs grey', 'escape' => false), __('Are you sure you want to delete # %s?', $feed['id']));
								echo $this->Html->div('timeline-body-head-actions', $this->Html->div('btn-group', $button));
							echo '</div>';
							echo '<div class="timeline-body-content">';
								echo '<h4>'.$feed['matter'].'</h4>';
								echo '<span class="">'.$feed['twitter_code'].'</span>';
							echo '</div>';
							echo '<hr />';
						//last replies
						foreach($crisis->feeds as $reply) {
							if($reply['parent_id']==$feed['id']){
								echo '<div class="timeline  white-bg ">';
									echo '<div class="timeline-item">';
										if(isset($feed['user']['image'])){
											echo $this->Html->div('timeline-badge', $this->Html->image('/'.$feed['user']['image'], array('class' => 'timeline-badge-userpic')));	
										}else{
											echo $this->Html->div('timeline-badge', $this->Html->image('default-avatar.png', array('class' => 'timeline-badge-userpic')));
										}
										echo '<div class="timeline-body">';
											echo '<div class="timeline-body-head">';
												echo $this->Html->div('timeline-body-head-caption', 
												$this->Html->link($reply['User']['name'], '#', array('class' => 'timeline-body-title font-blue-madison')) . 
												$this->Html->tag('span', 'Escrito el ' . $reply['created'], array('class' => 'timeline-body-time')));
												$button = $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-trash-o fa-fw fa-2x')), array('controller'=>'feeds','action' => 'delete', $reply['id']), array('class' => 'btn default btn-xs grey', 'escape' => false), __('Are you sure you want to delete # %s?', $reply['id']));
												echo $this->Html->div('timeline-body-head-actions', $this->Html->div('btn-group', $button));
											echo '</div>';
											echo $this->Html->div('timeline-body-content', $this->Html->tag('span', $reply['post'], array('class' => '')));
										echo "</div>";
									echo "</div>";
								echo "</div>";
							}
						}
						//end replies 
						//reply form
						echo $this->Form->create('Feed', array(
																'url' => array('controller' => 'feeds', 'action' => 'add'),
																'class' => 'form-horizontal form-bordered form-row-stripped', 
																'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-control')));
						echo $this->Form->input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id']));
						echo $this->Form->input('parent_id', array('type' => 'hidden', 'value' => $feed['id']));
						echo $this -> Form -> input('user_id', array('type' => 'hidden', 'value' => $authUser['id']));
						?>

						<div class="form-group">
							<div class="col-md-8 col-sm-8">
								<?php echo $this -> Form -> input('post', array('type' => 'textarea', 'class' => 'form-control', 'rows' => '2', 'placeholder' => 'Añadir comentario', 'label'=>false)); ?>
							</div>
							<div class="col-md-2 col-sm-2">
								<button class="btn green btn-lg" type="submit"><i class="fa fa-comment"></i> <?php echo __('Submit'); ?></button>
							</div>
						</div>
				<?php
				echo $this -> Form -> end();
				//end form reply
				echo '</div>'; //body end
			echo '</div>'; //timelineitem end
				}
			}
			?>
	</div>
	<div class="tab-pane fade" id="tab_4">

			<?php
						
			foreach($crisis->feeds as $feed) {
				if(($feed['parent_id']==null)&&($feed['type']==4)){
					echo '<div class="timeline-item">';
						if(isset($feed['user']['image'])){
							echo $this->Html->div('timeline-badge', $this->Html->image('/'.$feed['user']['image'], array('class' => 'timeline-badge-userpic')));	
						}else{
							echo $this->Html->div('timeline-badge', $this->Html->image('default-avatar.png', array('class' => 'timeline-badge-userpic')));
						}
						echo '<div class="timeline-body">';
							echo $this->Html->div('timeline-body-arrow', '');
							echo '<div class="timeline-body-head">';
								echo $this->Html->div('timeline-body-head-caption', 
								$this->Html->link($feed['user']['name'], '#', array('class' => 'timeline-body-title font-blue-madison')) . 
								$this->Html->tag('span', 'Escrito el ' . $feed['created'], array('class' => 'timeline-body-time')));
								$button = $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-trash-o fa-fw fa-2x')), array('controller'=>'feeds','action' => 'delete', $feed['id']), array('class' => 'btn default btn-xs grey', 'escape' => false), __('Are you sure you want to delete # %s?', $feed['id']));
								echo $this->Html->div('timeline-body-head-actions', $this->Html->div('btn-group', $button));
							echo '</div>';
							echo '<div class="timeline-body-content">';
								echo '<h4>'.$feed['matter'].'</h4>';
								echo '<p><span class="">'.$feed['post'].'</span></p>';
								echo "<p>Documento: ";
								echo $this -> Html -> link($feed['matter'], '/'.$feed['file'], array('escape' => false, 'target' => '_blank'));
								echo "</p>";
							echo '</div>';
							echo '<hr />';
						//last replies
						foreach($crisis->feeds as $reply) {
							if($reply['parent_id']==$feed['id']){
								echo '<div class="timeline  white-bg ">';
									echo '<div class="timeline-item">';
						if(isset($feed['user']['image'])){
							echo $this->Html->div('timeline-badge', $this->Html->image('/'.$feed['user']['image'], array('class' => 'timeline-badge-userpic')));	
						}else{
							echo $this->Html->div('timeline-badge', $this->Html->image('default-avatar.png', array('class' => 'timeline-badge-userpic')));
						}
										echo '<div class="timeline-body">';
											echo '<div class="timeline-body-head">';
												echo $this->Html->div('timeline-body-head-caption', 
												$this->Html->link($reply['User']['name'], '#', array('class' => 'timeline-body-title font-blue-madison')) . 
												$this->Html->tag('span', 'Escrito el ' . $reply['created'], array('class' => 'timeline-body-time')));
												$button = $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-trash-o fa-fw fa-2x')), array('controller'=>'feeds','action' => 'delete', $reply['id']), array('class' => 'btn default btn-xs grey', 'escape' => false), __('Are you sure you want to delete # %s?', $reply['id']));
												echo $this->Html->div('timeline-body-head-actions', $this->Html->div('btn-group', $button));
											echo '</div>';
											echo $this->Html->div('timeline-body-content', $this->Html->tag('span', $reply['post'], array('class' => '')));
										echo "</div>";
									echo "</div>";
								echo "</div>";
							}
						} 
						//end replies
						//reply form
						echo $this->Form->create('Feed', array(
																'url' => array('controller' => 'feeds', 'action' => 'add'),
																'class' => 'form-horizontal form-bordered form-row-stripped', 
																'inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-control')));
						echo $this->Form->input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id']));
						echo $this->Form->input('parent_id', array('type' => 'hidden', 'value' => $feed['id']));
						echo $this -> Form -> input('user_id', array('type' => 'hidden', 'value' => $authUser['id']));
						?>

						<div class="form-group">
							<div class="col-md-8 col-sm-8">
								<?php echo $this -> Form -> input('post', array('type' => 'textarea', 'class' => 'form-control', 'rows' => '2', 'placeholder' => 'Añadir comentario', 'label'=>false)); ?>
							</div>
							<div class="col-md-2 col-sm-2">
								<button class="btn green btn-lg" type="submit"><i class="fa fa-comment"></i> <?php echo __('Submit'); ?></button>
							</div>
						</div>
				<?php
				echo $this -> Form -> end();
				//end form reply
				echo '</div>'; //body end
			echo '</div>'; //timelineitem end
				}
			}
			?>
	</div>
</div>
</div>