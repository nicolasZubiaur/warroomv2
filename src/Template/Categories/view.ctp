<div class="row">
<div class="col-md-12">

<?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar Category', ['action' => 'edit', $category->id], ['escape' => false,
	'class'=>'btn btn-app']
); ?> 

<?= $this->Form->postLink(
						'<i class="fa fa-trash"></i> Borrar Categories',
						array('action' => 'delete', $category->id),
						array('escape'=>false , 'class' => 'btn btn-app','confirm' => __('Are you sure you want to delete # {0}?', $category->id))) ;?>
	
	<?= $this->Html->link(
    '<i class="fa fa-plus"></i> Nuevo Categories', ['action' => 'add'],
    ['escape' => false,
	'class'=>'btn btn-app']) ?>

</div>	
	
</div>

<section class="content-header">
          <h1>
            <?= h($category->name) ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#"><?= h($category->name) ?></a></li>
            <li class="active"></li>
          </ol>
        </section>

<div class="categories view large-9 medium-8 columns content">
  <section class="content">

<div class="row">
   <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?= h($category->name) ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <table class="table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($category->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Slug') ?></th>
            <td><?= h($category->slug) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($category->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($category->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($category->modified) ?></td>
        </tr>
    </table>
    

    
        <h3><?= __('Description') ?></h3>
        <?= $this->Text->autoParagraph(h($category->description)); ?>
    
    <div class="box-footer clearfix">
        <h3><?= __('Related Connections') ?></h3>
        <?php if (!empty($category->connections)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Office Phone') ?></th>
                <th><?= __('Mobile Phone') ?></th>
                <th><?= __('Job Description') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('File') ?></th>
                <th><?= __('Notes') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($category->connections as $connections): ?>
            <tr>
                <td><?= h($connections->id) ?></td>
                <td><?= h($connections->name) ?></td>
                <td><?= h($connections->office_phone) ?></td>
                <td><?= h($connections->mobile_phone) ?></td>
                <td><?= h($connections->job_description) ?></td>
                <td><?= h($connections->email) ?></td>
                <td><?= h($connections->file) ?></td>
                <td><?= h($connections->notes) ?></td>
                <td><?= h($connections->modified) ?></td>
                <td><?= h($connections->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Connections', 'action' => 'view', $connections->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Connections', 'action' => 'edit', $connections->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Connections', 'action' => 'delete', $connections->id], ['confirm' => __('Are you sure you want to delete # {0}?', $connections->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
			
			</div>	
</div>
</section>
</div>
