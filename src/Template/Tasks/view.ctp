<div class="row">
<div class="col-md-12">

<?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar Task', ['action' => 'edit', $task->id], ['escape' => false,
	'class'=>'btn btn-app']
); ?> 

<?= $this->Form->postLink(
						'<i class="fa fa-trash"></i> Borrar Tasks',
						array('action' => 'delete', $task->id),
						array('escape'=>false , 'class' => 'btn btn-app','confirm' => __('Are you sure you want to delete # {0}?', $task->id))) ;?>

</div>	
	
</div>

<section class="content-header">
          <h1>
            <?= h($task->name) ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#"><?= h($task->name) ?></a></li>
            <li class="active"></li>
          </ol>
        </section>

<div class="tasks view large-9 medium-8 columns content">
  <section class="content">

<div class="row">
   <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?= h($task->name) ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <table class="table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($task->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Crisis') ?></th>
            <td><?= $task->has('crisis') ? $this->Html->link($task->crisis->name, ['controller' => 'Crises', 'action' => 'view', $task->crisis->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $task->has('user') ? $this->Html->link($task->user->name, ['controller' => 'Users', 'action' => 'view', $task->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= h($task->status->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Priority') ?></th>
            <td><?= h($task->priority) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($task->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Start Date') ?></th>
            <td><?= h($task->start_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Due Date') ?></th>
            <td><?= h($task->due_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($task->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($task->created) ?></td>
        </tr>
    </table>
    

			
			</div>	
</div>
</section>
</div>
