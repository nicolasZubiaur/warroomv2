<section class="content">
<div class="row">
  <div class="col-md-12">
      <div class="box">
	        <div class="box-header">
	          <h3 class="box-title"><i class="fa fa-add"></i> <?= __('Edit Task') ?></h3>
	
	        </div>
        	<!-- /.box-header -->
<div class="box-body no-padding">
    <?= $this->Form->create($task,['type'=>'file']) ?>
	<table class="table" style="width: 90%">
	<tbody>

        
<tr><th>Nombre</th><td><?php            echo $this->Form->input('name', ['empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Inidio</th><td><?php            echo $this->Form->input('start_date', ['empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Due Date</th><td><?php            echo $this->Form->input('due_date', ['empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Estatus</th><td><?php			
            echo $this->Form->input('status_id', ['options' => $statuses, 'empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr><tr><th>Prioridad</th><td><?php            echo $this->Form->input('priority', ['empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr>       
          </tbody></table>
          <div style="padding-top: 10px;padding-bottom: 50px;padding-left: 5%;padding-right: 5%">
          	<?= 
              $this->Form->button('<i class="fa fa-floppy-o "></i> Guardar', [
														    'type' => 'submit',
														    'escape' => false,
														    'class' =>'btn btn-primary btn-block btn-lg'
															]); ?>
          </div>
              
    		<?= $this->Form->end() ?>

        </div>
        <!-- /.box-body -->
      </div>

	</div>
    <!-- /.col -->
</div>