<section class="content-header">
      <h1>
        <?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> Etiquetas
        <small>Índice</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"><a href="#"><i class="fa fa-page"></i> Etiquetas</a></li>
        
      </ol>
      <div class="btn-group pull-right">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
       <i class="fa fa-gears"></i> <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
      <li><a href="#"><?= $this->Html->link(__('Nueva Etiqueta'), ['action' => 'add']) ?></a></li>
    </ul>
  </div>
    </section>
<div class="col-md-12">
    <table cellpadding="0" cellspacing="0" class="table table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('slug') ?></th>
                <th><?= $this->Paginator->sort('updated') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tags as $tag): ?>
            <tr>
                <td><?= $this->Number->format($tag->id) ?></td>
                <td><?= $tag->has('user') ? $this->Html->link($tag->user->name, ['controller' => 'Users', 'action' => 'view', $tag->user->id]) : '' ?></td>
                <td><?= $this->Html->link(h($tag->title), ['action' => 'view', $tag->id]) ?></td>
                <td><?= h($tag->slug) ?></td>
                <td><?= h($tag->updated) ?></td>
                <td class="actions">
                    
                    <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $tag->id],['class'=>'btn btn-success btn-sm', 'escape'=>false]) ?>
                    <?= $this->Form->postLink('<i class="fa fa-edit"></i>', ['action' => 'delete', $tag->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tag->id),'class'=>'btn btn-danger btn-sm', 'escape'=>false]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
