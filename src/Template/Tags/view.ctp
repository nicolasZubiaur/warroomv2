<div class="row">
<div class="col-md-12">

<?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar Tag', ['action' => 'edit', $tag->id], ['escape' => false,
	'class'=>'btn btn-app']
); ?> 

<?= $this->Form->postLink(
						'<i class="fa fa-trash"></i> Borrar Tags',
						array('action' => 'delete', $tag->id),
						array('escape'=>false , 'class' => 'btn btn-app','confirm' => __('Are you sure you want to delete # {0}?', $tag->id))) ;?>
	
	<?= $this->Html->link(
    '<i class="fa fa-plus"></i> Nuevo Tags', ['action' => 'add'],
    ['escape' => false,
	'class'=>'btn btn-app']) ?>

</div>	
	
</div>

<section class="content-header">
          <h1>
            <?= h($tag->title) ?>
          </h1>
          <ol class="breadcrumb">
            <li><li><?php echo $this->Html->link('<i class="fa fa-home"></i> <span>Dashboard</span>', ['controller'=>'Pages', 'action'=>'dashboard'], ['escape'=>false]); ?></li></li>
            <li><a href="#"><?= h($tag->title) ?></a></li>
            <li class="active"></li>
          </ol>
        </section>

<div class="tags view large-9 medium-8 columns content">
  <section class="content">

<div class="row">
   <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?= h($tag->title) ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <table class="table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $tag->has('user') ? $this->Html->link($tag->user->name, ['controller' => 'Users', 'action' => 'view', $tag->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($tag->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Slug') ?></th>
            <td><?= h($tag->slug) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tag->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Updated By') ?></th>
            <td><?= $this->Number->format($tag->updated_by) ?></td>
        </tr>
        <tr>
            <th><?= __('Updated') ?></th>
            <td><?= h($tag->updated) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($tag->created) ?></td>
        </tr>
    </table>
    

    
        <h3><?= __('Description') ?></h3>
        <?= $this->Text->autoParagraph(h($tag->description)); ?>
    
    <div class="box-footer clearfix">
        <h3><?= __('Related Crises') ?></h3>
        <?php if (!empty($tag->crises)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Analysis') ?></th>
                <th><?= __('Action') ?></th>
                <th><?= __('Result') ?></th>
                <th><?= __('Category') ?></th>
                <th><?= __('Severity') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Terms') ?></th>
                <th><?= __('City') ?></th>
                <th><?= __('Address') ?></th>
                <th><?= __('Country') ?></th>
                <th><?= __('Google Link') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tag->crises as $crises): ?>
            <tr>
                <td><?= h($crises->id) ?></td>
                <td><?= h($crises->name) ?></td>
                <td><?= h($crises->description) ?></td>
                <td><?= h($crises->Analysis) ?></td>
                <td><?= h($crises->Action) ?></td>
                <td><?= h($crises->Result) ?></td>
                <td><?= h($crises->category) ?></td>
                <td><?= h($crises->severity) ?></td>
                <td><?= h($crises->user_id) ?></td>
                <td><?= h($crises->status) ?></td>
                <td><?= h($crises->modified) ?></td>
                <td><?= h($crises->created) ?></td>
                <td><?= h($crises->terms) ?></td>
                <td><?= h($crises->city) ?></td>
                <td><?= h($crises->address) ?></td>
                <td><?= h($crises->country) ?></td>
                <td><?= h($crises->google_link) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Crises', 'action' => 'view', $crises->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Crises', 'action' => 'edit', $crises->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Crises', 'action' => 'delete', $crises->id], ['confirm' => __('Are you sure you want to delete # {0}?', $crises->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
			
			</div>	
</div>
</section>
</div>
