<h4>Hola <?= $user ?>!</h4>

<p>Te han invitado a la incidencia "<?= $crisis ?>", para verla debes ingresar con las siguientes credenciales: </p>
<ul>
	<li>URL: <a href="http://warroomv2.war-roomdigital.com">warroomv2.war-roomdigital.com</a></li>
	<li>Usuario: <?= $username ?></li>
	<li>Password: <?= $pass ?></li>
</ul>
<p>Una vez ingresado, puedes ver los detalles de ésta aquí:</p>
	<p><a href="http://warroomv2.war-roomdigital.com/crises/view/<?= $id ?>">"<?= $crisis ?>"</a></p>
<p>Puedes cambiar tu contraseña ingresando a tu <a href="http://warroomv2.war-roomdigital.com/users/view/<?= $uid ?>">perfil</a>.</p>
<p>Atte. War Room Digital.</p>