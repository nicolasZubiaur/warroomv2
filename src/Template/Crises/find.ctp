<?php 
//title for layout
	$this->Html
		->addCrumb('War Room', '#');
?>

<div class="row manual index">
	<div class="col-md-12 col-sm-12">
		<h2 style="padding: 20px;" class="title">Entradas Encontradas</h2>
		<div class="portlet box light">
			<div class="portlet-body">
				<?php
				
				foreach($nodes as $node){ ?>
					<div class="note">
						<h3>
							<?php echo $this->Html->link($node['title'],['controller'=>'Articles', 'action'=>'view', $node['id']]);	 ?>	
						</h3> 
						<em>	<?php echo $this->Text->truncate($node['body'], 150, ['ellipsis'=>'...']); ?> </em><br />
						<h6>
							<em>Ultima Modificacion: </em>
							<?php if($node['modified']){
								 echo  $node['modified']->format('d-m-Y H:m'); 
							}  ?> 
							| <em> Creado: </em>  
							<?php if($node['created']){
								echo  $node['created']->format('d-m-Y H:m');
							} 
							?> 
						</h6>
							
                    </div>
				<?php
				}
				
				if(isset($nodes)){
					echo '<p>No se encontraron entradas con esos términos.</p>';
				}
					
				?>
			</div>
		</div>
	</div>
</div>

<div class="row crises index">
	<div class="col-md-12 col-sm-12">
		<h2 style="padding: 20px;" class="title">Crisis Encontradas</h2>
		<div class="portlet box light">
			<div class="portlet-body">

					<?php foreach ($crises as $crisis): ?>
						<div class="note <?php echo $crisis['severity']; ?>">

                                <h3 class="font-white"><?php echo $this->Html->tag('i', '', array('class' => 'fa fa-warning fa-2x fa-fw '));?></td>
							
								<?php
								echo $this->Html->link(h($crisis['name']), array('action' => 'view', $crisis['id'])); 
							?></h3> 
					<em>	<?php echo $this->Text->truncate($crisis['description'], 150, ['ellipsis'=>'...']); ?> </em><br />
							<h6><em>Ultima Modificacion: </em><?php echo h($crisis['modified']); ?> | <em> Creado: </em>  <?php echo h($crisis['created']); ?> </h6>
							
                            </div>

					<?php endforeach; 
					
					if(isset($crises)){
						echo '<p>No se encontraron crisis con esos términos.</p>';
					}
					if($crises){
					?>
					
					

				<div class="paginator">
				        <ul class="pagination">
				            <?= $this->Paginator->prev('< ' . __('previous')) ?>
				            <?= $this->Paginator->numbers() ?>
				            <?= $this->Paginator->next(__('next') . ' >') ?>
				        </ul>
				        <p><?= $this->Paginator->counter() ?></p>
				    
				</div>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>