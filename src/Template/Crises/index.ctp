<div class="content-header">
      <h1>
      <?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?>  War Rooms

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-dashboard"></i> War Rooms</a></li>
      </ol>
    </div>
<div class="clearfix" style="padding-top:20px"></div>

	<div class="col-md-2 col-sm-2 pull-right">
		<div class="">
			<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-plus')) . ' ' . __('Nueva Incidencia'), array('action' => 'add'), array('class' => 'btn btn-block btn-info btn-sm', 'escape' => false)); ?>
			<?php echo " "; ?>
			<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-clock-o')) . ' ' . __('Ver Crisis Cerradas'), array('action' => 'closed'), array('class' => 'btn btn-block btn-danger btn-sm', 'escape' => false)); ?>
		</div>
	</div>
	<div class="Custom_category_filters">
                <div class="col-md-10">
                    <?php
                    $base_url = array('controller' => 'crises', 'action' => 'index');
					$severities = ['verde'=>'Verde', 'naranja'=>'Naranja', 'amarillo'=>'Amarilla', 'rojo'=>'Roja'];
                    echo $this->Form->create("Filter", array('url' => $base_url));
                    echo $this->Form->input("severity", array('label' => ' Filtrar', 'options' => $severities, 'empty' => 'Todas las Severidades'));
					?>
				</div>
				<?php
				
				?>
				<div class="col-md-10">
					<?php
					if(($authUser['role_id']==1)||($authUser['role_id']==3)){ 
						echo $this->Form->input("country", array('label' => false, 'options' => $countries, 'empty' => 'Todos los Países'));
					}else{
						echo $this->Form->input("country", array('type' => 'hidden', 'value' => $authUser['country_id']));
					}
				?>
				</div>
                <div class="col-md-2">
					<button class="btn btn-success btn-block btn-sm" type="submit"><i class="fa fa-filter"></i> <?php echo __('Filtrar'); ?></button>
		        </div>
				<?php
                	echo $this->Form->end();
                ?>
   </div>
<div class="clearfix" style="padding-top:20px"></div>
<hr>
	<?php foreach ($crises as $crisis): 
	switch ($crisis['severity']){
			case 'rojo':
	        $cstyle = 'bg-red-active color-palette';
	        $cstyle1 = 'text-danger';
	        $cstyle2 = 'danger';
	        break;
	        case 'naranja':
	        $cstyle = 'bg-orange-active color-palette';
	        $cstyle1 = 'text-orange';
	        $cstyle2 = 'orange';
	        break;
	        case 'amarillo':
	        $cstyle = 'bg-yellow color-palette';
	        $cstyle1 = 'text-warning';
	        $cstyle2 = 'warning';
	        break;
	       	case 'verde':
	        $cstyle = 'bg-green-active color-palette';
	        $cstyle1 = 'text-success';
	        $cstyle2 = 'success';
	        break;

		}
	?>
	<div class="col-md-6 col-sm-6">
		<div class="box" style="padding: 0px; min-height: 245px; border-top: none">
			<div class="<?php echo $cstyle ?>" style="padding:5px 15px">
                    <h3 class="font-white"><?php echo $this->Html->tag('i', '', array('class' => 'fa fa-warning fa-fw '));?>
					<?php
					echo h($crisis['name']); 
				?></h3> </div>
		<div style="padding:10px 15px 0px 15px;">
		<p>
			<em>	
				<?php echo '<i class="fa fa-file fa-fw pull-left"></i> '.strip_tags ($this->Text->truncate($crisis['description'],150, 
						array(
					    'ellipsis' => '...',
					    'exact' => true,
					    'html' => true
					))); ?>
			 </em>
		</p>
		<p style="font-weight: 300"><em><i class="fa fa-clock-o fa-fw"></i> Ultima Modificacion: </em><?php echo h($crisis['modified']); ?> <br /><em><i class="fa fa-clock-o fa-fw"></i> Creado: </em>  <?php echo h($crisis['created']); ?><br><em><i class="fa fa-flag fa-fw"></i> Pais: </em>  <?php echo h($crisis['country']->name); ?> </p>

		</div>
		<div style="padding:0px 10px 15px 10px;" class="text-right">
		 <?php
		 echo $this->Html->link('Mas informacion <i class="fa fa-arrow-circle-right fa-fw"></i>', array('action' => 'view', $crisis['id']),array('class'=>'btn btn-'.$cstyle2, 'escape'=>false));
		 ?>	
		 </div>
		 						
            </div>    
	</div>
	<?php endforeach; ?>

	<div class="col-md-12">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Ant')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Sig') . ' >') ?>
        </ul>
    </div>

