<?php
//title for layout
$this -> Html -> addCrumb('Crises', '#');

$listOptions = array(

		'frequency' => array('label' => 'Frecuencia', 

			'options' => array('Habitual' => 'Habitual', 'Infrequent' => 'Infrequente', 'very rare' => 'Muy Infrequente', 'extraordinary' => 'Extraordinario')),

		'management' => array('label' => 'Gestión', 

			'options' => array('responsible unit' => 'Unidad responsable', 'one or more units' => 'Una o varias unidades', 'Business / country group' => 'Negocio / país conjunto', 'group' => 'Grupo')),

		'media' => array('label' => 'Intervención de medios / noticias', 

			'options' => array('unlikely' => 'Improbable', 'local-national' => 'Local / Nacional', 'national' => 'Nacional', 'national / international' => 'Nacional / Internacional')),

		'social' => array('label' => 'Movimiento en redes sociales', 

			'options' => array('unlikely' => 'Muy Improbable', 'half' => 'Medio', 'high' => 'Alto', 'very high' => 'muy alto')),

		'authorities' => array('label' => 'Interveción de autoridades y Emergencias', 

			'options' => array('highly unlikey' => 'Muy Improbable', 'possible' => 'Posible', 'possibleO' => 'Posible', 'safe' => 'Segura')),

		'people' => array('label' => 'Personas', 

			'options' => array('unaffected' => 'No afectadas', 'affected' => 'Afectadas', 'affectedO' => 'Afectadas', 'high involvement' => 'Alta afectación')),

		'assets' => array('label' => 'Bienes / propiedades', 

			'options' => array('unaffected' => 'No afectados', 'limited impact' => 'Impacto limitado', 'significant impact' => 'Impacto relevante', 'very severe impact' => 'Impacto muy severo')),

		'incident' => array('label' => 'Ámbito del incidente', 

			'options' => array('local' => 'Local', 'local-national' => 'Local / Nacional', 'national' => 'Nacional', 'national / international' => 'Nacional / Internacional')),

		'alarm' => array('label' => 'Alarma pública', 

			'options' => array('any' => 'Ninguna', 'liekly' => 'Probable', 'very liekly' => 'Muy probable', 'safe' => 'Segura')),

		'facilities' => array('label' => 'Afectación de Instalaciones estratégicas', 

			'options' => array('null' => 'Nula', 'possible' => 'Posible', 'possibleO' => 'Posible', 'safe' => 'Segura')),

		'business' => array('label' => 'Impacto en actividad', 

			'options' => array('none / low' => 'Ninguno / bajo', 'not relevant' => 'No relevante', 'relevant' => 'Relevante', 'very high' => 'Muy alto')),

	);

?>
<?= $this->Html->script('/plugins/ckeditor/ckeditor') ?>
<div class="row crises form">
	<div class="col-md-12">
		<div class="portlet box blue ">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i><?php echo __('Edit Crisis'); ?>				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn btn-default btn-sm" href="#" data-toggle="dropdown"> <i class="fa fa-cogs"></i> Actions <i class="fa fa-angle-down"></i> </a>
						<ul class="dropdown-menu pull-right">
							<li><?php echo $this -> Form -> postLink(__('Delete'), array('action' => 'delete', $this -> Form -> value('Crisis.id')), array(), __('Are you sure you want to delete # %s?', $this -> Form -> value('Crisis.id'))); ?></li>
							<li><?php echo $this -> Html -> link(__('List Crises'), array('action' => 'index')); ?></li>
							<li class="divider"></li>
							<li><?php echo $this -> Html -> link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
							<li><?php echo $this -> Html -> link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
							<li class="divider"></li>
							<li><?php echo $this -> Html -> link(__('List Contacts'), array('controller' => 'connections', 'action' => 'index')); ?> </li>
							<li><?php echo $this -> Html -> link(__('New Contact'), array('controller' => 'connections', 'action' => 'add')); ?> </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				<?php echo $this -> Form -> create($crisis, array('class' => 'form-horizontal form-bordered form-row-stripped')); ?>
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Name'); ?></label>
							<div class="col-md-5">
								<?php echo $this -> Form -> input('severity', array('type' => 'hidden')); ?>
								<?php echo $this -> Form -> input('name', ['label'=> false, 'class' => 'form-control']); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Description'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('description', array('id' => 'description', 'label'=> false, 'class' => 'form-control ckeditor')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Analysis'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Analysis', array('id' => 'Analysis', 'label'=> false, 'class' => 'form-control ckeditor')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Action'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Action', array('id' => 'Action', 'label'=> false, 'class' => 'form-control ckeditor')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Result'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Result', array('id' => 'Result', 'label'=> false, 'class' => 'form-control ckeditor')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Pais'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('country_id', array('label'=> false, 'class' => 'form-control', 'options'=>$countries, 'empty' => true)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Ciudad'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('city', array('label'=> false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Address'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('address', array('label'=> false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Link Google Maps'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('google_link', array('label'=> false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Users'); ?></label>
							<div class="col-md-5">
								<?php echo $this->Form->input('users._ids',  array('class' => 'form-control multi-select', 'label'=> false, 'required'=>'required')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Contact'); ?></label>
							<div class="col-md-5">
								<?php echo $this -> Form -> input('connections._ids', array('class' => 'form-control multi-select', 'label'=> false, 'empty'=>true)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Tags'); ?></label>
							<div class="col-md-5">
								<?php echo $this -> Form -> input('tags._ids', array('class' => 'form-control', 'empty' => true, 'label'=> false)); ?>
							</div>
						</div>
						<?php foreach($listOptions as $typ => $list) { ?>
							<div class="form-group">
								<label class="col-md-3 control-label"><?php echo __($list['label']); ?></label>
								<div class="col-md-9">
									<?php
									echo '<select id="'.$typ.'" name="'.$typ.'" class="form-control tipos" required="required">';
										echo '<option value=""></option>';
										foreach($crisis->crisis_types as $tipos){
											foreach($list['options'] as $key=>$opcion){
												if($tipos['name']==$typ){
													if($tipos['value']==$key){
														echo '<option selected="selected" value="'.$key.'">'.$opcion.'</option>';	
													}else{
														echo '<option value="'.$key.'">'.$opcion.'</option>';
													}
												}
											}
										}
									echo '</select>';
									  ?>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="form-actions right">
						<?php echo $this -> Html -> link(__('Cancel'), $this -> request -> referer(), array('class' => 'btn default')); ?>
						<button class="btn green" type="submit"><i class="fa fa-check"></i> Submit</button>
					</div>
				<?php echo $this -> Form -> end(); ?>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){

		priority = [];
		prioridad = [];

		prioridad[0] = $('#frequency option:selected').val();
		prioridad[1] = $('#management option:selected').val();
		prioridad[2] = $('#media option:selected').val();
		prioridad[3] = $('#social option:selected').val();
		prioridad[4] = $('#authorities option:selected').val();
		prioridad[5] = $('#people option:selected').val();
		prioridad[6] = $('#assets option:selected').val();
		prioridad[7] = $('#incident option:selected').val();
		prioridad[8] = $('#alarm option:selected').val();
		prioridad[9] = $('#facilities option:selected').val();
		prioridad[10] = $('#business option:selected').val();

		if (prioridad[0] == 'extraordinary') {
			priority[0] = 'red';
		} else if (prioridad[0] == 'very rare') {
			priority[0] = 'orange';
		} else if (prioridad[0] == 'Infrequent') {
			priority[0] = 'yellow';
		} else {
			priority[0] = 'green';
		}

		$('#frequency').change(function() {
			console.log('entra');
			if ($('#frequency option:selected').val() == 'extraordinary') {
				priority[0] = 'red';
			} else if ($('#frequency option:selected').val() == 'very rare') {
				priority[0] = 'orange';
			} else if ($('#frequency option:selected').val() == 'Infrequent') {
				priority[0] = 'yellow';
			} else {
				priority[0] = 'green';
			}

		});

		if (prioridad[1] == 'group') {
			priority[1] = 'red';
		} else if (prioridad[1] == 'Business / country group') {
			priority[1] = 'orange';
		} else if (prioridad[1] == 'one or more units') {
			priority[1] = 'yellow';
		} else {
			priority[1] = 'green';
		}

		$('#management').change(function() {
			if ($('#management option:selected').val() == 'group') {
				priority[1] = 'red';
			} else if ($('#management option:selected').val() == 'Business / country group') {
				priority[1] = 'orange';
			} else if ($('#management option:selected').val() == 'one or more units') {
				priority[1] = 'yellow';
			} else {
				priority[1] = 'green';
			}

		});

		if (prioridad[2] == 'national / international') {
			priority[2] = 'red';
		} else if (prioridad[2] == 'national') {
			priority[2] = 'orange';
		} else if (prioridad[2] == 'local-national') {
			priority[2] = 'yellow';
		} else {
			priority[2] = 'green';
		}

		$('#media').change(function() {
			if ($('#media option:selected').val() == 'national / international') {
				priority[2] = 'red';
			} else if ($('#media option:selected').val() == 'national') {
				priority[2] = 'orange';
			} else if ($('#media option:selected').val() == 'local-national') {
				priority[2] = 'yellow';
			} else {
				priority[2] = 'green';
			}

		});

		if (prioridad[3] == 'very high') {
			priority[3] = 'red';
		} else if (prioridad[3] == 'high') {
			priority[3] = 'orange';
		} else if (prioridad[3] == 'half') {
			priority[3] = 'yellow';
		} else {
			priority[3] = 'green';
		}

		$('#social').change(function() {
			if ($('#social option:selected').val() == 'very high') {
				priority[3] = 'red';
			} else if ($('#social option:selected').val() == 'high') {
				priority[3] = 'orange';
			} else if ($('#social option:selected').val() == 'half') {
				priority[3] = 'yellow';
			} else {
				priority[3] = 'green';
			}

		});

		if (prioridad[4] == 'safe') {
			priority[4] = 'red';
		} else if (prioridad[4] == 'possibleO') {
			priority[4] = 'orange';
		} else if (prioridad[4] == 'possible') {
			priority[4] = 'yellow';
		} else {
			priority[4] = 'green';
		}

		$('#authorities').change(function() {
			if ($('#authorities option:selected').val() == 'safe') {
				priority[4] = 'red';
			} else if ($('#authorities option:selected').val() == 'possibleO') {
				priority[4] = 'orange';
			} else if ($('#authorities option:selected').val() == 'possible') {
				priority[4] = 'yellow';
			} else {
				priority[4] = 'green';
			}

		});

		if (prioridad[5] == 'high involvement') {
			priority[5] = 'red';
		} else if (prioridad[5] == 'affectedO') {
			priority[5] = 'orange';
		} else if (prioridad[5] == 'affected') {
			priority[5] = 'yellow';
		} else {
			priority[5] = 'green';
		}

		$('#people').change(function() {
			if ($('#people option:selected').val() == 'high involvement') {
				priority[5] = 'red';
			} else if ($('#people option:selected').val() == 'affectedO') {
				priority[5] = 'orange';
			} else if ($('#people option:selected').val() == 'affected') {
				priority[5] = 'yellow';
			} else {
				priority[5] = 'green';
			}

		});

		if (prioridad[6] == 'very severe impact') {
			priority[6] = 'red';
		} else if (prioridad[6] == 'significant impact') {
			priority[6] = 'orange';
		} else if (prioridad[6] == 'limited impact') {
			priority[6] = 'yellow';
		} else {
			priority[6] = 'green';
		}

		$('#assets').change(function() {
			if ($('#assets option:selected').val() == 'very severe impact') {
				priority[6] = 'red';
			} else if ($('#assets option:selected').val() == 'significant impact') {
				priority[6] = 'orange';
			} else if ($('#assets option:selected').val() == 'limited impact') {
				priority[6] = 'yellow';
			} else {
				priority[6] = 'green';
			}

		});

		if (prioridad[7] == 'national / international') {
			priority[7] = 'red';
		} else if (prioridad[7] == 'national') {
			priority[7] = 'orange';
		} else if (prioridad[7] == 'local-national') {
			priority[7] = 'yellow';
		} else {
			priority[7] = 'green';
		}

		$('#incident').change(function() {
			if ($('#incident option:selected').val() == 'national / international') {
				priority[7] = 'red';
			} else if ($('#incident option:selected').val() == 'national') {
				priority[7] = 'orange';
			} else if ($('#incident option:selected').val() == 'local-national') {
				priority[7] = 'yellow';
			} else {
				priority[7] = 'green';
			}

		});

		if (prioridad[8] == 'safe') {
			priority[8] = 'red';
		} else if (prioridad[8] == 'very liekly') {
			priority[8] = 'orange';
		} else if (prioridad[8] == 'liekly') {
			priority[8] = 'yellow';
		} else {
			priority[8] = 'green';
		}

		$('#alarm').change(function() {
			if ($('#alarm option:selected').val() == 'safe') {
				priority[8] = 'red';
			} else if ($('#alarm option:selected').val() == 'very liekly') {
				priority[8] = 'orange';
			} else if ($('#alarm option:selected').val() == 'liekly') {
				priority[8] = 'yellow';
			} else {
				priority[8] = 'green';
			}

		});

		if (prioridad[9] == 'safe') {
			priority[9] = 'red';
		} else if (prioridad[9] == 'possibleO') {
			priority[9] = 'orange';
		} else if (prioridad[9] == 'possible') {
			priority[9] = 'yellow';
		} else {
			priority[9] = 'green';
		}

		$('#facilities').change(function() {
			if ($('#facilities option:selected').val() == 'safe') {
				priority[9] = 'red';
			} else if ($('#facilities option:selected').val() == 'possibleO') {
				priority[9] = 'orange';
			} else if ($('#facilities option:selected').val() == 'possible') {
				priority[9] = 'yellow';
			} else {
				priority[9] = 'green';
			}

		});

		if (prioridad[10] == 'very high') {
			priority[10] = 'red';
		} else if (prioridad[10] == 'relevant') {
			priority[10] = 'orange';
		} else if (prioridad[10] == 'not relevant') {
			priority[10] = 'yellow';
		} else {
			priority[10] = 'green';
		}

		$('#business').change(function() {
			if ($('#business option:selected').val() == 'very high') {
				priority[10] = 'red';
			} else if ($('#business option:selected').val() == 'relevant') {
				priority[10] = 'orange';
			} else if ($('#business option:selected').val() == 'not relevant') {
				priority[10] = 'yellow';
			} else {
				priority[10] = 'green';
			}

		});

		$('.tipos').change(function() {
			red = 0;
			orange = 0;
			yellow = 0;
			green = 0;
			color = '';
			priority.forEach(function(entry) {
				if (entry == 'red') {
					red++;
				} else if (entry == 'orange') {
					orange++;
				} else if (entry == 'yellow') {
					yellow++;
				} else if (entry == 'green') {
					green++;
				}
			});
			if (green == 11) {
				color = 'verde';
			} else {
				if (red >= 1) {
					color = 'rojo';
				} else if (orange >= 1) {
					color = 'naranja';
				} else if (yellow >= 1) {
					color = 'amarillo';
				}
			}
			$('#severity').val(color);
		});
	
	}); 
</script>