<?php
//title for layout
$this -> Html -> addCrumb('Crises', '#');

$listOptions = array(
		'frequency' => array('label' => 'Frecuencia', 
			'options' => array('Habitual' => 'Habitual', 'Infrequent' => 'Infrequente', 'very rare' => 'Muy Infrequente', 'extraordinary' => 'Extraordinario')),
		'management' => array('label' => 'Gestión', 
			'options' => array('responsible unit' => 'Unidad responsable', 'one or more units' => 'Una o varias unidades', 'Business / country group' => 'Negocio / país conjunto', 'group' => 'Grupo')),
		'media' => array('label' => 'Intervención de medios / noticias', 
			'options' => array('unlikely' => 'Improbable', 'local-national' => 'Local / Nacional', 'national' => 'Nacional', 'national / international' => 'Nacional / Internacional')),
		'social' => array('label' => 'Movimiento en redes sociales', 
			'options' => array('unlikely' => 'Muy Improbable', 'half' => 'Medio', 'high' => 'Alto', 'very high' => 'muy alto')),
		'authorities' => array('label' => 'Interveción de autoridades y Emergencias', 
			'options' => array('highly unlikey' => 'Muy Improbable', 'possible' => 'Posible', 'possibleO' => 'Posible', 'safe' => 'Segura')),
		'people' => array('label' => 'Personas', 
			'options' => array('unaffected' => 'No afectadas', 'affected' => 'Afectadas', 'affectedO' => 'Afectadas', 'high involvement' => 'Alta afectación')),
		'assets' => array('label' => 'Bienes / propiedades', 
			'options' => array('unaffected' => 'No afectados', 'limited impact' => 'Impacto limitado', 'significant impact' => 'Impacto relevante', 'very severe impact' => 'Impacto muy severo')),
		'incident' => array('label' => 'Ámbito del incidente', 
			'options' => array('local' => 'Local', 'local-national' => 'Local / Nacional', 'national' => 'Nacional', 'national / international' => 'Nacional / Internacional')),
		'alarm' => array('label' => 'Alarma pública', 
		'options' => array('any' => 'Ninguna', 'liekly' => 'Probable', 'very liekly' => 'Muy probable', 'safe' => 'Segura')),
		'facilities' => array('label' => 'Afectación de Instalaciones estratégicas', 
			'options' => array('null' => 'Nula', 'possible' => 'Posible', 'possibleO' => 'Posible', 'safe' => 'Segura')),
		'business' => array('label' => 'Impacto en actividad', 
			'options' => array('none / low' => 'Ninguno / bajo', 'not relevant' => 'No relevante', 'relevant' => 'Relevante', 'very high' => 'Muy alto')),
	);
	$severidad = ['verde'=>'Verde', 'naranja'=>'Naranja', 'amarillo'=>'Amarilla', 'rojo'=>'Roja'];
?>
<?= $this->Html->script('/plugins/ckeditor/ckeditor') ?>
<div class="col-md-12">
<div class="box box-body box-success">				
        <h3><?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> <?= __('Nueva Incidencia') ?></h3>

				<!-- BEGIN FORM-->
	<?php echo $this -> Form -> create($crisis); ?>
	<?php echo $this -> Form -> input('severity', array('type' => 'hidden')); ?>
	<?php echo $this -> Form -> input('user_id', array('type' => 'hidden', 'value'=>$authUser['id'])); ?>
	<?php echo $this->Form->label('Nombre'); ?>
	<?php echo $this -> Form -> input('name', ['label'=> false, 'class' => 'form-control']); ?>
	<?php echo $this->Form->label('Descripcion'); ?>
	<?php echo $this->Form->input('description', array('id' => 'description', 'label'=> false, 'class' => 'form-control ckeditor')); ?>
	<?php echo $this->Form->label('Pais'); ?>
	<?php echo $this->Form->input('country_id', array('label'=> false, 'class' => 'form-control', 'options'=>$countries, 'empty' => true)); ?>
	<?php echo $this->Form->label('Ciudad'); ?>
	<?php echo $this->Form->input('city', array('label'=> false, 'class' => 'form-control')); ?>
	<?php echo $this->Form->label('Direccion'); ?>
	<?php echo $this->Form->input('address', array('label'=> false, 'class' => 'form-control')); ?>
	<?php echo $this->Form->label('Link Google Maps'); ?>
	<?php echo $this->Form->input('google_link', array('label'=> false, 'class' => 'form-control')); ?>
	<?php echo $this->Form->label('Tags'); ?>
	<?php echo $this -> Form -> input('tags._ids', array('class' => 'form-control', 'options' => $tags, 'empty' => true, 'label'=> false)); ?>
	<div class="form-group">
    	<div class="checkbox">
        	<label>
            	<input type="checkbox" name="forma" id="forma">
                	Seleccionar manualmente severidad de incidencia
            </label>
       </div>
    </div>

    <div id="manual" style="display: none;">
    	<?php echo $this->Form->label('Severidad'); ?>
		<?php echo $this -> Form -> input('severidad', array('class' => 'form-control', 'options' => $severidad, 'empty' => true, 'label'=> false)); ?>
    </div>
	<?php foreach($listOptions as $typ => $list) { ?>
	<?php echo $this->Form->label($list['label']); ?>
	<?php
		echo $this -> Form -> input($typ, ['class' => 'form-control tipos', 'options' => $list['options'], 'empty' => true, 'label'=> false, 'required'=>'required']);
	  ?>
	<?php } ?>
		<?php echo $this->Form->label('Usuarios asignados'); ?>
	<div class="nav-tabs-custom">
    	<ul class="nav nav-tabs">
        	<li class="active" id="ligroup"><a href="#tab_1" data-toggle="tab">Seleccionar por Grupos</a></li>
            <li id="liusrs"><a href="#tab_2" data-toggle="tab">Seleccionar por Usuario</a></li>
		</ul>
        <div class="tab-content">
        	<div class="tab-pane active" id="tab_1">
        		<div class="col-md-12 col-sm-12">
	        		<p style="text-align: left;" id="mensaje">
	        			<i class="fa fa-exclamation-triangle fa-fw"> </i> Debes seleccionar un País
	        		</p>
	        		<div id="mensaje2" class="text-center" style="display: none;">
						<i class="fa fa-exclamation-triangle fa-3x fa-fw"></i>
						<p>No se encontraron resultados en ese País</p>
					</div>
					<div id="loading" class="text-center" style="display: none;">
						<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
					</div>
					<div id="noload" class="text-center" style="display: none;">
						<i class="fa fa-exclamation-circle fa-3x fa-fw"></i>
						<p>Hubo un error, verifica que se haya seleccionado un País</p>
					</div>
        		</div>
               	<select name="groups[_ids][]" multiple class="form-control select2" style="width: 100%;" id="groups-ids" required="required">
                 	
                </select>
                <br />
                 <?php echo $this->Form->label('Usuarios seleccionados'); ?>
                 <div id="usrs">
                 	<div id="mensaje3" class="text-center" style="display: none;">
						<i class="fa fa-exclamation-triangle fa-3x fa-fw"></i>
						<p>Debes seleccionar un País y un Rol</p>
					</div>
					<div id="mensaje4" class="text-center" style="display: none;">
						<i class="fa fa-exclamation-triangle fa-3x fa-fw"></i>
						<p>No se encontraron resultados</p>
					</div>
					<div id="loading2" class="text-center" style="display: none;">
						<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
					</div>
					<div id="noload2" class="text-center" style="display: none;">
						<i class="fa fa-exclamation-circle fa-3x fa-fw"></i>
						<p>Hubo un error</p>
					</div>
                 	<div id="usuarios">
                 	</div>
                 </div>
			</div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
            	<?php echo $this->Form->input('users._ids',  array('class' => 'form-control select2','style'=>'width: 100%', 'label'=> false, 'required'=>'required')); ?>
            </div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
    </div>
	<?php echo $this->Form->label('Stakeholders relacionados'); ?>
	<?php echo $this -> Form -> input('connections._ids', array('class' => 'form-control select2', 'label'=> false, 'empty'=>true)); ?>
	<div class="row">
    <div class="col-md-6">
    	<button class="btn btn-block btn-success" type="submit"><i class="fa fa-check"></i> Guardar Crisis</button>
    </div>
    <div class="col-md-6">
    	<?php echo $this -> Html -> link('<i class="fa fa-times"></i> Cancelar', $this -> request -> referer(), array('escape'=>false, 'class' => 'btn btn-block btn-danger')); ?>
    </div>
</div>
	
	
	<?php echo $this -> Form -> end(); ?>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		priority = [];
		prioridad = [];
		prioridad[0] = $('#frequency option:selected').val();
		prioridad[1] = $('#management option:selected').val();
		prioridad[2] = $('#media option:selected').val();
		prioridad[3] = $('#social option:selected').val();
		prioridad[4] = $('#authorities option:selected').val();
		prioridad[5] = $('#people option:selected').val();
		prioridad[6] = $('#assets option:selected').val();
		prioridad[7] = $('#incident option:selected').val();
		prioridad[8] = $('#alarm option:selected').val();
		prioridad[9] = $('#facilities option:selected').val();
		prioridad[10] = $('#business option:selected').val();
		if (prioridad[0] == 'extraordinary') {
			priority[0] = 'red';
		} else if (prioridad[0] == 'very rare') {
			priority[0] = 'orange';
		} else if (prioridad[0] == 'Infrequent') {
			priority[0] = 'yellow';
		} else {
			priority[0] = 'green';
		}

		$('#frequency').change(function() {
			console.log('entra');
			if ($('#frequency option:selected').val() == 'extraordinary') {
				priority[0] = 'red';
			} else if ($('#frequency option:selected').val() == 'very rare') {
				priority[0] = 'orange';
			} else if ($('#frequency option:selected').val() == 'Infrequent') {
				priority[0] = 'yellow';
			} else {
				priority[0] = 'green';
			}
		});

		if (prioridad[1] == 'group') {
			priority[1] = 'red';
		} else if (prioridad[1] == 'Business / country group') {
			priority[1] = 'orange';
		} else if (prioridad[1] == 'one or more units') {
			priority[1] = 'yellow';
		} else {
			priority[1] = 'green';
		}

		$('#management').change(function() {
			if ($('#management option:selected').val() == 'group') {
				priority[1] = 'red';
			} else if ($('#management option:selected').val() == 'Business / country group') {
				priority[1] = 'orange';
			} else if ($('#management option:selected').val() == 'one or more units') {
				priority[1] = 'yellow';
			} else {
				priority[1] = 'green';
			}
		});

		if (prioridad[2] == 'national / international') {
			priority[2] = 'red';
		} else if (prioridad[2] == 'national') {
			priority[2] = 'orange';
		} else if (prioridad[2] == 'local-national') {
			priority[2] = 'yellow';
		} else {
			priority[2] = 'green';
		}

		$('#media').change(function() {
			if ($('#media option:selected').val() == 'national / international') {
				priority[2] = 'red';
			} else if ($('#media option:selected').val() == 'national') {
				priority[2] = 'orange';
			} else if ($('#media option:selected').val() == 'local-national') {
				priority[2] = 'yellow';
			} else {
				priority[2] = 'green';
			}
		});

		if (prioridad[3] == 'very high') {
			priority[3] = 'red';
		} else if (prioridad[3] == 'high') {
			priority[3] = 'orange';
		} else if (prioridad[3] == 'half') {
			priority[3] = 'yellow';
		} else {
			priority[3] = 'green';
		}

		$('#social').change(function() {
			if ($('#social option:selected').val() == 'very high') {
				priority[3] = 'red';
			} else if ($('#social option:selected').val() == 'high') {
				priority[3] = 'orange';
			} else if ($('#social option:selected').val() == 'half') {
				priority[3] = 'yellow';
			} else {
				priority[3] = 'green';
			}
		});

		if (prioridad[4] == 'safe') {
			priority[4] = 'red';
		} else if (prioridad[4] == 'possibleO') {
			priority[4] = 'orange';
		} else if (prioridad[4] == 'possible') {
			priority[4] = 'yellow';
		} else {
			priority[4] = 'green';
		}

		$('#authorities').change(function() {
			if ($('#authorities option:selected').val() == 'safe') {
				priority[4] = 'red';
			} else if ($('#authorities option:selected').val() == 'possibleO') {
				priority[4] = 'orange';
			} else if ($('#authorities option:selected').val() == 'possible') {
				priority[4] = 'yellow';
			} else {
				priority[4] = 'green';
			}
		});

		if (prioridad[5] == 'high involvement') {
			priority[5] = 'red';
		} else if (prioridad[5] == 'affectedO') {
			priority[5] = 'orange';
		} else if (prioridad[5] == 'affected') {
			priority[5] = 'yellow';
		} else {
			priority[5] = 'green';
		}

		$('#people').change(function() {
			if ($('#people option:selected').val() == 'high involvement') {
				priority[5] = 'red';
			} else if ($('#people option:selected').val() == 'affectedO') {
				priority[5] = 'orange';
			} else if ($('#people option:selected').val() == 'affected') {
				priority[5] = 'yellow';
			} else {
				priority[5] = 'green';
			}
		});

		if (prioridad[6] == 'very severe impact') {
			priority[6] = 'red';
		} else if (prioridad[6] == 'significant impact') {
			priority[6] = 'orange';
		} else if (prioridad[6] == 'limited impact') {
			priority[6] = 'yellow';
		} else {
			priority[6] = 'green';
		}
		$('#assets').change(function() {
			if ($('#assets option:selected').val() == 'very severe impact') {
				priority[6] = 'red';
			} else if ($('#assets option:selected').val() == 'significant impact') {
				priority[6] = 'orange';
			} else if ($('#assets option:selected').val() == 'limited impact') {
				priority[6] = 'yellow';
			} else {
				priority[6] = 'green';
			}
		});

		if (prioridad[7] == 'national / international') {
			priority[7] = 'red';
		} else if (prioridad[7] == 'national') {
			priority[7] = 'orange';
		} else if (prioridad[7] == 'local-national') {
			priority[7] = 'yellow';
		} else {
			priority[7] = 'green';
		}

		$('#incident').change(function() {
			if ($('#incident option:selected').val() == 'national / international') {
				priority[7] = 'red';
			} else if ($('#incident option:selected').val() == 'national') {
				priority[7] = 'orange';
			} else if ($('#incident option:selected').val() == 'local-national') {
				priority[7] = 'yellow';
			} else {
				priority[7] = 'green';
			}
		});

		if (prioridad[8] == 'safe') {
			priority[8] = 'red';
		} else if (prioridad[8] == 'very liekly') {
			priority[8] = 'orange';
		} else if (prioridad[8] == 'liekly') {
			priority[8] = 'yellow';
		} else {
			priority[8] = 'green';
		}

		$('#alarm').change(function() {
			if ($('#alarm option:selected').val() == 'safe') {
				priority[8] = 'red';
			} else if ($('#alarm option:selected').val() == 'very liekly') {
				priority[8] = 'orange';
			} else if ($('#alarm option:selected').val() == 'liekly') {
				priority[8] = 'yellow';
			} else {
				priority[8] = 'green';
			}
		});

		if (prioridad[9] == 'safe') {
			priority[9] = 'red';
		} else if (prioridad[9] == 'possibleO') {
			priority[9] = 'orange';
		} else if (prioridad[9] == 'possible') {
			priority[9] = 'yellow';
		} else {
			priority[9] = 'green';
		}

		$('#facilities').change(function() {
			if ($('#facilities option:selected').val() == 'safe') {
				priority[9] = 'red';
			} else if ($('#facilities option:selected').val() == 'possibleO') {
				priority[9] = 'orange';
			} else if ($('#facilities option:selected').val() == 'possible') {
				priority[9] = 'yellow';
			} else {
				priority[9] = 'green';
			}
		});

		if (prioridad[10] == 'very high') {
			priority[10] = 'red';
		} else if (prioridad[10] == 'relevant') {
			priority[10] = 'orange';
		} else if (prioridad[10] == 'not relevant') {
			priority[10] = 'yellow';
		} else {
			priority[10] = 'green';
		}

		$('#business').change(function() {
			if ($('#business option:selected').val() == 'very high') {
				priority[10] = 'red';
			} else if ($('#business option:selected').val() == 'relevant') {
				priority[10] = 'orange';
			} else if ($('#business option:selected').val() == 'not relevant') {
				priority[10] = 'yellow';
			} else {
				priority[10] = 'green';
			}
		});

		$('.tipos').change(function() {
			red = 0;
			orange = 0;
			yellow = 0;
			green = 0;
			color = '';
			priority.forEach(function(entry) {
				if (entry == 'red') {
					red++;
				} else if (entry == 'orange') {
					orange++;
				} else if (entry == 'yellow') {
					yellow++;
				} else if (entry == 'green') {
					green++;
				}
			});
			if (green == 11) {
				color = 'verde';
			} else {
				if (red >= 1) {
					color = 'rojo';
				} else if (orange >= 1) {
					color = 'naranja';
				} else if (yellow >= 1) {
					color = 'amarillo';
				}
			}
			$('#severity').val(color);
		});
		
		$('#forma').change(function(){
			if($('#forma').is(':checked')) {
				$('#manual').show();
				$('#severidad').attr('required','required');
				$('#frequency').removeAttr('required');
				$('#management').removeAttr('required');
				$('#media').removeAttr('required');
				$('#social').removeAttr('required');
				$('#authorities').removeAttr('required');
				$('#people').removeAttr('required');
				$('#assets').removeAttr('required');
				$('#incident').removeAttr('required');
				$('#alarm').removeAttr('required');
				$('#facilities').removeAttr('required');
				$('#business').removeAttr('required');
				$('#frequency').attr('disabled','disabled');
				$('#management').attr('disabled','disabled');
				$('#media').attr('disabled','disabled');
				$('#social').attr('disabled','disabled');
				$('#authorities').attr('disabled','disabled');
				$('#people').attr('disabled','disabled');
				$('#assets').attr('disabled','disabled');
				$('#incident').attr('disabled','disabled');
				$('#alarm').attr('disabled','disabled');
				$('#facilities').attr('disabled','disabled');
				$('#business').attr('disabled','disabled');
			}else{
				$('#manual').hide();
				$('#severidad').removeAttr('required');
				$('#frequency').attr('required','required');
				$('#management').attr('required','required');
				$('#media').attr('required','required');
				$('#social').attr('required','required');
				$('#authorities').attr('required','required');
				$('#people').attr('required','required');
				$('#assets').attr('required','required');
				$('#incident').attr('required','required');
				$('#alarm').attr('required','required');
				$('#facilities').attr('required','required');
				$('#business').attr('required','required');
				$('#frequency').removeAttr('disabled');
				$('#management').removeAttr('disabled');
				$('#media').removeAttr('disabled');
				$('#social').removeAttr('disabled');
				$('#authorities').removeAttr('disabled');
				$('#people').removeAttr('disabled');
				$('#assets').removeAttr('disabled');
				$('#incident').removeAttr('disabled');
				$('#alarm').removeAttr('disabled');
				$('#facilities').removeAttr('disabled');
				$('#business').removeAttr('disabled');
			}
		});
		
		$('#severidad').change(function(){
			$('#severity').val($('#severidad').val());
		});
		
		$('#country-id').change(function(){
			$('#groups-ids option').remove();
			$('#usuarios').empty();
			$('#loading').show();
			$('#noload').hide();
			
			if($('#country-id').val()){
				$('#mensaje').hide();
			}else{
				$('#mensaje').show();
			}
			
			$.ajax({
				url: "<?php echo $this->Url->build(["controller" => "Groups", "action" => "searchgroups" ]); ?>",
				data:{
					country: $('#country-id').val(),
				},
				dataType: "json",
				success: function(data){
					if(data.length>0){
						//$('#usrs').append('<select class="form-control" multiple="multiple" id="users-ids" name="users[_ids][]"></select>');
						$.each( data, function( key, value ) {
							$('#groups-ids').append('<option value="'+value.id+'">'+value.name+' ('+ value.role.alias +')</option>');
						});
						$('#loading').hide();	
					}else{
						$('#loading').hide();	
						$('#mensaje2').show(500, function() {
							setTimeout(function() {
								$('#mensaje2').html(function() {
									setTimeout(function() {
										$('#mensaje2').hide();
									}, 1000);
								});
							}, 2500);
						});
					}
				},
				error: function(data){								
					$('#loading').hide();
					$('#noload').show();
				}  
			});
		});
		
		$('#ligroup').click(function(){
			$('select#users-ids option').removeAttr("selected");
			$('#users-ids').removeAttr('required');
			$('#groups-ids').attr('required', 'required');
		});
		
		$('#liusrs').click(function(){
			$('select#groups-ids option').removeAttr("selected");
			$('#usuarios').empty();
			$('#groups-ids').removeAttr('required');
			$('#users-ids').attr('required', 'required');
		});
		
		$('#groups-ids').change(function(){
			$('#usuarios').empty();
			$('#loading2').show();
			$('#noload2').hide();
			
			var grupos = []; 
			$('#groups-ids :selected').each(function(i, selected){ 
			  grupos[i] = $(selected).val(); 
			});
			
			$.ajax({
				url: "<?php echo $this->Url->build(["controller" => "Groups", "action" => "getusers" ]); ?>",
				data:{
					group: grupos,
				},
				dataType: "json",
				success: function(data){
					if(!jQuery.isEmptyObject(data)){
						$.each( data, function( key, value ) {
							$('#usuarios').append('<li>'+value.name+'</li>');
							$('select#users-ids option[value="'+value.id+'"]').attr("selected", "selected");
						});
						$('#loading2').hide();	
					}else{
						$('#loading2').hide();	
						$('#mensaje4').show(500, function() {
							setTimeout(function() {
								$('#mensaje4').html(function() {
									setTimeout(function() {
										$('#mensaje4').hide();
									}, 1000);
								});
							}, 2500);
						});
					}
				},
				error: function(data){								
					$('#loading2').hide();
					$('#noload2').show();
				}  
			});
		});
	}); 
</script>