<?php
//title for layout
$this->Html
        ->addCrumb('War Room');
$this->Html->addCrumb(h($crisis['name']));
echo $this->Html->script(array('crisis'));
/* BEGIN PAGE LEVEL PLUGINS */
echo $this->Html->css(array('tasks', 'timeline'));
/* END PAGE LEVEL PLUGINS */
if (!empty($crisis)):
    ?>

        <div class="col-md-12 col-sm-12">
<h2 style="padding: 20px;" class="crisis_title">
        	 <div class=" pull-right">
	<?php
	switch ($crisis['severity']){
			case 'rojo':
	        $cstyle2 = 'bg-red-active';
	        $cstyle = 'text-danger';
	        $cstyle2 = 'danger';
	        break;
	        case 'naranja':
	        $cstyle2 = 'bg-orange-active';
	        $cstyle = 'text-orange';
	        $cstyle2 = 'danger';
	        break;
	        case 'amarillo':
	        $cstyle2 = 'bg-yellow-active';
	        $cstyle = 'text-warning';
	        $cstyle2 = 'warning';
	        break;
	       	case 'verde':
	        $cstyle2 = 'bg-green-active';
	        $cstyle = 'text-success';
	        $cstyle2 = 'success';
	        break;

		}
	if (($authUser['role_id'] != 4)&&($authUser['role_id'] != 3)) {
		echo $this -> Html -> link($this -> Html -> tag('i', '', array('class' => 'fa fa-edit fa-fw')).' Editar', array('action' => 'edit', $crisis['id']), array('class' => 'btn btn-info','escape' => false));
		echo ' ';
	}
	if ($authUser['role_id'] == 1) {
		echo $this -> Form -> postLink($this -> Html -> tag('i', '', array('class' => 'fa fa-times-circle fa-fw ')).' Cerrar', array('action' => 'close', $crisis['id']), array('class' => 'btn btn-success',  'escape' => false), __('Estás seguro de cerrar la crisis %s?', $crisis['name']));
		echo ' ';
		echo $this -> Form -> postLink($this -> Html -> tag('i', '', array('class' => 'fa fa-trash-o fa-fw ')).' Borrar', array('action' => 'delete', $crisis['id']), array('class' => 'btn btn-danger',  'escape' => false), __('Are you sure you want to delete %s?', $crisis['name']));
	}
	?>
</div>

         <?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> <span class="<?php echo $cstyle ?>"><?php echo h($crisis['name']); ?></span></h2>
               
                			<div class="col-md-3 col-sm-3">
                                <p style="text-align: left;">
                                    <i class="fa fa-info-circle font-blue"></i>
                                    <b>Nivel: <span class="<?php echo $cstyle ?>"><?php echo h($crisis['severity']); ?></span></b>
                                </p>
                            </div>
                           <div class="col-md-3 col-sm-3">
                                <p style="text-align: left;">
                                    <i class="fa fa-info-circle font-blue"></i>
                                    <b>Estatus: </b><?php echo h($crisis['status']); ?>
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <p style="text-align: left;">
                                    <i class="fa fa-calendar font-blue"></i>
                                    <b>Creada: </b><?php echo h($crisis['created']); ?>
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <p style="text-align: right;">
                                    <i class="fa fa-calendar font-blue"></i>
                                    <b>Modificada: </b><?php echo h($crisis['modified']); ?>
                                </p>
                                
                            </div>


<div class="clearfix"></div>

                <div class="">
					<?php if($authUser['role_id']!= 3){ ?>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<br/>
							<div class="col-md-5 col-sm-5">
							<div class="box box-<?php echo $cstyle2 ?>" style="min-height:350px">
							<div class="box-header">
								<h4><i class="fa fa-bookmark"></i> Contexto</h4>
							</div>
							<div class="box-body">
								<?php echo $crisis['description']; ?>
								</div>
							</div>
							</div>
							
							<div class="col-md-3 col-sm-3">
							<div class="box box-<?php echo $cstyle2 ?>" style="min-height:350px">
							<div class="box-header">
								<h4><i class="fa fa-map-marker"></i> Ubicación</h4>
							</div>
							<div class="box-body">
								<p><strong>Ciudad:</strong> <?php echo $crisis['city']; ?></p>
								<p><strong>Dirección:</strong><br /> <?php echo $crisis['address']; ?></p>
								<p><strong>País:</strong><br /><?php echo $crisis['country']->name; ?></p>
								<?php
									if($crisis['google_link']){
								?>
									<a href="<?php echo $crisis['google_link']; ?>" class="btn btn-success" target="_blank"><i class="fa fa-map-marker"></i> Ver mapa</a>
								<?php
									}
								?>
								</div>
							</div>
							</div>
								<div class="col-md-4 col-sm-4">
															<div class="box box-<?php echo $cstyle2 ?>" style="min-height:350px">
							<div class="box-header">
								<h4><i class="fa fa-user"></i> Usuarios</h4>
							</div>
							<div class="box-body">
									<div class="nav-tabs-custom">
						                <ul class="nav nav-tabs">
						                  <li style="margin-right: 0px;" class="active"><a href="#tab1_1" data-toggle="tab">Admin</a></li>
						                  <li style="margin-right: 0px;"><a href="#tab1_2" data-toggle="tab">Operat</a></li>
						                  <li style="margin-right: 0px;"><a href="#tab1_3" data-toggle="tab">Comité</a></li>
						                  <li style="margin-right: 0px;"><a href="#tab1_4" data-toggle="tab">Ejec</a></li>
						                </ul>
						                <div class="tab-content">
						                  <div class="tab-pane active" id="tab1_1">
						                    <table class="table table-striped table-hover" id="Contacts_index">

												<tbody>
													<?php if (!empty($crisis->users)): ?>
													<?php foreach ($crisis->users as $contact): ?>
														<?php if ($contact['role_id']==1): ?>
															<tr>
																<td><i class="fa fa-user text-info"></i> <?php echo $this->Html->link($contact['name'], ['controller'=>'Users', 'action'=>'view', $contact['id']]); ?>	</td>
															</tr>
														<?php endif; ?>
													<?php endforeach; ?>
													<?php endif; ?>
												</tbody>
											</table>
						                  </div><!-- /.tab-pane -->
						                  <div class="tab-pane" id="tab1_2">
						                  	<table class="table table-striped table-hover" id="Contacts_index">

												<tbody>
													<?php if (!empty($crisis->users)): ?>
													<?php foreach ($crisis->users as $contact): ?>
														<?php if ($contact['role_id']==2): ?>
															<tr>
																<td><i class="fa fa-user text-info"></i> <?php echo $this->Html->link($contact['name'], ['controller'=>'Users', 'action'=>'view', $contact['id']]); ?>	</td>
									
															</tr>
														<?php endif; ?>
													<?php endforeach; ?>
													<?php endif; ?>
												</tbody>
											</table>
						                  </div><!-- /.tab-pane -->
						                  <div class="tab-pane" id="tab1_3">
						                   	<table class="table table-striped table-hover" id="Contacts_index">

												<tbody>
													<?php if (!empty($crisis->users)): ?>
													<?php foreach ($crisis->users as $contact): ?>
														<?php if ($contact['role_id']==4): ?>
															<tr>
																<td><i class="fa fa-user text-info"></i> <?php echo $this->Html->link($contact['name'], ['controller'=>'Users', 'action'=>'view', $contact['id']]); ?>	</td>
									
															</tr>
														<?php endif; ?>
													<?php endforeach; ?>
													<?php endif; ?>
												</tbody>
											</table>
						                  </div><!-- /.tab-pane -->
						                  <div class="tab-pane" id="tab1_4">
						                  	<table class="table table-striped table-hover" id="Contacts_index">

												<tbody>
													<?php if (!empty($crisis->users)): ?>
													<?php foreach ($crisis->users as $contact): ?>
														<?php if ($contact['role_id']==3): ?>
															<tr>
																<td><i class="fa fa-user text-info"></i> <?php echo $this->Html->link($contact['name'], ['controller'=>'Users', 'action'=>'view', $contact['id']]); ?>	</td>
									
															</tr>
														<?php endif; ?>
													<?php endforeach; ?>
													<?php endif; ?>
												</tbody>
											</table>
						                  </div><!-- /.tab-pane -->
						                  <a class="btn btn-success" data-toggle="modal" href="#addusers"> <i class="fa fa-plus"></i> Agregar Usuarios</a>
						                </div><!-- /.tab-content -->
						              </div>
								</div>
								</div>
							</div>

					</div>
                    </div>                                    
                
            </div>

                         <div class="col-sm-12 col-md-12">
							<div class="box box-<?php echo $cstyle2 ?>">
							<div class="box-header">
							<div class="col-md-4 pull-right">
								
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mensaje">
 									 <i class="fa fa-envelope-o"></i> <i class="fa fa-plus"></i>
								</button>
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#social-media">
 									 <i class="fa fa-share-alt"></i> <i class="fa fa-plus"></i>
								</button>
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#contacto">
 									 <i class="fa fa-phone"></i> <i class="fa fa-plus"></i>
								</button>
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#archivo">
 									 <i class="fa fa-paperclip"></i> <i class="fa fa-plus"></i>
								</button> 
								<a href="#" class="pull-right" data-widget="collapse"><i class="fa fa-minus"></i> </a>
							</div>
               								<h4><i class="fa fa-clock-o"></i> Historia</h4>
							</div>
							<div class="box-body">
								<?php echo $this->element('crisis-timeline'); ?>
						
							</div>
							</div>
</div>
					

	<div class="">
		<div class="col-md-4 col-sm-4">
		<div class="box box-<?php echo $cstyle2 ?>" style="min-height:380px">
		<div class="box-header">
			<h4>						<i class="fa fa-list"></i>
						<?php echo __('Nueva Tarea'); ?></h4>
		</div>
		<div class="box-body">
<?php echo $this -> Form -> create('Task', array('url' => array('controller' => 'tasks', 'action' => 'add'), 'class' => '')); ?>
<div class="form-body">
	<?php echo $this -> Form -> input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id'])); ?>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Name'); ?></label>
		<div class="col-md-8"><?php echo $this -> Form -> input('name', ['label' => false, 'class' => 'form-control', 'required' => 'required']); ?></div>
	</div>
	
		
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Responsable'); ?></label>
		<div class="col-md-8">
			<select name="user_id" required="required" class="form-control">
				<option value=""></option>
			<?php
				foreach($crisis->users as $usuario){
					echo '<option value="'.$usuario['id'].'">'.$usuario['name'].'</option>';
				}
			?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Inicio'); ?></label>
		<div class="col-md-8">
			<div class="input-group date date-picker input-small" data-date-format="yyyy-mm-dd" data-date-start-date="0y">
				<?php echo $this -> Form -> input('start_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date', 'required' => 'required')); ?>
				<span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-calendar"></i></button></span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Fin'); ?></label>
		<div class="col-md-8">
			<div class="input-group date date-picker input-small" data-date-format="yyyy-mm-dd" data-date-start-date="0y">
				<?php echo $this -> Form -> input('due_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date', 'required' => 'required')); ?>
				<span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-calendar"></i></button></span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Estado'); ?></label>
		<div class="col-md-8"><?php echo $this -> Form -> input('status_id', ['options'=>$status, 'empty'=>true, 'label' => false, 'class' => 'form-control', 'required' => 'required']); ?></div>
	</div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __('Priority'); ?></label>
		<div class="col-md-8"><?php echo $this -> Form -> input('priority', ['label' => false, 'class' => 'form-control', 'required' => 'required']); ?></div>
	</div>
	
</div>
<div class="text-center">
	<button class="btn green" type="submit"><i class="fa fa-save"></i> <?php echo __('Guardar'); ?></button>
</div>
<?php echo $this -> Form -> end(); ?>
					</div>
				</div>
					
				</div>

		<div class="col-md-4 col-sm-4">
			<!-- BEGIN PORTLET-->
			<div class="box box-<?php echo $cstyle2 ?>" style="min-height:380px">
			<div class="box-header">
			<h4>						<i class="fa fa-list"></i>
						<?php echo __('Tareas'); ?></h4>
			</div>
			<div class="box-body">
									<ul class="chats">
							<?php 
								if (empty($crisis->tasks)){
									?><br /><h4 class="text-center">Aun no hay tareas asignadas<br /> a esta crisis</h4><?php
								}
							foreach($crisis->tasks as $key => $task) { ?>
							<li class="in">
								<?php 
								if($task['user']['image']){
									echo $this -> Html -> image('/'.$task['user']['image'], array('class' => 'avatar')); 	
								}else{
									echo $this -> Html -> image('default-avatar.png', array('class' => 'avatar')); 
								}
								?>
								<div class="message">
									<span class="arrow">
									</span>
									<span><?php echo $this->Html->link($task['name'], array('controller' => 'tasks', 'action' => 'view', $task['id'])); ?></span>
									<span class="body"><?php echo $task['user']['name'];?> </span>
									<span class="datetime"><i class="fa fa-calendar"></i>
									<?php echo $task['created']; ?> </span>
								</div>
							</li>
							<?php } ?>
						</ul>

			</div>
			</div>
			<!-- END PORTLET-->
		</div>

		<div class="col-md-4 col-sm-4">
		                         <div class="col-sm-12 col-md-12">
							<div class="box box-<?php echo $cstyle2 ?>" style="min-height:380px">
							<div class="box-header">
								<h4><i class="fa fa-download"></i> Ultimos Documentos</h4>
							</div>
							<div class="box-body">
								<ul class="task-list">
							<?php

							foreach ($crisis->feeds as $document) {
								if($document['type']==4){
									$task_title = $this -> Html -> div('task-title', $this -> Html -> link($this -> Html -> tag('span', $document['matter'], array('class' => 'task-title-sp')), '/'. $document['file'], array('escape' => false, 'target' => '_blank')));

									$attributes = array();
									if ($document == end($crisis->feeds)) {
										$attributes = array('class' => 'last-line');
									}
									echo $this -> Html -> tag('li', $task_title, $attributes);	
								}
							}
							?>
							</ul>
						
							</div>
							</div>
					</div>
			<!-- BEGIN PORTLET-->

		</div>
	</div>
<div class="col-sm-12 col-md-12">
	<div class="box box-<?php echo $cstyle2 ?>">
	<div class="box-header">
		<h4><i class="fa fa-users"></i> Stakeholders Involucrados</h4>
	</div>
	<div class="box-body">
				<?php foreach ($crisis->connections as $contact) {?>
				<div class="col-md-6 col-sm-12 margin-bottom-10 no-padding-right">
					<div class="col-md-2 col-sm-12 no-space media-object">
						<?php
							if (is_null($contact['file'])) {
								echo $this->Html->image('default-avatar.png', array('class' => 'img-responsive img-rounded'));
							} else {
								echo $this->Html->image('/' . $contact['file'], array('class' => 'img-responsive img-rounded'));
							}
							
						?>
					</div>
					<div class="col-md-10 col-sm-12 no-padding-right">
						<h3 class="font-blue-madison margin-top-0">
							<?php echo $this->Html->link($contact['name'], ['controller'=>'Connections', 'action'=>'view', $contact['id']]); ?>						
						</h3>
						<span class="media-object">
						<i class="fa fa-envelope-o btn blue btn-xs">
						</i> <?php echo $this->Text->autoLinkEmails($contact['email']); ?>
						</span>
						<span class="media-object">
						<i class="fa fa-phone btn green btn-xs">
						</i> <?php echo $contact['mobile_phone']; ?>
						</span>
					</div>
				
				</div>
				<?php } ?>

	</div>
</div>
</div>
<?php } ?>
				
	<div class="clearfix"></div>
<div class="col-md-12">
          <!-- Custom Tabs (Pulled to the right) -->
          <div class="nav-tabs-custom nav-tabs-<?php echo $cstyle2 ?>">
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#tab_1-1" data-toggle="tab"><i class="fa fa-book"></i> Analisis</a></li>
              <li><a href="#tab_2-2" data-toggle="tab"><i class="fa fa-book"></i> Acciones</a></li>
              <li><a href="#tab_3-2" data-toggle="tab"><i class="fa fa-book"></i> Resultados</a></li>
              <li class="pull-left header"><i class="fa fa-book"></i> Resumen Ejecutivo</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">
                <div class="pull-right" style="margin:5px; min-height:25px"><a class="btn btn-info" data-toggle="modal" href="#analisis"><i class="fa fa-edit"></i></a></div>
				<?php if(isset($crisis['Analysis'])){ echo $crisis['Analysis']; }
						else { echo "<br><h4 class='text-center'>Analisis aun no ha sido llenado. Da click en el boton azul para contribuir a la crisis.</h4> <br>";}?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2-2">
              	<div class="pull-right" style="margin:5px; min-height:25px"><a class="btn btn-info" data-toggle="modal" href="#acciones"><i class="fa fa-edit"></i></a></div>
				<?php if(isset($crisis['Action'])){ echo $crisis['Action']; }
						else { echo "<br><h4 class='text-center'>Acciones aun no ha sido llenado. Da click en el boton azul para contribuir a la crisis.</h4> <br>";}?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3-2">
              	<div class="pull-right" style="margin:5px; min-height:25px"><a class="btn btn-info" data-toggle="modal" href="#resultado"><i class="fa fa-edit"></i></a></div>
				<?php if(isset($crisis['Result'])){ echo $crisis['Result'];}
						else { echo "<br><h4 class='text-center'>Resultados aun no ha sido llenado. Da click en el boton azul para contribuir a la crisis.</h4> <br>" ;}?>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
					

				<?php endif; ?>
				
	<div class="modal fade bs-modal-lg" id="mensaje" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Nuevo Mensaje</h4>
				</div>
				<div class="modal-body"> 
					<?php echo $this -> Form -> create('Feed', array('url' => array('controller' => 'feeds', 'action' => 'add'), 'type' => 'file', 'class' => '')); ?>
					<?php echo $this -> Form -> input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id'])); ?>
					<?php echo $this -> Form -> input('user_id', array('type' => 'hidden', 'value' => $authUser['id'])); ?>
					<?php echo $this -> Form -> input('type', array('type' => 'hidden', 'value' => 1)); ?>
					
					<div class="form-group">
					<div class="col-md-12 col-sm-12">
						<?php echo $this -> Form -> input('matter', array('type'=>'text','required' => true,'placeholder'=>'Anota el asunto aqui...', 'label' => false, 'class' => 'form-control')); ?>
					</div>
					<div class="col-md-12 col-sm-12">
						<?php echo $this->Form->input('post', array('id' => 'post','placeholder'=>'Anota el mensaje aqui...', 'label' => false, 'class' => 'form-control')); ?>
					</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn dark btn-outline">Cerrar</button>
					<button class="btn green" type="submit"><i class="fa fa-check"></i> <?php echo __('Submit'); ?></button>
					<?php echo $this -> Form -> end(); ?>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
		<div class="modal fade bs-modal-lg" id="social-media" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Nuevo Social Media</h4>
				</div>
				<div class="modal-body"> 
					<?php echo $this -> Form -> create('Feed', array('url' => array('controller' => 'feeds', 'action' => 'add'), 'type' => 'file', 'class' => '')); ?>
					<?php echo $this -> Form -> input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id'])); ?>
					<?php echo $this -> Form -> input('user_id', array('type' => 'hidden', 'value' => $authUser['id'])); ?>
					<?php echo $this -> Form -> input('type', array('type' => 'hidden', 'value' => 3)); ?>
					<div class="form-group">
						<div class="col-md-12 col-sm-12">
							<?php echo $this -> Form -> input('matter', array('type'=>'text','required' => true,'placeholder'=>'Anota el asunto aqui...', 'label' => false, 'class' => 'form-control')); ?>
						</div>
						<div class="col-md-12 col-sm-12">
							<?php echo $this -> Form -> input('twitter_code', array('type' => 'textarea', 'class' => 'form-control', 'rows' => '2', 'placeholder' => '< / > Pega aqui el codigo embed de Twitter, Facebook o Youtube', 'label' => false, 'class' => 'form-control')); ?>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn dark btn-outline">Cerrar</button>
					<button class="btn green" type="submit"><i class="fa fa-check"></i> <?php echo __('Submit'); ?></button>
					<?php echo $this -> Form -> end(); ?>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div></div>
	<!-- /.modal -->
	
	
		<div class="modal fade bs-modal-lg" id="contacto" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Nuevo Contacto con Stakeholder</h4>
				</div>
				<div class="modal-body"> 
					<?php echo $this -> Form -> create('Feed', array('url' => array('controller' => 'feeds', 'action' => 'add'), 'type' => 'file', 'class' => '')); ?>
						<?php echo $this -> Form -> input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id'])); ?>
						<?php echo $this -> Form -> input('user_id', array('type' => 'hidden', 'value' => $authUser['id'])); ?>
						<?php echo $this -> Form -> input('type', array('type' => 'hidden', 'value' => 2)); ?>
						<div class="form-group">
							<div class="col-md-12 col-sm-12">
								<?php echo $this -> Form -> input('matter', array('type'=>'text','required' => true,'placeholder'=>'Anota el asunto aqui...', 'label' => false, 'class' => 'form-control')); ?></div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6">
									<label class="control-label">Usuario que hizo el contacto</label>
									<select name="assign_user_id" class="form-control" required="required" id="assign-user-id">
										<option value=""></option>
									<?php 
										foreach($crisis->users as $participant){
											echo '<option value="'.$participant['id'].'">'.$participant['name'].'</option>';
										}
									?>
									</select>
								</div>
								<div class="col-md-6 col-sm-6">
									<label class="control-label">Stakeholder Involucrado</label>
									<select name="connection_id" class="form-control" required="required" id="connection-id">
										<option value=""></option>
										<?php 
										foreach($crisis->connections as $conection){
											echo '<option value="'.$conection['id'].'">'.$conection['name'].'</option>';
										}
									?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 col-sm-12">
									<?php echo $this -> Form -> input('post', array('type' => 'textarea', 'class' => 'form-control', 'rows' => '2', 'placeholder' => 'Añadir mensaje', 'label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn dark btn-outline">Cerrar</button>
					<button class="btn green" type="submit"><i class="fa fa-check"></i> <?php echo __('Submit'); ?></button>
					<?php echo $this -> Form -> end(); ?>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div></div>
	<!-- /.modal -->
	
	<div class="modal fade bs-modal-lg" id="archivo" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Nuevo Documento</h4>
		</div>
			<div class="modal-body"> 
				<?php echo $this -> Form -> create('Feed', array('url' => array('controller' => 'feeds', 'action' => 'add'), 'type' => 'file', 'class' => '')); ?>
				<?php echo $this -> Form -> input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id'])); ?>
				<?php echo $this -> Form -> input('user_id', array('type' => 'hidden', 'value' => $authUser['id'])); ?>
				<?php echo $this -> Form -> input('type', array('type' => 'hidden', 'value' => 4)); ?>
					<div class="form-group">
						<div class="col-md-12 col-sm-12">
							<?php echo $this -> Form -> input('matter', array('type'=>'text','required' => true,'placeholder'=>'Descripción del archivo...', 'label' => false, 'class' => 'form-control')); ?>
						</div>
						<div class="col-md-12 col-sm-12">
							<?php echo $this -> Form -> input('post', array('type' => 'textarea', 'class' => 'form-control', 'rows' => '2', 'placeholder' => 'Añadir comentario', 'label' => false, 'class' => 'form-control')); ?>
						</div>
					</div>
				<div class="form-group">
					<div class="col-md-12">
					<div class="fileinput fileinput-new" data-provides="fileinput">
						<div class="input-group input-large">
							<div class="form-control uneditable-input" data-trigger="fileinput">
								<i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"></span>
							</div>
							<span class="input-group-addon btn default btn-file">
							<span class="fileinput-new"><?php echo __('Select file'); ?> </span>
							<span class="fileinput-exists"><?php echo __('Change'); ?> </span>
							<?php echo $this -> Form -> input('file_file', array('type' => 'file', 'class' => false)); ?>
							</span>
							<a href="#" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"><?php echo __('Remove'); ?> </a>
						</div>
					</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn dark btn-outline">Cerrar</button>
				<button class="btn green" type="submit"><i class="fa fa-check"></i> <?php echo __('Submit'); ?></button>
				<?php echo $this -> Form -> end(); ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
<div class="modal fade bs-modal-lg" id="addusers" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Agregar Usuarios</h4>
		</div>
			<div class="modal-body"> 
				<?php echo $this -> Form -> create('Crises', array('url' => array('action' => 'addusers'), 'class' => '')); ?>
				<?php echo $this -> Form -> input('crisis_id', array('type' => 'hidden', 'value' => $crisis['id'])); ?>
					<div class="form-group">
						<div class="col-md-12 col-sm-12">
							<label class="control-label">Agregar Usuario</label>
							<?php echo $this -> Form -> input('user_id', array('options'=>$usuarios,'required' => true, 'class'=>'selectpicker','data-live-search'=>true, 'data-width'=>'100%', 'empty'=>true, 'label' => false, 'class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 col-sm-12">
							<label class="control-label">No lo encuentras?</label>
								<?php echo $this -> Html -> link($this -> Html -> tag('i', '', array('class' => 'fa fa-plus')) . ' ' . __('Invitar Usuarios'), ['controller'=>'Users', 'action'=>'usercrisis', $crisis['id']], array('class' => 'btn green-meadow', 'escape' => false, 'plugin' => false));  ?>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn dark btn-outline">Cerrar</button>
				<button class="btn green" type="submit"><i class="fa fa-check"></i> <?php echo __('Submit'); ?></button>
				<?php echo $this -> Form -> end(); ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade bs-modal-lg" id="analisis" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Editar Crisis</h4>
		</div>
			<div class="modal-body"> 
				<?php 
					echo $this -> Form -> create($crisis, array('url' => array('action' => 'edit'), 'class' => ''));
					echo $this->Form->input('action', ['value'=>'view', 'type'=>'hidden']); 
				?>
					<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Análisis'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Analysis', array('id' => 'Analysis', 'label'=> false, 'class' => 'form-control ckeditor')); ?>
							</div>
					</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn dark btn-outline">Cerrar</button>
				<button class="btn green" type="submit"><i class="fa fa-check"></i> <?php echo __('Submit'); ?></button>
				<?php echo $this -> Form -> end(); ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
</div>
<div class="modal fade bs-modal-lg" id="acciones" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Editar Crisis</h4>
		</div>
			<div class="modal-body"> 
				<?php 
					echo $this -> Form -> create($crisis, array('url' => array('action' => 'edit'), 'class' => ''));
					echo $this->Form->input('action', ['value'=>'view', 'type'=>'hidden']); 
				?>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Acciones'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Action', array('id' => 'Result', 'label'=> false, 'class' => 'form-control ckeditor')); ?>
							</div>
						</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn dark btn-outline">Cerrar</button>
				<button class="btn green" type="submit"><i class="fa fa-check"></i> <?php echo __('Submit'); ?></button>
				<?php echo $this -> Form -> end(); ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
<div class="modal fade bs-modal-lg" id="resultado" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Editar Crisis</h4>
		</div>
			<div class="modal-body"> 
				<?php 
					echo $this -> Form -> create($crisis, array('url' => array('action' => 'edit'), 'class' => ''));
					echo $this->Form->input('action', ['value'=>'view', 'type'=>'hidden']); 
				?>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo __('Resultados'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Result', array('id' => 'Action', 'label'=> false, 'class' => 'form-control ckeditor')); ?>
							</div>
						</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn dark btn-outline">Cerrar</button>
				<button class="btn green" type="submit"><i class="fa fa-check"></i> <?php echo __('Submit'); ?></button>
				<?php echo $this -> Form -> end(); ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
<?php
	echo $this->Html->script('bootstrap-select', array('block' => 'script'));
	echo $this->Html->css('bootstrap-select.min.css', array('block' => 'css'));
?>
<!-- Datepicker -->
	<?= $this->Html->css('/plugins/datepicker/datepicker3.css'); ?>
    <?= $this->Html->script('/plugins/datepicker/bootstrap-datepicker.js') ?>
					
						<script>
							jQuery(document).ready(function() {
								$('.date').datepicker({
								    format: "yyyy-mm-dd"
								});

							});
						</script>
						<!-- END JAVASCRIPTS -->