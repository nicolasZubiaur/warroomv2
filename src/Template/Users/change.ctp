<?php
	$user = $this->request->query('user');
	$token = $this->request->query('token');
?>
		<?= $this->Form->create('',array('class'=>'js-validation-login form-horizontal push-30-t push-50','style'=>'background-color:#FFF;padding:40px')) ?>

		    	<div class="text-center"><?php //echo $this->Html->image('exmar-logo.png', ['alt' => 'Exmar']); ?></div>
		    	<?= $this->Flash->render('auth') ?>
		        <h1 class="text-center"><?= __('Cambiar Contraseña') ?></h1>
		        <?= $this -> Flash -> render() ?>
		        <div id="msg" class="messagesE" style="display: none;">Las contraseñas no coinciden, favor de verificarlo</div>
		        <div class="form-group">
		        	<div class="col-xs-12">
		        		<div class="form-material form-material-primary floating">
		        			<?= $this->Form->input('user',array('type'=>'hidden', 'value'=>$user)) ?>
		        			<?= $this->Form->input('token',array('type'=>'hidden', 'value'=>$token)) ?>
		        			<?= $this->Form->input('email',array('class'=>'form-control', 'required'=>'required')) ?>
		        		</div>
		        	</div>
		        </div>
		        <div class="form-group">
		        	<div class="col-xs-12">
		        		<div class="form-material form-material-primary floating">
		        		<?= $this->Form->input('password1',array('class'=>'form-control','label'=>'Nueva Contraseña','type'=>'password', 'required'=>'required', 'pattern'=>'.{6,}', 'required title'=>'min 6 caracteres')) ?>
		      		  	</div>
		      		</div>
		      	</div>
		      	<div class="form-group">
		        	<div class="col-xs-12">
		        		<div class="form-material form-material-primary floating">
		        		<?= $this->Form->input('password2',array('class'=>'form-control','label'=>'Confirma Contraseña','type'=>'password', 'required'=>'required', 'min')) ?>
		      		  	</div>
		      		</div>
		      	</div>
                <div class="form-group">
                	<div class="col-xs-12 col-sm-6 col-md-4">
						<?= $this->Form->button(__('Guardar'),array('class'=>'btn btn-primary btn-md')); ?>
					</div>
				</div>

		<?= $this->Form->end() ?>

<script>
	$(document).ready(function(){
		
		$('#password2').change(function(){
			if($('#password2').val()!==$('#password1').val()){
				$('#msg').show();
				$('#password2').val('');
			}else{
				$('#msg').hide();
			}
		});
	    
	});
</script>


