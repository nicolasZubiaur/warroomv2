<?= $this->Html->script('/plugins/ckeditor/ckeditor') ?>
<div class="col-md-12"><div class="box box-body box-success">
        <h3><?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> <?= __('Nuevo Usuario') ?></h3>
    <hr class="navy">
    <?= $this->Form->create($user,['type'=>'file']);
        $status = [ '1'=>'Activo','0'=>'Inactivo'];
        ?>
<div class="row">
    <div class="col-md-6">
        <?php  
        echo $this->Form->label('username', 'Nombre de usuario');
        echo $this->Form->input('username', array('label'=>false)); ?>
    </div>
        <div class="col-md-6">

        <?php  
        echo $this->Form->label('name', 'Nombre');
        echo $this->Form->input('name', array('label'=>false)); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?php 
        echo $this->Form->label('job_description', 'Puesto');
        echo $this->Form->input('job_description', array('label'=>false)); ?>
    </div>
        <div class="col-md-6">

        <?php
        echo $this->Form->label('email', 'Email'); 
        echo $this->Form->input('email', array('label'=>false)); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">

        <?php 
        echo $this->Form->label('password', 'Password');
        echo $this->Form->input('password', array('label'=>false)); ?>
    </div>
        <div class="col-md-6">
        <?php $options = [ '2' => 'Operativo', '3' => 'Comitee','1' => 'Admin', '4' => 'Publico'];
             echo $this->Form->label('role_id', 'Permisos');
             echo $this->Form->select('role_id', $options, array('label'=>false)); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?php 
        echo $this->Form->label('telephone', 'Telefono');
        echo $this->Form->input('telephone', array('label'=>false)); ?>
    </div>
        <div class="col-md-6">
        <?php 
        echo $this->Form->label('website', 'Sitio Web');
        echo $this->Form->input('website', array('label'=>false)); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php  
        echo $this->Form->label('bio', 'Acerca de');
        echo $this->Form->input('bio', ['class'=>'ckeditor','label'=>false]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?php echo $this->Form->input('status', ['options'=>$status,'label'=>false]); ?>
    </div>
        <div class="col-md-6">
        <?php echo $this->Form->input('image_file', ['type' => 'file','label'=>false]); ?>
    </div>
</div>
    <?= $this->Form->button('Guardar usuario', array('class'=>'btn btn-block btn-success')) ?>
    <?= $this->Form->end() ?>
</div>
</div>