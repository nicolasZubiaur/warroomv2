<div><?= $this->Flash->render('auth') ?></div>
    <div class="login-box">
      <div class="login-logo">
        <?= $this->Html->image('logo.png', ['class'=>'img-responsive']); ?>
      </div><!-- /.login-logo -->
      <div class="login-box-body"> 
		<?= $this->Form->create() ?>
          <div class="form-group has-feedback">
            <?= $this->Form->input('username',['label'=>false, 'placeholder'=>'Username', 'class'=>'form-control','div'=>'false', 'required'=>'required']) ?>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <?= $this->Form->input('password',['label'=>false, 'placeholder'=>'Password', 'class'=>'form-control','div'=>'false', 'required'=>'required']) ?>
			<span class="fa fa-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-6">
                      <a href="#" data-toggle="modal" data-target="#forgot"> Olvidé mi contraseña
</a>
            </div><!-- /.col -->
            <div class="col-xs-6">
              <?= $this->Form->button(__('Iniciar Sesión'), ['type'=>'submit', 'class'=>'btn btn-primary btn-block btn-flat']); ?>
            </div><!-- /.col -->
          </div>
        <?= $this->Form->end() ?>



      </div><!-- /.login-box-body -->

<div class="modal fade" id="forgot">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">¿Olvidó su contraseña?</h4>
      </div>
      <div class="modal-body">
        <p>Ingrese su correo electrónico y le será enviado un link para restablecerla</p>
    <?= $this->Form->create() ?>
          <div class="form-group has-feedback">
            <?= $this->Form->input('email',['label'=>false, 'placeholder'=>'Email', 'class'=>'form-control','div'=>'false', 'required'=>'required']) ?>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <?= $this->Form->button(__('Enviar'), ['type'=>'submit', 'class'=>'btn btn-primary']); ?>
         <?= $this->Form->end() ?>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->