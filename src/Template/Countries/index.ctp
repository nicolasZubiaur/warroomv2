<section class="content-header">
      <h1>
        <?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> Paises
        <small>Índice</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active"><a href="#"><i class="fa fa-page"></i> Paises</a></li>
        
      </ol>
      <div class="btn-group pull-right">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
       <i class="fa fa-gears"></i> <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
      <li><a href="#"><?= $this->Html->link(__('Nuevo Pais'), ['action' => 'add']) ?></a></li>
    </ul>
  </div>
    </section>
<div class="col-md-12">
    <table cellpadding="0" cellspacing="0" class="table table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($countries as $country): ?>
            <tr>
                <td><?= $this->Number->format($country->id) ?></td>
                <td><?= $this->Html->link(h($country->name), ['action' => 'view', $country->id]) ?></td>
                <td class="actions">
                    
                    <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $country->id], ['class'=>'btn btn-success btn-sm', 'escape'=>false]) ?>
                    <?= $this->Form->postLink('<i class="fa fa-trash-o"></i>', ['action' => 'delete', $country->id], ['confirm' => __('Are you sure you want to delete # {0}?', $country->name), 'class'=>'btn btn-danger btn-sm', 'escape'=>false]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
