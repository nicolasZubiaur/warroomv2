<div class="row">
<div class="col-md-12">

<?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar Country', ['action' => 'edit', $country->id], ['escape' => false,
	'class'=>'btn btn-app']
); ?> 

<?= $this->Form->postLink(
						'<i class="fa fa-trash"></i> Borrar Countries',
						array('action' => 'delete', $country->id),
						array('escape'=>false , 'class' => 'btn btn-app','confirm' => __('Are you sure you want to delete # {0}?', $country->id))) ;?>
	
	<?= $this->Html->link(
    '<i class="fa fa-plus"></i> Nuevo Countries', ['action' => 'add'],
    ['escape' => false,
	'class'=>'btn btn-app']) ?>

</div>	
	
</div>

<section class="content-header">
          <h1>
            <?= h($country->name) ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#"><?= h($country->name) ?></a></li>
            <li class="active"></li>
          </ol>
        </section>

<div class="countries view large-9 medium-8 columns content">
  <section class="content">

<div class="row">
   <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?= h($country->name) ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <table class="table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($country->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($country->id) ?></td>
        </tr>
    </table>
    

    <div class="box-footer clearfix">
        <h3><?= __('Related Articles') ?></h3>
        <?php if (!empty($country->articles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Slug') ?></th>
                <th><?= __('Body') ?></th>
                <th><?= __('Image') ?></th>
                <th><?= __('Parent Id') ?></th>
                <th><?= __('Next Id') ?></th>
                <th><?= __('Before Id') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Type') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Updated By') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->articles as $articles): ?>
            <tr>
                <td><?= h($articles->id) ?></td>
                <td><?= h($articles->user_id) ?></td>
                <td><?= h($articles->title) ?></td>
                <td><?= h($articles->slug) ?></td>
                <td><?= h($articles->body) ?></td>
                <td><?= h($articles->image) ?></td>
                <td><?= h($articles->parent_id) ?></td>
                <td><?= h($articles->next_id) ?></td>
                <td><?= h($articles->before_id) ?></td>
                <td><?= h($articles->status) ?></td>
                <td><?= h($articles->type) ?></td>
                <td><?= h($articles->country_id) ?></td>
                <td><?= h($articles->updated_by) ?></td>
                <td><?= h($articles->created) ?></td>
                <td><?= h($articles->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Articles', 'action' => 'view', $articles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Articles', 'action' => 'edit', $articles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Articles', 'action' => 'delete', $articles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="box-footer clearfix">
        <h3><?= __('Related Crises') ?></h3>
        <?php if (!empty($country->crises)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Analysis') ?></th>
                <th><?= __('Action') ?></th>
                <th><?= __('Result') ?></th>
                <th><?= __('Category') ?></th>
                <th><?= __('Severity') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Terms') ?></th>
                <th><?= __('City') ?></th>
                <th><?= __('Address') ?></th>
                <th><?= __('Country') ?></th>
                <th><?= __('Google Link') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->crises as $crises): ?>
            <tr>
                <td><?= h($crises->id) ?></td>
                <td><?= h($crises->name) ?></td>
                <td><?= h($crises->description) ?></td>
                <td><?= h($crises->Analysis) ?></td>
                <td><?= h($crises->Action) ?></td>
                <td><?= h($crises->Result) ?></td>
                <td><?= h($crises->category) ?></td>
                <td><?= h($crises->severity) ?></td>
                <td><?= h($crises->user_id) ?></td>
                <td><?= h($crises->country_id) ?></td>
                <td><?= h($crises->status) ?></td>
                <td><?= h($crises->modified) ?></td>
                <td><?= h($crises->created) ?></td>
                <td><?= h($crises->terms) ?></td>
                <td><?= h($crises->city) ?></td>
                <td><?= h($crises->address) ?></td>
                <td><?= h($crises->country) ?></td>
                <td><?= h($crises->google_link) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Crises', 'action' => 'view', $crises->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Crises', 'action' => 'edit', $crises->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Crises', 'action' => 'delete', $crises->id], ['confirm' => __('Are you sure you want to delete # {0}?', $crises->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="box-footer clearfix">
        <h3><?= __('Related Users') ?></h3>
        <?php if (!empty($country->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Role Id') ?></th>
                <th><?= __('Username') ?></th>
                <th><?= __('Password') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Telephone') ?></th>
                <th><?= __('Job Description') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Website') ?></th>
                <th><?= __('Activation Key') ?></th>
                <th><?= __('Image') ?></th>
                <th><?= __('Bio') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Updated') ?></th>
                <th><?= __('Updated By') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Timezone') ?></th>
                <th><?= __('Created By') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->role_id) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->name) ?></td>
                <td><?= h($users->telephone) ?></td>
                <td><?= h($users->job_description) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->website) ?></td>
                <td><?= h($users->activation_key) ?></td>
                <td><?= h($users->image) ?></td>
                <td><?= h($users->bio) ?></td>
                <td><?= h($users->country_id) ?></td>
                <td><?= h($users->status) ?></td>
                <td><?= h($users->updated) ?></td>
                <td><?= h($users->updated_by) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->timezone) ?></td>
                <td><?= h($users->created_by) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
			
			</div>	
</div>
</section>
</div>
