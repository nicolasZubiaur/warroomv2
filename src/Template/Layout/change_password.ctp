<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EnacmentCore</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
   	<?= $this->Html->css('bootstrap.min.css');?>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <?= $this->Html->css('AdminLTE.min.css'); ?>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <?= $this->Html->css('skins/_all-skins.min.css'); ?>
    <!-- iCheck -->
    <?= $this->Html->css('iCheck/flat/blue.css'); ?>
    <!-- Morris chart -->
    <?= $this->Html->css('morris/morris.css'); ?>
    <!-- jvectormap -->
    <?= $this->Html->css('jvectormap/jquery-jvectormap-1.2.2.css'); ?>
    <!-- Date Picker -->
    <?= $this->Html->css('datepicker/datepicker3.css'); ?>
    <!-- Daterange picker -->
    <?= $this->Html->css('daterangepicker/daterangepicker-bs3.css'); ?>
    
    <!-- jQuery 2.1.4 -->
    <?= $this->Html->script('jQuery-2.1.4.min.js');?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue">
    <div class="wrapper">

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
		<div class="row">
		<?= $this->Flash->render() ?>
		</div>
		<div class="row">
		<?= $this->fetch('content') ?>
		</div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
             <strong>Copyright &copy; Powered by <a href="http://enacment.com">Enacment</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <?= $this->Html->script('bootstrap.min');?>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <?= $this->Html->script('morris/morris.min.js');?>
    <!-- Sparkline -->
    <?= $this->Html->script('sparkline/jquery.sparkline.min.js');?>
    <!-- jvectormap -->
    <?= $this->Html->script('jvectormap/jquery-jvectormap-1.2.2.min.js');?>
    <?= $this->Html->script('jvectormap/jquery-jvectormap-world-mill-en.js');?>
    <!-- jQuery Knob Chart -->
    <?= $this->Html->script('knob/jquery.knob.js');?>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
     <?= $this->Html->script('daterangepicker/daterangepicker.js');?>
    <!-- datepicker -->
     <?= $this->Html->script('datepicker/bootstrap-datepicker.js');?>
   
    <!-- Slimscroll -->
    <?= $this->Html->script('slimScroll/jquery.slimscroll.min.js');?>
    <!-- FastClick -->
    <?= $this->Html->script('fastclick/fastclick.min.js');?>
    <!-- AdminLTE App -->
    <?= $this->Html->script('app.min');?>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

    <!-- AdminLTE for demo purposes -->
    <?= $this->Html->script('demo.js');?>
    
    <!-- Adding Checkeditor -->
    <?= $this->Html->script('ckeditor/ckeditor.js') ?>

  </body>
</html>
