<?= $this->Flash->render('auth') ?>
		<?= $this->Form->create() ?>
          <div class="form-group has-feedback">
            <?= $this->Form->input('email',['label'=>false, 'placeholder'=>'Email', 'class'=>'form-control','div'=>'false', 'required'=>'required']) ?>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <?= $this->Form->button(__('Enviar'), ['type'=>'submit', 'class'=>'btn btn-primary btn-block btn-flat']); ?>
            </div><!-- /.col -->
          </div>
        <?= $this->Form->end() ?>

      </div><!-- /.login-box-body -->
