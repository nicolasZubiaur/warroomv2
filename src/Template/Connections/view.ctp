<?php 
use Cake\I18n\Time;

if(isset($authUser->timezone)){
  $time->timezone = $authUser->timezone;
}
?>
<section class="content-header">
      <h1>
        <?php echo $this->Html->image('logo-mini.png', array('style'=>'height:30px')) ?> Stakeholders
        <small>Informacion de Stakeholder</small>
      </h1>
          <ol class="breadcrumb">
            <li><?php echo $this->Html->link('<i class="fa fa-home"></i> <span>Dashboard</span>', ['controller'=>'Pages', 'action'=>'dashboard'], ['escape'=>false]); ?></li>
            <li><a href="#">Stakeholders</a></li>
            <li class="active">$connection['name']</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-success">
                <div class="box-body box-profile">
                  <?php
                  	if ($connection['file']){
                  		
                  		 echo $this->Html->image('/'.$connection['file'], [ 'class'=>'profile-user-img img-responsive ']);
                  	}else{
                  		
                  		echo $this->Html->image('default-avatar.png', [ 'class'=>'profile-user-img img-responsive ']);
                  	}
						
                  
                   ?>
                  <h3 class="profile-username text-center"><?php echo $connection['name']; ?></h3>

                  <ul class="list-group list-group-unbordered">
                  <li class="list-group-item text-muted text-center"><i class="fa fa-flag pull-left"></i> <?php echo $connection['country']['name']; ?></li>
                    <li class="list-group-item text-muted text-center">
                      <i class="fa fa-briefcase pull-left" style="display:block"></i> <?php echo (!empty($connection['job_description'])?$connection['job_description']:'N/A'); ?>
                    </li>
                    <li class="list-group-item text-muted text-center">
                      <i class="fa fa-phone pull-left" style="display:block"></i> <?= (!empty($connection['office_phone'])?$connection['office_phone']:'N/A') ?>
                    </li>
                    <li class="list-group-item text-muted text-center">
                      <i class="fa fa-envelope pull-left" style="display:block"></i> <?php echo (!empty($connection['email'])?$this->Text->autoLinkEmails($connection['email']):'N/A'); ?>
                                      </li> 
                    <li class="list-group-item text-muted text-center">     <?php foreach ($connection->categories as $category) {
                     	echo $this->Html->link('#'.$category['name'], ['controller' => 'Categories', 'action' => 'view', $category['id']], ['target'=>'_self', 'escape'=>false, 'class'=>"btn btn-block btn-danger btn-sm", 'style'=>'margin-bottom:4px; word-wrap:break-word;']);
                    } ?></li>
                    <li class="list-group-item text-muted text-center">
                    <?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar',
    ['action' => 'edit', $connection['id']],
    ['escape' => false,
  'class'=>'btn btn-primary btn-block']
); ?> 
                    </li>
                  </ul>


                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	<div class="col-md-9">
		<div class="row">
	      <div class="box box-success"> 
	      <div class="box-header">
								<h4><i class="fa fa-pencil"></i> Notas sobre <?= $connection['name'] ?></h4>
							</div>
	      	 <?php echo (!empty($connection['notes'])?$this->Html-> div('box-body', $connection['notes']):'<div class="box-body"><h6 class="text-center"> Sin notas encontradas.</h6></div>'); ?>
	            </div>
	      <div class="box box-success"> 
	      <div class="box-header">
								<h4><i class="fa fa-bookmark"></i> Crisis Relacionadas</h4>
							</div>
                 				<div class="box-body">
					<?php if (!empty($connection->crises)){ ?>
					<table class="table table-striped table-bordered table-advance table-hover" id="Crises_index">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>
								<th><?php echo __('Nivel'); ?></th>
								<th><?php echo __('Category'); ?></th>
								
								<!-- <th><?php echo __('User Id'); ?></th> -->
							</tr>
						</thead>
						<tbody>
							<?php foreach ($connection->crises as $crisis): ?>
							<tr>
								<td><?php echo $this -> Html -> link($this -> Html -> tag('i', '', array('class' => 'fa fa-share')) . ' ' .$crisis['name'], array('controller' => 'crises', 'action' => 'view', $crisis['id']), array('class' => '', 'escape' => false)); ?></td>
								<td><?php echo ($crisis['severity'] =='verde'? '<span class="bg-green color-palette" style="padding:3px;text-align:center"> Verde</span>': 
												($crisis['severity'] =='amarillo'? '<span class="bg-yellow color-palette" style="padding:3px;text-align:center"> Amarillo</span>':
													($crisis['severity'] =='naranja'? '<span class="bg-orange color-palette" style="padding:3px;text-align:center"> Naranja</span>':
														'<span class="bg-red color-palette" style="padding:3px;text-align:center"> Rojo</span>'
														)
													)
												)
										; ?></td>
								<td><?php echo $crisis['category']; ?></td>

								<!-- <td><?php echo $crisis['user_id']; ?></td> -->

							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<?php } else { ?>
						<div class="box-body"><h6 class="text-center"> Sin crisis asignadas.</h6></div>
					<?php } ?>
				</div> 
	            </div>

			<?php
 ?>
		</div>
	</div>
</div>


	
