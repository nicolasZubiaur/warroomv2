<?php 
//title for layout
if(isset($crisisId) && !empty($crisisId)){
	//title for layout
	$this->Html
	->addCrumb('Crises',  array('controller' => 'crises', 'action' => 'view', $crisisId))
	->addCrumb(__('Panic Rooms'), '#');
}else{
	$this->Html
		->addCrumb('Stakeholders', '#');
}
?>
<?= $this->Html->script('/plugins/ckeditor/ckeditor') ?>
<div class="">
	<div class="col-md-12">
		<div class="box box-info ">
			<div class="box-header">
				<h1><?php echo __('Edit Contact'); ?></h1>
				</div>
				<div class="box-body">
				<div class="pull-right">
					<div class="btn-group">
						<a class="btn btn-default btn-sm" href="#" data-toggle="dropdown"> <i class="fa fa-cogs"></i> <i class="fa fa-angle-down"></i> </a>
						<ul class="dropdown-menu pull-right">
							<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Connection.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Connection.id'))); ?></li>
							<li><?php echo $this->Html->link(__('List Stakeholders'), array('action' => 'index')); ?></li>
							<li class="divider"></li>
							<li><?php echo $this->Html->link(__('List Crises'), array('controller' => 'crises', 'action' => 'index')); ?> </li>
							<li><?php echo $this->Html->link(__('New Crisis'), array('controller' => 'crises', 'action' => 'add')); ?> </li>
						</ul>
					</div>
				</div>
			</div>
			
			<!-- BEGIN FORM-->
			<?php echo $this->Form->create($connection, array(	'type' => 'file',
																'class' => 'form-horizontal form-bordered form-row-stripped', 
																)); ?>
				<div class="form-body">
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Name'); ?></label>
						<div class="col-md-5">
							<?php echo $this->Form->input('id'); ?>
							<?php echo $this->Form->input('name', ['label' => false, 'class' => 'form-control']); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Office Phone'); ?></label>
						<div class="col-md-5"><?php echo $this->Form->input('office_phone', ['label' => false, 'class' => 'form-control']); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Mobile Phone'); ?></label>
						<div class="col-md-5"><?php echo $this->Form->input('mobile_phone', ['label' => false, 'class' => 'form-control']); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Job Description'); ?></label>
						<div class="col-md-5"><?php echo $this->Form->input('job_description', array('rows' => '1', 'label' => false, 'class' => 'form-control')); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Email'); ?></label>
						<div class="col-md-5"><?php echo $this->Form->input('email', ['label' => false, 'class' => 'form-control']); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Notes'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('notes', array('label' => false, 'class' => 'form-control ckeditor')); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Crisis'); ?></label>
						<div class="col-md-5"><?php echo $this->Form->input('crises._ids', ['label' => false, 'class' => 'form-control select2', 'multiple'=>'multiple']); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Category'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('categories._ids', ['label' => false, 'class' => 'form-control select2']); ?></div>
					</div>
					<?php
						if($authUser!=1){
					?>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('País'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('country_id', array('class' => 'form-control select', 'label' => false, 'options'=>$countries)); ?></div>
					</div>
					<?php
					}
					?>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Avatar'); ?></label>
						<div class="col-md-3">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="input-group">
									<div class="form-control uneditable-input" data-trigger="fileinput">
										<i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn default btn-file">
										<?php echo $this->Form->input('file_file', array('type' => 'file', 'class' => false, 'label' => false)); ?>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer text-center">
					<?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Cancelar', $this->request->referer(), array('class' => 'btn btn-lg btn-default', 'escape'=>false, 'style'=>'padding:5px 20px')); ?>
					<button style="padding:5px 20px" class="btn btn-lg btn-success" type="submit"><i class="fa fa-check"></i>Guardar</button>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>