<?php
/* BEGIN PAGE LEVEL PLUGINS */
echo $this->Html->css('portfolio');
echo $this->Html->script('../plugins/jquery-mixitup/build/jquery.mixitup.min');
/* END PAGE LEVEL PLUGINS */
/*
  $alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J',
  11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T',
  21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
 */
$alphabet = array('A' => 1, 'B' => 2, 'C' => 3, 'D' => 4, 'E' => 5, 'F' => 6, 'G' => 7, 'H' => 8, 'I' => 9, 'J' => 10, 'K' => 11,
    'L' => 12, 'M' => 13, 'N' => 14, 'O' => 15, 'P' => 16, 'Q' => 17, 'R' => 18, 'S' => 19, 'T' => 20, 'U' => 21,
    'V' => 22, 'W' => 23, 'X' => 24, 'Y' => 25, 'Z' => 26);

$contacts = array('A' => '', 'B' => '', 'C' => '', 'D' => '', 'E' => '', 'F' => '', 'G' => '', 'H' => '', 'I' => '', 'J' => '', 'K' => '',
    'L' => '', 'M' => '', 'N' => '', 'O' => '', 'P' => '', 'Q' => '', 'R' => '', 'S' => '', 'T' => '', 'U' => '',
    'V' => '', 'W' => '', 'X' => '', 'Y' => '', 'Z' => '', '0' => '', '1' => '', '2' => '', '3' => '', '4' => '', '5' => '',
    '6' => '', '7' => '', '8' => '', '9' => '');

//title for layout
$this->Html
        ->addCrumb('Stakeholders', array('controller' => 'connections', 'action' => 'index'));

foreach ($connections as $connection):
    if (is_null($connection['file'])) {
        $image = $this->Html->image('default-avatar.png', array('class' => 'profile-user-img img-responsive '));
    } else {
        $image = $this->Html->image('/' . $connection['file'], array('class' => 'profile-user-img img-responsive '));
    }

    $name = $this->Html->tag('h3', $this->Html->link($connection['name'], array('action' => 'view', $connection['id'])), array('class' => 'font-blue-madison', 'style' => 'margin-top: 0px;margin-bottom: 0px;'));

    if (!empty($connection['email'])) {
        $name .= $this->Html->tag('span', $this->Html->tag('i', '', array('class' => 'fa fa-link btn blue btn-xs')) . ' ' . $connection['email'], array('class' => 'media-object', 'escape' => false));
    }

    if (!empty($connection['mobile_phone'])) {
        $name .= $this->Html->tag('span', $this->Html->tag('i', '', array('class' => 'fa fa-search btn green btn-xs')) . ' ' . $connection['mobile_phone'], array('class' => 'media-object', 'escape' => false));
    }

    $image = $this->Html->div('col-md-2 col-sm-6 stakeholder media-object', $image);
    $name = $this->Html->div('col-md-10 col-sm-6 stakeholder', $name);

    if (!empty($connection['name'][0])) {
        $contacts[strtoupper($connection['name'][0])] .= $this->Html->div('row no-margin margin-bottom-20', $image . $name);
    }
endforeach;
?>

<div class="">
    <div class="col-md-12 col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="caption">
                                <div class="pull-right">
                    <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-plus')) . ' ' . __('Nuevo stakeholder'), array('action' => 'add'), array('class' => 'btn btn-default btn-sm', 'escape' => false)); ?>
                </div>
                <div class="col-md-10">
                    <ul class="mix-filter">
                        <li data-filter="all" class="filter active">Todos</li>
                        <?php
                        foreach ($alphabet as $key => $value) {
                            if ($contacts[$key] != '') {
                                echo $this->Html->tag('li', $key, array('class' => 'filter ', 'data-filter' => '.category-' . $value));
                            }
                        }
                        ?>
                    </ul>
                    </div>
                </div>
			<div class="Custom_category_filters">
                <div class="col-md-10">
                    <?php
                    $base_url = array('controller' => 'connections', 'action' => 'index');
                    echo $this->Form->create("Filter", array('url' => $base_url));
                    echo $this->Form->input("category_id", array('label' => '', 'options' => $categories, 'empty' => 'Todas las Categorias', 'default' => ''));
					?>
				</div>
				<div class="col-md-10">
					<?php
						if($authUser['role_id']==1){
					?>
					<select class="form-control select" name="country_id" id="country-id">
						<option value="">Todos los Países</option>
							<?php
								foreach($countries as $country){
									echo '<option value="'.$country->id.'">'.$country->name.'</option>';
								}
							?>
					</select>
					<?php
	                    }else{
							echo $this->Form->input("country_id", array('type' => 'hidden', 'value' => $authUser['country_id']));
						}
                	?>
				</div>
                <div class="col-md-2">
					<button class="btn btn-success btn-block" type="submit"><i class="fa fa-search"></i> <?php echo __('Buscar'); ?></button>
                </div>
                <?php               
				 	echo $this->Form->end();  
				 ?>
			</div>
		</div>
            
            <div class="box-body">
                <div class="margin-top-10">
                    <div class="row mix-grid" id="mixgrid">
                        <?php
                        foreach ($contacts as $key => $value) {
                            if (!empty($value)) {
                                $mix_alphabet = $this->Html->div('col-md-12 col-sm-12', '<h3 class="alert alert-success alert-dismissible"><i class="fa fa-user fa-fw"></i> ' . $key . '</h3>');
                                echo $this->Html->div('col-md-6 col-sm-6 mix category-' . $alphabet[$key], $mix_alphabet . $this->Html->div('col-md-12 col-sm-12 margin-bottom-10 no-padding-right', $value), array('data-value' => $alphabet[$key]));
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    jQuery(document).ready(function () {
    	$('#mixgrid').mixItUp();
    	    	
    	<?php
    	if(isset($coun)){
    	?>
    		$('#country-id option[value=<?php echo $coun; ?>]').attr('selected', 'selected');
    	<?php
    	}
    	?>
    });
</script>
