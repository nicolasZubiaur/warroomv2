<?php 
//title for layout
if(isset($crisis_id) && !empty($crisis_id)){
	//title for layout
	$this->Html
	->addCrumb('Crises',  array('controller' => 'crises', 'action' => 'view', $crisis_id))
	->addCrumb('Stakeholders', array('controller' => 'connections', 'action' => 'index'));
}else{
	$this->Html
	->addCrumb('Stakeholders', array('controller' => 'connections', 'action' => 'index'));
}
?>
<?= $this->Html->script('/plugins/ckeditor/ckeditor') ?>
<div class="row connections form">
	<div class="col-md-12">
		<div class="portlet box blue ">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-gift"></i><?php echo __('Add Contact'); ?></div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn btn-default btn-sm" href="#" data-toggle="dropdown"> <i class="fa fa-cogs"></i> <i class="fa fa-angle-down"></i> </a>
						<ul class="dropdown-menu pull-right">
							<li><?php echo $this->Html->link(__('List Contacts'), array('action' => 'index')); ?></li>
							<li class="divider"></li>
							<li><?php echo $this->Html->link(__('List Crises'), array('controller' => 'crises', 'action' => 'index')); ?> </li>
							<li><?php echo $this->Html->link(__('New Crisis'), array('controller' => 'crises', 'action' => 'add')); ?> </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php echo $this->Form->create($connection, array(	'type' => 'file',
																'class' => 'form-horizontal form-bordered form-row-stripped', 
															)); ?>
				<div class="form-body">
                    <div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Name'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('name', ['label' => false, 'class' => 'form-control']); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Office Phone'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('office_phone', ['label' => false, 'class' => 'form-control']); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Mobile Phone'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('mobile_phone', ['label' => false, 'class' => 'form-control']); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Job Description'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('job_description', ['label' => false, 'class' => 'form-control','rows' => '1']); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Email'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('email', ['label' => false, 'class' => 'form-control']); ?></div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Notes'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('notes', ['label' => false, 'class' => 'form-control ckeditor', 'type'=>'textarea']); ?></div>
					</div>
					<?php if(isset($crisis_id)) {
						echo $this->Form->input('crisis_id', array('value' => $crisis_id, 'type' => 'hidden')); 
					}  else { ?>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Crisis'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('crises._ids', array('class' => 'form-control select2', 'label' => false )); ?></div>
					</div>
					<?php } ?>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('Category'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('categories._ids', array('class' => 'form-control select2', 'label' => false)); ?></div>
					</div>
					<?php
						if($authUser==1){
					?>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo __('País'); ?></label>
						<div class="col-md-7"><?php echo $this->Form->input('country_id', array('class' => 'form-control select', 'label' => false, 'options'=>$countries)); ?></div>
					</div>
					<?php
					}else{
						echo $this->Form->input('country_id', array('type' => 'hidden', 'value' => $authUser['country_id']));
					}
					?>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Avatar'); ?></label>
						<div class="col-md-3">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="input-group input-large">
									<div class="form-control uneditable-input" data-trigger="fileinput">
										<i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn default btn-file">
										<span class="fileinput-new"><?php echo __('Select file'); ?> </span>
										<span class="fileinput-exists"><?php echo __('Change'); ?> </span>
										<?php echo $this->Form->input('file_file', array('type' => 'file', 'class' => false, 'label' => false)); ?>
									</span>
									<a href="#" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"><?php echo __('Remove'); ?> </a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions right">
					<?php echo $this->Html->link(__('Cancel'), $this->request->referer(), array('class' => 'btn default')); ?>
					<button class="btn green" type="submit"><i class="fa fa-check"></i> <?php echo __('Submit'); ?></button>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>