<div class="row">
<div class="col-md-12">

<?= $this->Html->link(
    '<i class="fa fa-edit"></i> Editar Group', ['action' => 'edit', $group->id], ['escape' => false,
	'class'=>'btn btn-app']
); ?> 

<?= $this->Form->postLink(
						'<i class="fa fa-trash"></i> Borrar Groups',
						array('action' => 'delete', $group->id),
						array('escape'=>false , 'class' => 'btn btn-app','confirm' => __('Are you sure you want to delete # {0}?', $group->id))) ;?>
	
	<?= $this->Html->link(
    '<i class="fa fa-plus"></i> Nuevo Groups', ['action' => 'add'],
    ['escape' => false,
	'class'=>'btn btn-app']) ?>

</div>	
	
</div>

<section class="content-header">
          <h1>
            <?= h($group->name) ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="#"><?= h($group->name) ?></a></li>
            <li class="active"></li>
          </ol>
        </section>

<div class="groups view large-9 medium-8 columns content">
  <section class="content">

<div class="row">
   <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?= h($group->name) ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <table class="table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($group->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Role') ?></th>
            <td><?= $group->has('role') ? $this->Html->link($group->role->title, ['controller' => 'Roles', 'action' => 'view', $group->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Country') ?></th>
            <td><?= $group->has('country') ? $this->Html->link($group->country->name, ['controller' => 'Countries', 'action' => 'view', $group->country->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($group->id) ?></td>
        </tr>
    </table>
    

    <div class="box-footer clearfix">
        <h3><?= __('Related Users') ?></h3>
        <?php if (!empty($group->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Role Id') ?></th>
                <th><?= __('Username') ?></th>
                <th><?= __('Password') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Telephone') ?></th>
                <th><?= __('Job Description') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Website') ?></th>
                <th><?= __('Activation Key') ?></th>
                <th><?= __('Image') ?></th>
                <th><?= __('Bio') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Updated') ?></th>
                <th><?= __('Updated By') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Timezone') ?></th>
                <th><?= __('Created By') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($group->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->role_id) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->name) ?></td>
                <td><?= h($users->telephone) ?></td>
                <td><?= h($users->job_description) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->website) ?></td>
                <td><?= h($users->activation_key) ?></td>
                <td><?= h($users->image) ?></td>
                <td><?= h($users->bio) ?></td>
                <td><?= h($users->country_id) ?></td>
                <td><?= h($users->status) ?></td>
                <td><?= h($users->updated) ?></td>
                <td><?= h($users->updated_by) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->timezone) ?></td>
                <td><?= h($users->created_by) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
			
			</div>	
</div>
</section>
</div>
