<section class="content">
<div class="row">
  <div class="col-md-12">
      <div class="box box-info">
	        <div class="box-header">
	          <h3 class="box-title"><i class="fa fa-add"></i> <?= __('Add Group') ?></h3>
	
	        </div>
        	<!-- /.box-header -->
<div class="box-body no-padding">
    <?= $this->Form->create($group,['type'=>'file']) ?>
	<table class="table" style="width: 90%">
	<tbody>

        
<tr><th>Nombre</th><td><?php            echo $this->Form->input('name', ['empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr>
<tr><th>Tipo</th><td><?php			
            echo $this->Form->input('role_id', ['options' => $roles, 'empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr>
<?php
	if($authUser['role_id']==1){
?>
<tr><th>País</th><td><?php			
            echo $this->Form->input('country_id', ['options' => $countries, 'empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr>
<?php
	}else{
		echo $this->Form->input('country_id', ['type' => 'hidden', 'value' => $authUser['country_id']]);
	}
?>
<tr>
	<th>Usuarios</th>
	<td>
		<?php            
			//echo $this->Form->input('users._ids', ['options' => $users, 'empty' => true,'class'=>'form-control','label'=>false]);
		?>
		<div id="usuarios">
			<div id="mensaje" class="text-center" style="display: none;">
				<i class="fa fa-exclamation-triangle fa-3x fa-fw"></i>
				<p>Debes seleccionar un País y un Rol</p>
			</div>
			<div id="mensaje2" class="text-center" style="display: none;">
				<i class="fa fa-exclamation-triangle fa-3x fa-fw"></i>
				<p>No se encontraron resultados</p>
			</div>
			<div id="loading" class="text-center" style="display: none;">
				<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
			</div>
			<div id="noload" class="text-center" style="display: none;">
				<i class="fa fa-exclamation-circle fa-3x fa-fw"></i>
				<p>Hubo un error</p>
			</div>
		</div>
	</td>
</tr>       
</tbody></table>
          <div style="padding-top: 10px;padding-bottom: 50px;padding-left: 5%;padding-right: 5%">
          	<?= 
              $this->Form->button('<i class="fa fa-floppy-o "></i> Guardar', [
														    'type' => 'submit',
														    'escape' => false,
														    'class' =>'btn btn-primary btn-block btn-lg'
															]); ?>
          </div>
              
    		<?= $this->Form->end() ?>

        </div>
        <!-- /.box-body -->
      </div>

	</div>
    <!-- /.col -->
</div>
<script>
	$(document).ready(function(){
		
		function getusers(){
			$('#users-ids').remove();
			$('#loading').show();
			$('#noload').hide();
			
			$.ajax({
				url: "<?php echo $this->Url->build(["controller" => "Users", "action" => "searchusers" ]); ?>",
				data:{
					country: $('#country-id').val(),
					role: $('#role-id').val()
				},
				dataType: "json",
				success: function(data){
					if(data.length>0){
						$('#usuarios').append('<select class="form-control select2" multiple="multiple" id="users-ids" name="users[_ids][]"></select>');
						$.each( data, function( key, value ) {
							$('#users-ids').append('<option value="'+value.id+'">'+value.name+'</option>');
						});
						$('#loading').hide();	
					}else{
						$('#loading').hide();	
						$('#mensaje2').show(500, function() {
							setTimeout(function() {
								$('#mensaje2').html(function() {
									setTimeout(function() {
										$('#mensaje2').hide();
									}, 1000);
								});
							}, 2500);
						});
					}
				},
				error: function(data){								
					$('#loading').hide();
					$('#noload').show();
				}  
			});
		}
		
		$('#country-id').change(function(){
			if(($('#country-id').val())&&($('#role-id').val())){
				getusers();	
			}else{
				$('#mensaje').show(500, function() {
						setTimeout(function() {
							$('#mensaje').html(function() {
								setTimeout(function() {
									$('#mensaje').hide();
								}, 1000);
							});
						}, 2500);
					});
			}
		});
		
		$('#role-id').change(function(){
			if(($('#country-id').val())&& ($('#role-id').val())){
				getusers();
			}else{
				$('#mensaje').show(500, function() {
						setTimeout(function() {
							$('#mensaje').html(function() {
								setTimeout(function() {
									$('#mensaje').hide();
								}, 1000);
							});
						}, 2500);
					});
			}
		});
	});
</script>