<section class="content">
<div class="row">
  <div class="col-md-12">
      <div class="box">
	        <div class="box-header">
	          <h3 class="box-title"><i class="fa fa-add"></i> <?= __('Edit Group') ?></h3>
	
	        </div>
        	<!-- /.box-header -->
<div class="box-body no-padding">
    <?= $this->Form->create($group,['type'=>'file']) ?>
	<table class="table" style="width: 90%">
	<tbody>

        
<tr><th>Nombre</th><td><?php            echo $this->Form->input('name', ['empty' => true,'class'=>'form-control','label'=>false]);
?></td></tr>
<tr>
	<th>Usuarios</th>
	<td>
		<?php            
			echo $this->Form->input('users._ids', ['options' => $users, 'class'=>'form-control select2','label'=>false]);
		?>
	</td>
</tr>       
</tbody></table>
          <div style="padding-top: 10px;padding-bottom: 50px;padding-left: 5%;padding-right: 5%">
          	<?= 
              $this->Form->button('<i class="fa fa-floppy-o "></i> Guardar', [
														    'type' => 'submit',
														    'escape' => false,
														    'class' =>'btn btn-primary btn-block btn-lg'
															]); ?>
          </div>
              
    		<?= $this->Form->end() ?>

        </div>
        <!-- /.box-body -->
      </div>

	</div>
    <!-- /.col -->
</div>
<script>
	$(document).ready(function(){
		
		function getusers(){
			$('#users-ids').remove();
			$('#loading').show();
			$('#noload').hide();
			
			$.ajax({
				url: "<?php echo $this->Url->build(["controller" => "Users", "action" => "searchusers" ]); ?>",
				data:{
					country: $('#country-id').val(),
					role: $('#role-id').val()
				},
				dataType: "json",
				success: function(data){
					if(data.length>0){
						$('#usuarios').append('<select class="form-control" multiple="multiple" id="users-ids" name="users[_ids][]"></select>');
						$.each( data, function( key, value ) {
							$('#users-ids').append('<option value="'+value.id+'">'+value.name+'</option>');
						});
						$('#loading').hide();	
					}else{
						$('#loading').hide();	
						$('#mensaje2').show(500, function() {
							setTimeout(function() {
								$('#mensaje2').html(function() {
									setTimeout(function() {
										$('#mensaje2').hide();
									}, 1000);
								});
							}, 2500);
						});
					}
				},
				error: function(data){								
					$('#loading').hide();
					$('#noload').show();
				}  
			});
		}
		
		$('#country-id').change(function(){
			if(($('#country-id').val())&&($('#role-id').val())){
				getusers();	
			}else{
				$('#mensaje').show(500, function() {
						setTimeout(function() {
							$('#mensaje').html(function() {
								setTimeout(function() {
									$('#mensaje').hide();
								}, 1000);
							});
						}, 2500);
					});
			}
		});
		
		$('#role-id').change(function(){
			if(($('#country-id').val())&& ($('#role-id').val())){
				getusers();
			}else{
				$('#mensaje').show(500, function() {
						setTimeout(function() {
							$('#mensaje').html(function() {
								setTimeout(function() {
									$('#mensaje').hide();
								}, 1000);
							});
						}, 2500);
					});
			}
		});
	});
</script>