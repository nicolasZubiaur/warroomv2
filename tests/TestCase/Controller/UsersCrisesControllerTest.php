<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersCrisesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UsersCrisesController Test Case
 */
class UsersCrisesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_crises',
        'app.users',
        'app.countries',
        'app.articles',
        'app.comments',
        'app.tags',
        'app.crises',
        'app.crisis_types',
        'app.feeds',
        'app.connections',
        'app.categories',
        'app.connections_categories',
        'app.connections_crises',
        'app.tasks',
        'app.statuses',
        'app.crises_tags',
        'app.articles_tags',
        'app.roles',
        'app.groups',
        'app.users_groups'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
