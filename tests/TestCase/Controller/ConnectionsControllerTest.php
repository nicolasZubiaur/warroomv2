<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ConnectionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ConnectionsController Test Case
 */
class ConnectionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.connections',
        'app.feeds',
        'app.users',
        'app.crises',
        'app.users_crises',
        'app.crisis_types',
        'app.tasks',
        'app.statuses',
        'app.connections_crises',
        'app.assign_users',
        'app.categories',
        'app.connections_categories'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
