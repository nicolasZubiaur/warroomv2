<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CrisesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CrisesController Test Case
 */
class CrisesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.crises',
        'app.users',
        'app.users_crises',
        'app.crisis_types',
        'app.feeds',
        'app.assign_users',
        'app.connections',
        'app.categories',
        'app.connections_categories',
        'app.connections_crises',
        'app.tasks',
        'app.statuses'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
