<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersCrisesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersCrisesTable Test Case
 */
class UsersCrisesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersCrisesTable
     */
    public $UsersCrises;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_crises',
        'app.users',
        'app.countries',
        'app.articles',
        'app.comments',
        'app.tags',
        'app.crises',
        'app.crisis_types',
        'app.feeds',
        'app.connections',
        'app.categories',
        'app.connections_categories',
        'app.connections_crises',
        'app.tasks',
        'app.statuses',
        'app.crises_tags',
        'app.articles_tags',
        'app.roles',
        'app.groups',
        'app.users_groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UsersCrises') ? [] : ['className' => 'App\Model\Table\UsersCrisesTable'];
        $this->UsersCrises = TableRegistry::get('UsersCrises', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersCrises);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
