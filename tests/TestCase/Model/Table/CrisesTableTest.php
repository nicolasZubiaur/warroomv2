<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CrisesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CrisesTable Test Case
 */
class CrisesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CrisesTable
     */
    public $Crises;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.crises',
        'app.users',
        'app.users_crises',
        'app.crisis_types',
        'app.feeds',
        'app.tasks',
        'app.connections',
        'app.connections_crises'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Crises') ? [] : ['className' => 'App\Model\Table\CrisesTable'];
        $this->Crises = TableRegistry::get('Crises', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Crises);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
