<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CrisisTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CrisisTypesTable Test Case
 */
class CrisisTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CrisisTypesTable
     */
    public $CrisisTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.crisis_types',
        'app.crises',
        'app.users',
        'app.feeds',
        'app.assign_users',
        'app.connections',
        'app.categories',
        'app.connections_categories',
        'app.connections_crises',
        'app.tasks',
        'app.statuses',
        'app.tags',
        'app.crises_tags',
        'app.types',
        'app.crises_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CrisisTypes') ? [] : ['className' => 'App\Model\Table\CrisisTypesTable'];
        $this->CrisisTypes = TableRegistry::get('CrisisTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CrisisTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
